import pandas as pd
import numpy as np
import math
#import matplotlib.pyplot as plt
import os
system_path = "/Users/pegah/Documents/VS/participant_data_updated/"

participants_list = ["Feb20_ID02", "Feb22_ID03" , "Mar1_ID04" , "Mar14_ID05", "Mar16_ID06" , "Mar21_ID07",
                     "Mar22_ID08", "Apr10_ID09","Apr13_ID10","Apr24_ID12", "Apr27_ID13", "May22_ID14" , "May22_ID15", "Jun11_ID16"]
activities = ["sixmwt", "sitting" , "walking" , "lying" , "standing", "eating" , "brushing", "climbup", "climbdown"]


def add_vector_magnitude(participant, activity):
    # i have the accel csv file per participant, per activity. Each csv file has 4 columns. ts, x, y, z
    # in this function I want to add a new column called r, and I want it to be the vector magnitude of the values
    # which is radikal (x^2 + y^2 + z^2)

    accel_path = system_path + participant + "_controlled" + "/watch1/watch1_gyro_activity_sep_files/" + activity + ".csv"
    accel_df = pd.read_csv(accel_path)
    accel_df['r'] = (accel_df.x **2 + accel_df.y **2 + accel_df.z **2) ** 0.5

    accel_df.to_csv(accel_path)


def compress_for_every_second(participant, activity):
    # The watch gives 20 values per second for accel. I want to average them all and have a single value of r for every second.
    # The first and the last ts may not have exactly 20 values

    # I want to make new csv files and store them separately.
    # I also want to remove the ms part of the ts

    accel_path = system_path + participant + "_controlled" + "/watch1/watch1_gyro_activity_sep_files/" + activity + ".csv"
    accel_df  = pd.read_csv(accel_path)
    accel_df['ts'] = pd.to_datetime(accel_df['ts']).apply(lambda x: x.replace(microsecond=0)) # remove ms

    compressed_accel_dir = system_path + participant + "_controlled" + "/watch1/watch1_gyro_activity_sep_files/compressed/"
    if not os.path.exists( compressed_accel_dir):
        os.makedirs( compressed_accel_dir)

    compressed_accel_path = compressed_accel_dir + activity + ".csv"

    compressed_accel_df = pd.DataFrame(index = None, columns=['ts','r'] )

    # group by ts   so all the rwos that have the same ts will be together and I can average them
    # the good thing about group by is that it does not use a fixed constant value as the frequency
    # the exact same code can be used for the gyroscope data

    for ts , sub_section in accel_df.groupby('ts'):
        average = np.mean(sub_section.r)
        new_row = {'ts': ts , 'r': average}
        compressed_accel_df = compressed_accel_df.append(new_row , ignore_index= True)

    #print compressed_accel_df
    compressed_accel_df.to_csv(compressed_accel_path)

def add_vector_magnitude_wild(participant):

    gyro_path = system_path + participant + "_wild" + "/watch1/watch1_csv_files/" + "all_gyro_data.csv"
    gyro_df = pd.read_csv(gyro_path)
    gyro_df['r'] = (gyro_df.x ** 2 + gyro_df.y ** 2 + gyro_df.z ** 2) ** 0.5

    gyro_df.to_csv(gyro_path)

def compress_for_every_second_wild(participant):

    gyro_path = system_path + participant + "_wild" + "/watch1/watch1_csv_files/" + "all_gyro_data.csv"
    gyro_df = pd.read_csv(gyro_path)

    gyro_df['ts'] = pd.to_datetime(gyro_df['ts']).apply(lambda x: x.replace(microsecond=0))  # remove ms

    compressed_gyro_path = system_path + participant + "_wild" + "/watch1/watch1_csv_files/all_gyro_data_compressed.csv"

    compressed_gyro_df = pd.DataFrame(index=None, columns=['ts', 'r'])

    # group by ts   so all the rwos that have the same ts will be together and I can average them
    # the good thing about group by is that it does not use a fixed constant value as the frequency
    # the exact same code can be used for the gyroscope data

    for ts, sub_section in gyro_df.groupby('ts'):
        average = np.mean(sub_section.r)
        new_row = {'ts': ts, 'r': average}
        compressed_gyro_df = compressed_gyro_df.append(new_row, ignore_index=True)

    # print compressed_accel_df
    compressed_gyro_df.to_csv(compressed_gyro_path)


if __name__ == "__main__":


    for participant in participants_list:
        print "participant is: ", participant
        for activity in activities:
            print "activity is: ", activity

            add_vector_magnitude(participant, activity)
            compress_for_every_second(participant, activity)
