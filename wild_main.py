import pandas as pd
import csv
import os
from CopdFeatureExtraction.helpers.SensorHelper import *
from datetime import datetime, date
import time
import general_device

system_path = "/Users/pegah/Documents/VS/participant_data_updated/"
this_participant_path= "Mar14_ID05_wild/"
BH_summary_file = "shifted_times_log.csv"
NEW_TIME = True

def watch_csv_creator(watch, new_time):
    # First, read all .json files.
    # Take the hr data and put it into a csv file. Take the accel data and put it into a csv file. Take the gyro data and put it into a csv file
    # Second, combine all of the hr data into one csv and combine all of the accel data files into one csv and combine all of th e gyro data file into one csv

    list_of_files =[]
    for file in os.listdir(system_path+this_participant_path+watch):
        if file.endswith(".json"):
            list_of_files.append(file)

    for item in list_of_files:
        file_name= str(item).split(".")[0]
        file_path = system_path + this_participant_path + watch +"/" + file_name +".json"
        hr_file_path = system_path+this_participant_path + watch +"/" + watch +"_csv_files/" +file_name +"_hr.csv"
        accel_file_path= system_path+this_participant_path + watch + "/" + watch +"_csv_files/" + file_name +"_accel.csv"
        gyro_file_path= system_path+this_participant_path + watch +"/" + watch +"_csv_files/" + file_name +"_gyro.csv"

        data = SensorJson(file_path)
        hr = data.get("hr")
        accel = data.get("accel")
        gyro = data.get("gyro")

        if new_time:
            hr.index =hr.index -  pd.Timedelta('04:00:00')
            accel.index =accel.index - pd.Timedelta('04:00:00')
            gyro.index= gyro.index - pd.Timedelta('04:00:00')
        else:
            hr.index = hr.index - pd.Timedelta('05:00:00')
            accel.index = accel.index - pd.Timedelta('05:00:00')
            gyro.index = gyro.index - pd.Timedelta('05:00:00')

        hr.to_csv(hr_file_path)
        accel.to_csv(accel_file_path)
        gyro.to_csv(gyro_file_path)

    all_hr_path = system_path + this_participant_path + watch + "/" + watch +"_csv_files" +"/all_hr_data.csv"
    all_accel_path = system_path + this_participant_path+ watch + "/" + watch +"_csv_files" +"/all_accel_data.csv"
    all_gyro_path = system_path + this_participant_path + watch + "/" + watch +"_csv_files" +"/all_gyro_data.csv"


    hr_files_list =[]
    accel_files_list = []
    gyro_files_list =[]

    for file in os.listdir(system_path + this_participant_path + watch + "/" + watch +"_csv_files" ):

        if file.endswith("hr.csv"):
            hr_files_list.append(file)
        elif file.endswith("accel.csv"):
            accel_files_list.append(file)
        elif file.endswith("gyro.csv"):
            gyro_files_list.append(file)

    hr_files_list.sort()
    accel_files_list.sort()
    gyro_files_list.sort()

    for i in range (len(hr_files_list)):
        hr_files_list[i]=open(system_path + this_participant_path + watch + "/" + watch +"_csv_files/" + hr_files_list[i], "r")

    for i in range (len(accel_files_list)):
        accel_files_list[i]=open(system_path + this_participant_path + watch + "/" + watch +"_csv_files/" +accel_files_list[i], "r")

    for i in range (len(gyro_files_list)):
        gyro_files_list[i]=open(system_path + this_participant_path + watch + "/" + watch +"_csv_files/" +gyro_files_list[i], "r")

    combined_csv_hr = pd.concat([pd.read_csv(f) for f in hr_files_list])
    combined_csv_hr.to_csv(all_hr_path, header=['ts', 'bpm', 'acc' ], index= False)

    combined_csv_accel = pd.concat([pd.read_csv(f) for f in accel_files_list])
    combined_csv_accel.to_csv(all_accel_path, header=['ts', 'x', 'y', 'z' ], index=False)

    combined_csv_gyro = pd.concat([pd.read_csv(f) for f in gyro_files_list])
    combined_csv_gyro.to_csv(all_gyro_path, header=['ts', 'x', 'y', 'z' ], index=False)

    csv_data = pd.read_csv(all_hr_path)
    titles_list = csv_data.columns.tolist()
    general_device.remove_duplicate_timestamps_and_time_ranges(all_hr_path,titles_list[0])

    return all_hr_path, all_accel_path, all_gyro_path

def phone_csv_creator(new_time):
    # First, read all .json files.
    # Take the accel data and put it into a csv file. Take the gyro data and put it into a csv file
    # Second, combine all of the accel data files into one csv and combine all of th e gyro data file into one csv

    list_of_files = []
    for file in os.listdir(system_path + this_participant_path + "phone"):
        if file.endswith(".json"):
            list_of_files.append(file)

    for item in list_of_files:
        file_name = str(item).split(".")[0]
        file_path = system_path + this_participant_path + "phone/" + file_name + ".json"
        accel_file_path = system_path + this_participant_path + "phone/phone_csv_files/ " + file_name + "_accel.csv"
        gyro_file_path = system_path + this_participant_path + "phone/phone_csv_files/ " + file_name + "_gyro.csv"

        data = SensorJson(file_path)
        accel = data.get("accel")
        gyro = data.get("gyro")


        if new_time:
            accel.index =accel.index - pd.Timedelta('04:00:00')
            gyro.index= gyro.index - pd.Timedelta('04:00:00')
        else:
            accel.index = accel.index - pd.Timedelta('05:00:00')
            gyro.index = gyro.index - pd.Timedelta('05:00:00')


        accel.to_csv(accel_file_path)
        gyro.to_csv(gyro_file_path)

    all_accel_path = system_path + this_participant_path + "phone/phone_csv_files/all_accel_data.csv"
    all_gyro_path = system_path + this_participant_path + "phone/phone_csv_files/all_gyro_data.csv"

    accel_files_list = []
    gyro_files_list = []

    for file in os.listdir(system_path + this_participant_path + "phone/phone_csv_files"):

        if file.endswith("accel.csv"):
            accel_files_list.append(file)
        elif file.endswith("gyro.csv"):
            gyro_files_list.append(file)

    accel_files_list.sort()
    gyro_files_list.sort()


    for i in range (len(accel_files_list)):
        accel_files_list[i]=open(system_path + this_participant_path + "phone/phone_csv_files/" + accel_files_list[i], "r")

    for i in range (len(gyro_files_list)):
        gyro_files_list[i]=open(system_path + this_participant_path + "phone/phone_csv_files/" + gyro_files_list[i], "r")


    combined_csv_accel = pd.concat([pd.read_csv(f) for f in accel_files_list])
    combined_csv_accel.to_csv(all_accel_path, index=False)

    combined_csv_gyro = pd.concat([pd.read_csv(f) for f in gyro_files_list])
    combined_csv_gyro.to_csv(all_gyro_path, index=False)

    return all_accel_path , all_gyro_path

def fill_missing_rows_in_file(file_path):

    # For imputating the data in the watch all_hr_data csv

    reader = pd.read_csv(file_path)  #reader is a pandas dataframe
    new_path = file_path.split(".csv")[0] + "_complete.csv"
    writer_file = open(new_path, "w")
    writer_file.truncate()
    start_index = 0

    heart_rates = reader.iloc[:,1]
    reliabilities = reader.iloc[:,2]

    timestamps= reader.iloc[:,0]
    timestamps = pd.to_datetime(timestamps)
    timestamps = timestamps.apply(lambda t: t.replace(microsecond=0)) # removing the milliseconds

    times = timestamps.dt.time

    file_length = times.size
    last_line_time = times[file_length-1]

    writer_df = pd.DataFrame(columns=['ts', 'hr', 'r'])  # writer is also a pandas dataframe
    while True:

        start_time = times[start_index]
        if start_time == last_line_time:
            writer_df = writer_df.append({'ts': timestamps[start_index] , 'hr': start_hr , 'r': start_r} , ignore_index= True )
            print "here in break"
            break

        # The idea is to read a line from the watch csv file, read the next line, if there is gap between them create rows
        # with new timestamps and the same heart rate value and reliability value

        start_hr = heart_rates[start_index]
        start_r = reliabilities[start_index]
        next_ts_time = times[start_index+1]

        subtraction = datetime.combine(date.today(), next_ts_time) - datetime.combine(date.today(),start_time)
        subtraction = abs(subtraction.total_seconds())

        if start_time != next_ts_time:
            writer_df = writer_df.append({'ts': timestamps[start_index] , 'hr': start_hr , 'r': start_r} , ignore_index= True )

        new_timestamp = timestamps[start_index]
        for j in range(1, int(subtraction)):
            new_timestamp =  new_timestamp + pd.Timedelta('00:00:01')

            new_row = [new_timestamp, start_hr, start_r ]
            writer_df = writer_df.append({'ts': new_timestamp , 'hr': start_hr , 'r': start_r} , ignore_index= True )
        start_index = start_index + 1

    writer_df.to_csv(writer_file, index = False)
    return new_path


def pick_BH_values_based_on_watch(start_time, end_time, device_2):
    # All the Bioharness csv file is in in_the_wild.csv
    # We want to read that, then pick those values between start_time and end_time that have a reported value from the device_2

    BH_csv_path = system_path + this_participant_path + "Bioharness/in_the_wild.csv"
    BH_picked_values_path = system_path + this_participant_path + "Bioharness/in_the_wild_picked_values.csv"
    writer_BH = open(BH_picked_values_path, "w")
    writer_BH.truncate()


    watch_to_pick_from_path = system_path + this_participant_path + device_2 + "/" + device_2 + "_in_the_wild_files/in_the_wild_hr_without_filling.csv"
    reader_watch = pd.read_csv(watch_to_pick_from_path)

    reader_BH = csv.reader(open(BH_csv_path, "r"))
    row1 = next(reader_BH)

    watch_timestamps = reader_watch["ts"]
    watch_timestamps = pd.to_datetime(watch_timestamps)
    watch_timestamps = watch_timestamps.apply(lambda t: t.replace(microsecond=0))
    watch_times = watch_timestamps.dt.time

    reader_BH_file = open(BH_csv_path, "r")
    reader_BH = pd.read_csv(reader_BH_file)
    BH_timestamps = reader_BH['Time']
    BH_timestamps = pd.to_datetime(BH_timestamps)
    BH_timestamps = BH_timestamps.apply(lambda t: t.replace(microsecond=0))
    BH_times = BH_timestamps.dt.time

    writer_BH_csv = csv.writer(writer_BH)
    writer_BH_csv.writerow(row1)

    i = 0
    for watch_time in watch_times:
        while i < BH_times.shape[0]:
            if watch_time == BH_times.iloc[i]:
                writer_BH_csv.writerow(reader_BH.iloc[i])
                break
            i += 1

    writer_BH.close()
    df= pd.read_csv(BH_picked_values_path)
    titles_list = df.columns.tolist()
    df = df.reset_index(drop = True)
    df = df.drop([titles_list[0]], axis = 1)
    df.to_csv(BH_picked_values_path)

    
def find_chunks_from_device_HR(start_time_unix, end_time_unix , device,  new_time, fill = None):
    print "here in find chunks"

    start_timestamp = pd.to_datetime(start_time_unix, unit= "s")
    end_timestamp = pd.to_datetime(end_time_unix, unit = "s")

    if new_time:
        start_timestamp = start_timestamp - pd.Timedelta('04:00:00')
        end_timestamp = end_timestamp - pd.Timedelta('04:00:00')

    else:
        start_timestamp = start_timestamp - pd.Timedelta('05:00:00')
        end_timestamp = end_timestamp - pd.Timedelta('05:00:00')

    start_time = start_timestamp.time()
    end_time = end_timestamp.time()

    if device =="Bioharness":
        device_csv_path = system_path + this_participant_path + device +  "/" + BH_summary_file
        device_in_the_wild_chunk_path = system_path + this_participant_path + device + "/" + "in_the_wild.csv"
        device_file = open(device_csv_path, "r")
        reader = csv.reader(device_file)
        titles = next(reader)  # reads the first row of the BH file which is the column titles

    else:

        # If it is supposed to do it without filling the rows, it will read the all_hr_data file
        # and it will store the in the wild chunk in the 'in_the_wild_hr_without_filling.csv'
        if fill == False:
            device_csv_path = system_path + this_participant_path + device + "/" + device + "_csv_files/" + "all_hr_data.csv"
            device_in_the_wild_chunk_path = system_path + this_participant_path + device + "/" + device + "_in_the_wild_files/" + "in_the_wild_hr_without_filling.csv"

        # If it is supposed to do it with filling the rows, it will read the all_hr_data_complete file
        # and it will store the in the wild chunk in the 'in_the_wild_hr_with_filling.csv'
        elif fill == True:
            device_csv_path = system_path + this_participant_path + device + "/" + device + "_csv_files/" + "all_hr_data_complete.csv"
            device_in_the_wild_chunk_path = system_path + this_participant_path + device + "/" + device + "_in_the_wild_files/" + "in_the_wild_hr_with_filling.csv"

        titles = ["ts", "hr", "r"]

    device_in_the_wild_chunk_file = open(device_in_the_wild_chunk_path, "w")
    device_in_the_wild_chunk_file.truncate()

    device_csv_reader = pd.read_csv(device_csv_path)

    # we want to read the lines of the device_csv_reader  find the timestamp that matches start_time and the timestamp that matches the end_time
    # and write all the lines in between in the device_in_the_wild_chunk_file

    device_timestamps = device_csv_reader.iloc[:,0]
    device_timestamps = pd.to_datetime(device_timestamps)
    device_times = device_timestamps.dt.time

    device_times_no_ms = device_times.apply(lambda x: x.replace(microsecond=0)) # removing the milliseconds

    start_row_num = 0
    end_row_num = 0

    found_exact_start = False
    found_exact_end = False


    # Look for an exact match of the start time and the end time among the timestamps
    for row_num in range(device_times_no_ms.size):
        if start_time == device_times_no_ms[row_num]:
            print "in found exact. the device start time is: ", device_times_no_ms[row_num]
            start_row_num = row_num
            found_exact_start = True

        if end_time == device_times_no_ms[row_num]:
            print "in found exact. the device end time is: ", device_times_no_ms[row_num]
            end_row_num = row_num
            found_exact_end = True

        if found_exact_start and found_exact_end:
            break

    if found_exact_start == False:  # for this time in app log, we have checked all the file and there is not exact timestamp entry
        for row_num in range(device_times_no_ms.size):
            if device_times_no_ms[row_num] > start_time:
                start_row_num = row_num
                break

    if found_exact_end == False:  # for this time in app log, we have checked all the file and there is not exact timestamp entry
        for row_num in range(device_times_no_ms.size):
            if device_times_no_ms[row_num] > end_time:
                end_row_num = row_num
                break

    print "start time is: ", start_time
    print " devices start time is: " , device_times_no_ms[start_row_num]


    print "end time is: ", end_time
    print " devices end time is: ", device_times_no_ms[end_row_num]


    writer = csv.writer(device_in_the_wild_chunk_file)
    writer.writerow(titles)

    # write whatever is between the start_row_num and end_row_num
    for counter, row in device_csv_reader.iterrows():
        if counter < start_row_num: continue
        if counter >= end_row_num: break
        writer.writerow(row)



def convert_str_to_unix(datetime_in_str):
    # We have the timestamp in a string format and we want to convert it to unix time
    # Format of the str timestamp:  May 4th, 2018 at 15:34:12 would be 20180504153412

    print "date and time in str is: ", datetime_in_str
    return int(time.mktime(datetime.strptime(datetime_in_str , "%Y%m%d%H%M%S").timetuple()))



def fix_BH_in_the_wild(system_path, this_participant_path):
     # we have the watch after filling the missing rows and the BH in the wild chunk
    # but there might be a few rows in BH that are not in watch. Because watch timestamps did not exactly match the starting time in time_range

    BH_wild_csv_path = system_path + this_participant_path + "Bioharness/in_the_wild.csv"
    BH_wild_df = pd.read_csv(BH_wild_csv_path)
    BH_time = BH_wild_df['Time']
    watch_wild_with_filling_path = system_path + this_participant_path + "watch1/watch1_in_the_wild_files/in_the_wild_hr_with_filling.csv"
    watch_wild_df = pd.read_csv(watch_wild_with_filling_path)
    watch_time = pd.to_datetime(watch_wild_df['ts'])
    found = False

    #print "watch time is: ", type(watch_time[0])

    watch_first_row = watch_time[0]
    print watch_first_row
    index_list = []
    index = 0
    while found == False:
        bh_t= BH_time[index].split(".")[0]
        bh_t= pd.to_datetime(bh_t)
        print "bh_t is: ", bh_t

        if bh_t == watch_first_row:
            found = True
        else:
            index_list.append(index)
        index += 1

    BH_wild_df = BH_wild_df.drop(BH_time.index[index_list])
    BH_wild_df.to_csv(BH_wild_csv_path)


def get_wild_chunk_from_BH(start_time, end_time, newtime):

    # i want to use the time range file to get the chunk of BH data points that are within that range
    # read start time, convert to datetime
    # read end time, convert to datetime
    # read BH, once it finds the start time keep adding lines until it reaches the end time
    # general_statistics.py has some formatting code samples that could help

    if newtime:
        start_time =  pd.to_datetime(start_time, unit = 's')-pd.Timedelta('04:00:00')
        end_time = pd.to_datetime(end_time, unit='s') - pd.Timedelta('04:00:00')
    else:
        start_time = pd.to_datetime(start_time, unit='s') - pd.Timedelta('05:00:00')
        end_time = pd.to_datetime(end_time, unit='s') - pd.Timedelta('05:00:00')

    print "start time is: ", start_time
    print "end time is: ",end_time

    BH_csv_path = system_path + this_participant_path + "Bioharness/shifted_times_log.csv"
    BH_csv_df = pd.read_csv(BH_csv_path)

    BH_csv_df['Time'] = pd.to_datetime(BH_csv_df['Time']).apply(lambda x: x.replace(microsecond=0))  # remove ms

    BH_wild_path = system_path + this_participant_path + "Bioharness/shifted_times_log_wild.csv"
    BH_wild_df = pd.DataFrame(columns=['Time', 'HR'])

    start_index_in_BH = 0
    end_index_in_BH = 0

    for i in range(len(BH_csv_df)):

        if BH_csv_df['Time'].iloc[i] == start_time:
            start_index_in_BH = i
        if BH_csv_df['Time'].iloc[i] == end_time:
            end_index_in_BH = i


    for i in range(start_index_in_BH, end_index_in_BH+1):
        time = BH_csv_df['Time'].iloc[i]
        hr = BH_csv_df['HR'].iloc[i]
        BH_wild_df = BH_wild_df.append(pd.DataFrame([[time, hr]], columns=['Time', 'HR']), ignore_index=True)

    BH_wild_df.to_csv(BH_wild_path, columns=['Time', 'HR'])


def get_wild_chunk_from_BH_exception_ID05(start_time, end_time, newtime):

    if newtime:
        start_time =  pd.to_datetime(start_time, unit = 's')-pd.Timedelta('04:00:00')
        end_time = pd.to_datetime(end_time, unit='s') - pd.Timedelta('04:00:00')
    else:
        start_time = pd.to_datetime(start_time, unit='s') - pd.Timedelta('05:00:00')
        end_time = pd.to_datetime(end_time, unit='s') - pd.Timedelta('05:00:00')

    print "start time is: ", start_time
    print "end time is: ",end_time

    BH_csv_path_1 = system_path + this_participant_path + "Bioharness/shifted_times_log.csv"
    BH_csv_df_1 = pd.read_csv(BH_csv_path_1)

    BH_csv_path_2 = system_path + this_participant_path + "Bioharness/shifted_times_log2.csv"
    BH_csv_df_2 = pd.read_csv(BH_csv_path_2)

    BH_csv_df_1['Time'] = pd.to_datetime(BH_csv_df_1['Time']).apply(lambda x: x.replace(microsecond=0))  # remove ms
    BH_csv_df_2['Time'] = pd.to_datetime(BH_csv_df_2['Time']).apply(lambda x: x.replace(microsecond=0))  # remove ms

    BH_wild_path = system_path + this_participant_path + "Bioharness/shifted_times_log_wild.csv"
    BH_wild_df = pd.DataFrame(columns=['Time', 'HR'])

    start_index_in_BH = 0
    end_index_in_BH = 0

    for i in range(len(BH_csv_df_1)):
        if BH_csv_df_1['Time'].iloc[i] == start_time:
            start_index_in_BH = i

    for i in range(start_index_in_BH, BH_csv_df_1.shape[0]):
        time = BH_csv_df_1['Time'].iloc[i]
        hr = BH_csv_df_1['HR'].iloc[i]
        BH_wild_df = BH_wild_df.append(pd.DataFrame([[time, hr]], columns=['Time', 'HR']), ignore_index=True)


    for i in range(len(BH_csv_df_1)):
        if BH_csv_df_2['Time'].iloc[i] == end_time:
            end_index_in_BH = i

    for i in range(0, end_index_in_BH + 1):
        time = BH_csv_df_2['Time'].iloc[i]
        hr = BH_csv_df_2['HR'].iloc[i]
        BH_wild_df = BH_wild_df.append(pd.DataFrame([[time, hr]], columns=['Time', 'HR']), ignore_index=True)


    BH_wild_df.to_csv(BH_wild_path, columns=['Time', 'HR'])











if __name__ == '__main__':
    # We already have the BH csv file.
    # We have so many little json files from watch and phone.
    # We have a text file called time_range that has the str format of the start time and end time of the 'in the wild' part of the experiment

    time_range_path = system_path + this_participant_path + "time_range"
    time_range_file = open(time_range_path, "r")

    start_time = time_range_file.readline().strip()
    end_time = time_range_file.readline().strip()

    # Converting the str start time and end time into unix time
    start_time_unix = convert_str_to_unix(start_time)
    end_time_unix = convert_str_to_unix(end_time)

    print start_time_unix
    print end_time_unix

    #get_wild_chunk_from_BH(start_time_unix, end_time_unix, NEW_TIME)

    get_wild_chunk_from_BH_exception_ID05(start_time_unix, end_time_unix, NEW_TIME)

    watch_list = ["watch1"]

    """BH_in_the_wild_path = ""
    # using the start time and end time, getting the chunk of Bioharness date that is within that range
    # We will save the chunk in a csv file called 'in_the_wild_hr.csv'
    find_chunks_from_device_HR(start_time_unix, end_time_unix, "Bioharness", NEW_TIME)
    """

    #for watch in watch_list:

        # This creates all_hr data, all_accel_data, all_gyro_data csv files
        # The all_hr_data is without filling the gaps
        #all_watch_data = watch_csv_creator(watch, NEW_TIME)

        # This creates all_hr_data_complete which is with filling the gaps
        #all_hr_data_complete_path = fill_missing_rows_in_file(all_watch_data[0])

        # Here we find the 'in the wild' chunk of the watch data with 'fill' parameter as 'False'
        # So it is without filling the gaps and it will use the all_hr_data csv file
        #find_chunks_from_device_HR(start_time_unix, end_time_unix, watch, NEW_TIME, False)

        # Here we find the 'in the wild' chunk of the watch data with 'fill' parameter as 'True'
        # So it is with filling the gaps and it will use the all_hr_data_complete csv file
        #find_chunks_from_device_HR(start_time_unix , end_time_unix , watch , NEW_TIME, True)


    # Using the start time and end time, getting the chunk of Bioharness date that is within that range
    # We will save the chunk in a csv file called 'in_the_wild_hr.csv'
    #find_chunks_from_device_HR(start_time_unix, end_time_unix, "Bioharness", NEW_TIME)

    # We have a start time and an end time from the time_range.txt
    # Bioharness definitely has timestamps that match these two times because it has a value for every seocond
    # But for the watch, it might not have the exact time stamp as the start time and it might start from a later time.
    # So we have to remove those timestamps in the Bioharness that are before the actual start time of the watch data
    #fix_BH_in_the_wild(system_path, this_participant_path)

    # Now, to do the 'without imputating' version, we want to pick those time stamps from the BH that have an exact match in the watch data
    #pick_BH_values_based_on_watch(start_time_unix, end_time_unix, "watch1")
    #test(system_path + this_participant_path + "Bioharness/in_the_wild_picked_values.csv")

    # ----------- the code for the phone and also for accel and gyro from the watch needs to be completed -------------#


    """
    all_phone_data = phone_csv_creator(NEW_TIME)  # all_accel_data is at index 0 and all_gyro_data is at index 1

    BH_wild_path = system_path + this_participant_path + "Bioharness/in_the_wild.csv"
    BH_wild_picked_path = system_path + this_participant_path + "Bioharness/in_the_wild_picked_values.csv"
    watch1_with_filling_path =  system_path + this_participant_path + "watch1/watch1_in_the_wild_files/in_the_wild_hr_with_filling.csv"
    watch1_without_filling_path =  system_path + this_participant_path + "watch1/watch1_in_the_wild_files/in_the_wild_hr_without_filling.csv"
    general_device.check_file_length_match(BH_wild_path, BH_wild_picked_path, watch1_with_filling_path, watch1_without_filling_path)
    """