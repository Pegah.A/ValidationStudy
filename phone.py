import os
from helpers.SensorHelper import *

def phone_csv_creator(system_path, this_participant_path, new_time):
    # First, read all .json files, take the accel data and put it into a csv file. Take the gyro data and put it into a csv file
    # Second, combine all of the accel data files into one csv and combine all of th e gyro data file into one csv

    list_of_files = []
    for file in os.listdir(system_path + this_participant_path + "phone"):
        if file.endswith(".json"):
            list_of_files.append(file)

    for item in list_of_files:
        file_name = str(item).split(".")[0]
        file_path = system_path + this_participant_path + "phone/" + file_name + ".json"
        accel_file_path = system_path + this_participant_path + "phone/phone_csv_files/ " + file_name + "_accel.csv"
        gyro_file_path = system_path + this_participant_path + "phone/phone_csv_files/ " + file_name + "_gyro.csv"

        data = SensorJson(file_path)
        accel = data.get("accel")
        gyro = data.get("gyro")


        if new_time:
            accel.index =accel.index - pd.Timedelta('04:00:00')
            gyro.index= gyro.index - pd.Timedelta('04:00:00')
        else:
            accel.index = accel.index - pd.Timedelta('05:00:00')
            gyro.index = gyro.index - pd.Timedelta('05:00:00')

        accel.to_csv(accel_file_path)
        gyro.to_csv(gyro_file_path)

    all_accel_path = system_path + this_participant_path + "phone/phone_csv_files/all_accel_data.csv"
    all_gyro_path = system_path + this_participant_path + "phone/phone_csv_files/all_gyro_data.csv"

    accel_files_list = []
    gyro_files_list = []

    for file in os.listdir(system_path + this_participant_path + "phone/phone_csv_files"):

        if file.endswith("accel.csv"):
            accel_files_list.append(file)
        elif file.endswith("gyro.csv"):
            gyro_files_list.append(file)

    accel_files_list.sort()
    gyro_files_list.sort()


    for i in range (len(accel_files_list)):
        accel_files_list[i]=open(system_path + this_participant_path + "phone/phone_csv_files/" + accel_files_list[i], "r")

    for i in range (len(gyro_files_list)):
        gyro_files_list[i]=open(system_path + this_participant_path + "phone/phone_csv_files/" + gyro_files_list[i], "r")


    combined_csv_accel = pd.concat([pd.read_csv(f) for f in accel_files_list])
    combined_csv_accel.to_csv(all_accel_path, index=False)

    combined_csv_gyro = pd.concat([pd.read_csv(f) for f in gyro_files_list])
    combined_csv_gyro.to_csv(all_gyro_path, index=False)

    return all_accel_path , all_gyro_path

