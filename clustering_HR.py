import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import pandas as pd
#add test

# I want to import the main .csv file here which has all the data.
# Then I want to generate 9 new csv files from it, each for one activity.
# And I want them to have the following columns:
# ts, ID, activity, BH

activities= ["sixmwt", "sitting" , "walking" ,"lying", "standing", "eating", "brushing"] #, "climbup", "climbdown"]
healthy_IDs = []
COPD_IDs = []

system_path = "/Users/pegah/Documents/VS/"
def generate_csv_for_each_activity(dataframe):

    # Until July 3rd, 7 healthy participants, 7 COPD (BH from 14 COPD is completely invalid, BH from 10 is invalid in 6mwt)
    # ==> 7 healthy, 6 COPD     for sixmwt: 7 healthy, 5 COPD

    for activity in activities:

        activity_csv_path = "/Users/pegah/Documents/VS/main_csv_files/" + activity+ "_csv_file.csv"
        df = pd.DataFrame(columns=['ts', 'ID','COPD','activity', 'BH'], )
        section = dataframe.query('activity==@activity')


        df.ts = section.ts
        df.ID = section.ID
        df.activity = activity
        df.BH = section.BH
        df.COPD = section.COPD

        df = df[df.ID != 'ID14']  # Because the heart rate values reported from BH are invalid for this person.

        if activity  == "sixmwt":
            df = df[df.ID != 'ID10']   # In this activity, heart rate values from Bioharness are invalid for this person.

        df.to_csv(activity_csv_path , index = False)


def plot_HR_per_healthiness():

    for activity in activities:

        activity_csv_path = "/Users/pegah/Documents/VS/main_csv_files/" + activity + "_csv_file.csv"
        df = pd.read_csv(activity_csv_path, index_col=0, parse_dates= ['ts'])

        healthy_HR_list = df[df.COPD == False].BH.tolist()
        COPD_HR_list = df[df.COPD == True].BH.tolist()

        plt.figure(figsize=(10, 8))

        plt.plot(healthy_HR_list, healthy_HR_list, 'o', color= 'navy', label="Healthy participants")
        plt.plot(COPD_HR_list, COPD_HR_list, '.', color='tomato', label="COPD patients")


        plt.title("Heart rate values reported by the Bioharness for healthy participants and COPD patients"
                  "\nActivity: " + activity +"\n")
        plt.ylabel("Heart rate value reported by the Bioharness (bpm)")

        frame1 = plt.gca()
        frame1.axes.get_xaxis().set_visible(False)

        plt.legend()

        fig_name =  system_path + "graphs/clustering_2/" + "BH_Healthy_COPD_" + activity + ".png"
        plt.savefig(fig_name)

        #plt.show()


def plot_high_rel_percentage_per_healthiness(window_size):

    for activity in activities:
        activity_csv_path = system_path + "BA_res/clustering_files/reliability_count_info_"\
                            + activity+ "_window_" + str(window_size) +".csv"
        df = pd.read_csv(activity_csv_path, index_col=0)

        healthy_high_rel_percent = df[df.COPD == False]['High_Rel_%'].tolist()
        COPD_high_rel_percent = df[df.COPD == True]['High_Rel_%'].tolist()


        plt.figure(figsize=(10, 8))

        plt.plot(healthy_high_rel_percent, 'o', color='navy', label="Healthy participants")
        plt.plot(COPD_high_rel_percent, '.', color='tomato', label="COPD patients")

        plt.title("Percentage of heart rate values reported with high reliability for healthy participants and COPD patients"
                  "\nActivity: " + activity + "\n")
        plt.ylabel("Percentage of highly reliable heart rate reports ")

        frame1 = plt.gca()
        frame1.axes.get_xaxis().set_visible(False)

        plt.legend()

        fig_name = system_path + "graphs/clustering_2/" + "high_rel_Healthy_COPD_" \
                   + activity + "_window_" + str(window_size)+".png"
        plt.savefig(fig_name)

        #plt.show()



main_csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
main_csv_df = pd.read_csv(main_csv_path, index_col=0, parse_dates=['ts'])
#generate_csv_for_each_activity(main_csv_df)
#plot_HR_per_healthiness()
plot_high_rel_percentage_per_healthiness(60)