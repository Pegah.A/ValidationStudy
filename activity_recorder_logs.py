import pandas as pd

#checking changes

def read_logs_from_app(system_path, this_participant_path,app_logs_file, new_time):

    activity_recorder_path = system_path + this_participant_path + app_logs_file
    activity_logs_csv_path = system_path + this_participant_path + "logs_" + this_participant_path.split("/")[0] +".csv"
    content = pd.read_json(activity_recorder_path)

    if new_time == True:

        content['timestamp'] = content['timestamp'] - pd.Timedelta('04:00:00') # the app gives GMT time which is +4:00 from Toronto time
    else:
        content['timestamp'] = content['timestamp'] - pd.Timedelta('05:00:00') # the app gives GMT time which is +5:00 from Toronto time

    content.to_csv(activity_logs_csv_path)
    # if the participant performs all the 9 activities, the logs.csv should have 20 lines.

    return activity_logs_csv_path
