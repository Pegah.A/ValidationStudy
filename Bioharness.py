import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import csv
import pandas as pd
import numpy as np

activities = ["sixmwt", "sitting" , "walking" , "lying" , "standing", "eating" , "brushing", "climbup", "climbdown"]
ID_list = ['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID10','ID12','ID13', 'ID15', 'ID16' ]

# creates a csv file for each activity which will have all the summary information
def bh_activity_csv_creator(system_path, this_participant_path, row_chunk_nums, BH_logs_path):
    all_files = []

    # csv files for each of the 9 activities
    for activity in activities:
        path = system_path + this_participant_path + "Bioharness/Bioharness_csv_files/" + activity + ".csv"
        file = open(path, "w")
        all_files.append(file)

    BH_file = open(BH_logs_path, "r")
    reader = csv.reader(BH_file)
    row1 = next(reader) # reads the first row of the BH file which is the column titles

    for item in all_files:
        item.truncate()
        writer = csv.writer(item)
        writer.writerow(row1)  # writes the column title row to the top of each file


    start_index = 1  #starting from 6MWT and not "experiment start"
    end_index = 2
    file_counter= 0


    BH_logs_file = open(BH_logs_path, "r")

    while(file_counter < 9):
        reader= csv.reader(BH_logs_file)
        BH_logs_file.seek(0)  # to reset the file iterator, otherwise it will iterate through the BH file only once and will fill only the first file.
        writer= csv.writer(all_files[file_counter])


        # we know the range for this specific activity from the row division that has been executed previously
        for counter, row in enumerate(reader):
            if counter<row_chunk_nums[start_index]: continue
            if counter >= row_chunk_nums[end_index]: break
            writer.writerow(row)

        start_index += 2  # because we have to skip the end log of the current activity to get to the start log of the next one
        end_index += 2
        file_counter += 1

def bh_activity_csv_creator_pick_values_based_on_watch_data(system_path, this_participant_path, watch):

    # For each activity, go to its file in the watch1 folder
    # Read each line of that file, find it in the Bioharness file and write it in the new Bioharness file

    for activity in activities:
        new_path = system_path + this_participant_path + "Bioharness/Bioharness_csv_files/picked_values_based_on_" + watch +"/" + activity + "_picked_values.csv"
        writer_BH = open(new_path, "w")
        writer_BH.truncate()

        corresponding_watch_path = system_path + this_participant_path + watch + "/" + watch + "_hr_activity_sep_files/without_filling/" + activity + "_hr.csv"
        reader_watch = pd.read_csv(corresponding_watch_path)

        corresponding_BH_path = system_path + this_participant_path + "Bioharness/Bioharness_csv_files/" + activity + ".csv"
        reader_BH = csv.reader(open(corresponding_BH_path,"r"))

        row1 = next(reader_BH)  # reads the first row of the BH file which is the column titles

        watch_timestamps = reader_watch["ts"]
        watch_timestamps = pd.to_datetime(watch_timestamps)
        watch_timestamps = watch_timestamps.apply(lambda t: t.replace(microsecond=0)) # to remove the milliseconds
        watch_times = watch_timestamps.dt.time

        reader_BH_file = open(corresponding_BH_path, "r")
        reader_BH = pd.read_csv(reader_BH_file)
        BH_timestamps = reader_BH['Time']
        BH_timestamps = pd.to_datetime(BH_timestamps)
        BH_timestamps = BH_timestamps.apply(lambda t: t.replace(microsecond=0)) # to remove the milliseconds
        BH_times= BH_timestamps.dt.time

        writer_BH= csv.writer(writer_BH)
        writer_BH.writerow(row1)


        for watch_time in watch_times:
            for i in range(BH_times.shape[0]):
                print "watch time is: ", watch_time
                print "BH time is: ", BH_times.iloc[i]
                if watch_time == BH_times.iloc[i]:
                    print "found match"
                    print "watch time is: ", watch_time
                    print "BH time is: ", BH_times.iloc[i]

                    writer_BH.writerow(reader_BH.iloc[i])
                    break # when it finds a match in the BH file for a datapoint in the watch,it doesn't need to look through the rest of the BH file


def bh_change_timestamps(system_path, this_participant_path, BH_file_name):
    # the Bioharness tends to be 10 seconds faster than the watch. This function adjusts the timestamps in the Bioharness
    # It will create a new csv file called shifted_times_log.csv
    # This file only has timestamps and heart rate. Other info from Bioharness such as accel, gyro, breathing, ... are not included


    corresponding_BH_path = system_path + this_participant_path + "Bioharness/" + BH_file_name
    corresponding_BH_path_2 = system_path +this_participant_path + "Bioharness/" + "shifted_times_log.csv"

    new_BH = pd.DataFrame(columns=['Time', 'HR'])
    reader_BH = pd.read_csv(corresponding_BH_path)

    BH_timestamps = reader_BH['Time']

    BH_timestamps = pd.to_datetime(BH_timestamps, dayfirst = True)

    new_BH_timestamps = BH_timestamps - pd.Timedelta(seconds=10)
    new_BH['Time'] = new_BH_timestamps
    new_BH['HR'] = reader_BH['HR']
    new_BH.set_index('Time', inplace=True)

    new_BH.to_csv(corresponding_BH_path_2)




def HR_from_BH_COPD_vs_non_COPD_wild(dataframe):
    # I have the main_csv_file of the wild data.
    # I want to read the BH column for COPD and nonCOPD
    # Have one list for COPD another list for nonCOPD
    # Draw hist  calculate min std variance
    # Who is invalid in BH? ID11 and ID14


    invalid_ID_1 = 'ID14'
    invalid_ID_2 = 'ID11'
    section = dataframe.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD'] == False

    COPD_section = section[COPD_condition]
    non_COPD_section = section[non_COPD_condition]

    COPD_BH = COPD_section['BH']
    non_COPD_BH = non_COPD_section['BH']



    for i in range(len(non_COPD_BH)):
        if np.isnan(non_COPD_BH.iloc[i])== True:
            print "nan for non_COPD"

    for i in range(len(COPD_BH)):
        if np.isnan(COPD_BH.iloc[i])== True:
            print "nan for COPD"

    print "mean of COOD is: ", np.mean(COPD_BH)
    print "mean of non-COPD is: ", np.mean(non_COPD_BH)
    print "std of COPD is: ", np.std(COPD_BH)
    print "std of non-COPD is: ", np.std(non_COPD_BH)


    """
    # ------ plotting ------------

    matplotlib.rcParams.update({'font.size': 16})

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_ylim([0, 35000])

    plt.hist(non_COPD_BH, color = 'deeppink')
    plt.title('Histogram of Heart Rate Values Reported by the Bioharness\n Non-COPD Participants \n Uncontrolled Setting')
    plt.xlabel('Heart Rate (bpm)')

    fig_name = "/Users/pegah/Documents/VS/graphs/wild/BH_HR_non_COPD_wild_hist.png"
    #plt.savefig(fig_name)
    plt.show()

    #--------
    
    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_ylim([0, 35000])

    plt.hist(COPD_BH, color= 'darkslateblue')
    plt.title('Histogram of Heart Rate Values Reported by the Bioharness\n COPD Participants \n Uncontrolled Setting')
    plt.xlabel('Heart Rate (bpm)')

    fig_name = "/Users/pegah/Documents/VS/graphs/wild/BH_HR_COPD_wild_hist.png"
    plt.savefig(fig_name)
    #plt.show()

    # ------ end of plotting------------
    """


#bh_change_timestamps(system_path, this_participant_path, BH_file_name)

wild_csv_path  = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild.csv"
wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])

HR_from_BH_COPD_vs_non_COPD_wild(wild_df)
