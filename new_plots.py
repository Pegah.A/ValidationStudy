import matplotlib
matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pickle

# alpha  transparency


system_path = "/home/pegah/ValidationStudy/participant_data/"

color_map_dict_1 = {3: "darkblue" , 2: "darkmagenta" , 1: "cornflowerblue", 0: "darkturquoise"}
#color_map_dict_2 = {3: "b" , 2: "c" , 1: "magenta", 0: "red"}



def plot_BH_watch_overtime(ID, activity, dataframe):
    section = dataframe.query('activity==@activity and ID == @ID')
    new_section = dataframe.query('activity==@activity and ID == @ID')
    BH = section['BH']
    time = section['ts']


    cols = ['watch1' , 'rel1']
    new_section[cols] = section[cols].ffill()


    #time_index = np.arange(time.shape[0])
    plt.rcParams["figure.figsize"] = [10, 10]
    #plt.figure(sharex = True)
    _, axs = plt.subplots(3,1,sharex= True)

    #plt.subplot()
    plt.sca(axs[0])
    plt.grid()
    plt.plot(time, BH, "seagreen", label = 'BH')
    plt.plot(time, new_section['watch1'], "mediumblue", label = "watch1")
    for rel,sub_section in section.groupby('rel1'):
        plt.plot(sub_section.ts, sub_section.watch1 , "o" , color = color_map_dict_1[rel], label = 'watch rel: {}'.format(int(rel)))

    #for rel, sub_section in section.groupby('rel2'):
        #plt.plot(sub_section.ts, sub_section.watch2, "x", color = color_map_dict_2[rel], label = 'watch2{} '.format(rel))

    #plt.xlabel('Time (seconds)')
    plt.ylabel('Heart Rate (bpm)')
    plt.title('Heart Rate value reported by Bioharness and watch \n ACtivity: ' + activity + '\n Participant ID: ' + ID)
    #plt.xticks(rotation = 15)
    plt.legend()
    plt.tight_layout()


    #plt.subplot(3,1,2)
    plt.sca(axs[1])
    accel_mag_1 = section['accel_mag_1']
    plt.grid()
    plt.plot(time,accel_mag_1 , "lightcoral", label='accel magnitude')
    for rel, sub_section in section.groupby('rel1'):
        plt.plot(sub_section.ts, sub_section.accel_mag_1, "o", color=color_map_dict_1[rel],
                 label='watch rel: {}'.format(int(rel)))

        # for rel, sub_section in section.groupby('rel2'):
        # plt.plot(sub_section.ts, sub_section.watch2, "x", color = color_map_dict_2[rel], label = 'watch2{} '.format(rel))

    #plt.xlabel('Time (seconds)')
    plt.ylabel('Accelerometer Vector Magnitude (m/s^2)')
    #plt.title('Heart Rate value reported by Bioharness and watch \n ACtivity: ' + activity + '\n Participant ID: ' + ID)
    #plt.xticks(rotation=15)
    plt.legend()
    plt.tight_layout()


    #plt.subplot(3,1,3)
    plt.sca(axs[2])
    gyro_mag_1 = section['gyro_mag_1']
    plt.grid()
    plt.plot(time, gyro_mag_1, "orchid", label='gyro magnitude')
    for rel, sub_section in section.groupby('rel1'):
        plt.plot(sub_section.ts, sub_section.gyro_mag_1, "o", color=color_map_dict_1[rel],
                 label='watch rel: {}'.format(int(rel)))

        # for rel, sub_section in section.groupby('rel2'):
        # plt.plot(sub_section.ts, sub_section.watch2, "x", color = color_map_dict_2[rel], label = 'watch2{} '.format(rel))

    plt.xlabel('Time (seconds)')
    plt.ylabel('Gyroscope Vector Magnitude')
    # plt.title('Heart Rate value reported by Bioharness and watch \n ACtivity: ' + activity + '\n Participant ID: ' + ID)
    plt.xticks(rotation=15)
    plt.legend()
    plt.tight_layout()


    fig_name =  system_path + "graphs_accel_gyro/ffill/" + "BH_watch_overtime_" + activity + "_" + ID + ".png"
    plt.savefig(fig_name)
    gcf = plt.gcf()
    pickle.dump(gcf, open(fig_name + '.pkl', 'wb'), pickle.HIGHEST_PROTOCOL)







activity_list = ["sixmwt", "sitting" , "walking" , "lying" , "standing", "eating" , "brushing", "climbup", "climbdown"]
ID_list =['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID10','ID12' ]

csv_path = "/home/pegah/ValidationStudy/participant_data/main_csv_files/main_csv_file_with_all_data.csv"
df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])

#for ID in ID_list:
    #for activity in activity_list:
ID ='ID10'
activity = "walking"
plot_BH_watch_overtime(ID,activity, df)
plt.show()