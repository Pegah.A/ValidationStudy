import pandas as pd
import time
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

import numpy as np
import math
import csv
import scipy
from scipy.stats.kde import gaussian_kde
from scipy.stats import f_oneway
from scipy.stats import ttest_ind
from scipy.stats import pearsonr
from numpy import linspace

system_path = "/home/pegah/ValidationStudy/participant_data/"


#---------------------------  what I had before
def plot_BH_vs_watch_value_at_each_time_controlled_across_participants(imputating):
    # deciding with general csv file to use.
    if imputating:
        general_csv_path = system_path + "main_csv_files/main_csv_file_with_data_imputating.csv"
        imputate = "True"
    else:
        general_csv_path = system_path + "main_csv_files/main_csv_file_without_data_imputating.csv"
        imputate = "False"

    general_csv_df = pd.read_csv(general_csv_path)
    general_csv_len = general_csv_df.shape[0]

    # the activity column of the general csv file
    activity_values = general_csv_df['activity']

    # creating a list of possible activities (10, including unknown for the 'in the wild' part)
    activities = []
    for item in activity_values:
        if item not in activities:
            activities.append(item)

    BH_values = general_csv_df['BH']  # the BH column of the general csv file
    watch1_values = general_csv_df['watch1']  # the watch1 column of the general csv file
    rel1_values = general_csv_df['rel1']  # the rel1 column of the general csv file

    # a dictionary for BH values. Keys are activities. The values will be a list of 'all' the datapoints for that activity for all participant
    # in other words, 'any' BH value that we have for each activity
    # The same for watch1 and rel1

    BH_dict = dict((el, []) for el in activities)
    watch1_dict = dict((el, []) for el in activities)
    rel1_dict = dict((el, []) for el in activities)

    # for every row in the general csv file, I read the activity, then I read the BH value, watch1 value and rel1 value and add it to the dictionaries
    # as the values for the key = activity
    # Because I am appending to a list, the order should be fine
    for i in range(general_csv_len):
        activity = activity_values[i]
        BH_dict[activity].append(BH_values[i])
        watch1_dict[activity].append(watch1_values[i])
        rel1_dict[activity].append(rel1_values[i])

    # Now I want to draw the graphs per activity. For each activity I go to the dictionaries, get the list of BH, watch1, and rel1 values for that activity
    for activity in activities:
        BH = BH_dict[activity]
        watch1 = watch1_dict[activity]
        rel1 = rel1_dict[activity]

        # BH_high and watch1_high are BH and watch1 values for datapoints that have a high rel1. ( by high I mean either 2 or 3)
        BH_high = []
        watch1_high = []

        for i in range(len(rel1)):
            if int(rel1[i]) > 1:
                BH_high.append(BH[i])
                watch1_high.append(watch1[i])

        plt.rcParams["figure.figsize"] = [10, 8]
        plt.figure()
        plt.title("BH value vs watch value for " + activity + "\n Imputating: " + imputate + "\n All reliabilities.")

        # the x=y line in the graph for a better visualization
        xx = np.arange(60, 130)
        yy = np.arange(60, 130)
        plt.plot(xx, yy, "k")

        # Drawing watch values vs BH values for all the data points (no matter what the reliability is)
        plt.plot(watch1, BH, "bo")
        plt.xlabel("Heart Rate from watch (bpm)")
        plt.ylabel("Heart rate from BH (bpm")
        plt.grid()

        figure_name = system_path + "graphs/" + "BH_vs_watch_" + activity + "_Imputating_" + imputate + "_all_rel.png"
        plt.savefig(figure_name)

        # ----------------------------------  The following code are similar to above, but for data points with high reliability

        plt.rcParams["figure.figsize"] = [10, 8]
        plt.figure()
        plt.title("BH value vs watch value for " + activity + "\n Imputating: " + imputate + "\n High reliabilities.")

        xx = np.arange(60, 130)
        yy = np.arange(60, 130)
        plt.plot(xx, yy, "k")

        plt.plot(watch1_high, BH_high, "bo")
        plt.xlabel("Heart Rate from watch (bpm)")
        plt.ylabel("Heart rate from BH (bpm)")
        plt.grid()

        figure_name = system_path + "graphs/" + "BH_vs_watch_" + activity + "_Imputating_" + imputate + "_high_rel.png"
        plt.savefig(figure_name)



def plot_BH_vs_watch_value_at_each_time_controlled_per_participants(imputating):
    # deciding with general csv file to use.
    if imputating:
        general_csv_path = system_path + "main_csv_files/main_csv_file_with_data_imputating.csv"
        imputate = "True"
    else:
        general_csv_path = system_path + "main_csv_files/main_csv_file_without_data_imputating.csv"
        imputate = "False"

    general_csv_df = pd.read_csv(general_csv_path)
    general_csv_len = general_csv_df.shape[0]

    # the ID column of the general csv file
    ID_values = general_csv_df['ID']

    IDs = [] # list of all the IDs
    for item in ID_values:
        if item not in IDs:
            IDs.append(item)

    # the activity column of the general csv file
    general_activity_values = general_csv_df['activity']
    activities = [] ## list of all the activities ( 10 including unknown for 'in the wild' part)
    for item in general_activity_values:
        if item not in activities:
            activities.append(item)

    # For each participant:
    for ID in IDs:
        print "ID is: ", ID
        participant_id = general_csv_df['ID'] == ID

        # This is how you select dataframe rows using a condition. Here BH_values will be a list of BH values
        # taken from the BH column for the rows that had this specific ID that we are having in this iteration of the loop
        # The same for watch1, rel1 and activity
        # So these lists are specific to each participant
        BH_values = list(general_csv_df[participant_id]['BH'])
        watch1_values = list(general_csv_df[participant_id]['watch1'])
        rel1_values = list(general_csv_df[participant_id]['rel1'])
        activity_values= list(general_csv_df[participant_id]['activity'])

        # Now I want to create a dictionary for each participant. The keys of the dictionary are activities.
        BH_dict = dict((el, []) for el in activities)
        watch1_dict = dict((el, []) for el in activities)
        rel1_dict = dict((el, []) for el in activities)

        for i in range(len(BH_values)):
            activity = activity_values[i]
            BH_dict[activity].append(BH_values[i])
            watch1_dict[activity].append(watch1_values[i])
            rel1_dict[activity].append(rel1_values[i])

        for activity in activities:
            BH = BH_dict[activity]
            watch1 = watch1_dict[activity]
            rel1 = rel1_dict[activity]

            BH_high = []
            watch1_high = []

            for i in range(len(rel1)):
                if int(rel1[i]) > 1:
                    BH_high.append(BH[i])
                    watch1_high.append(watch1[i])

            plt.rcParams["figure.figsize"] = [10, 8]
            plt.figure()
            plt.title(
                "BH value vs watch value for " + activity + "\n Imputating: " + imputate + "\n All reliabilities." + "\n ID: " + ID)

            xx = np.arange(60, 130)
            yy = np.arange(60, 130)
            plt.plot(xx, yy, "k")

            plt.plot(watch1, BH, "bo")

            plt.xlabel("Heart Rate from watch (bpm)")
            plt.ylabel("Heart rate from BH (bpm)")
            plt.grid()
            figure_name = system_path + "graphs/" + "BH_vs_watch_" + activity + "_Imputating_" + imputate + "_all_rel_" + ID + ".png"
            plt.savefig(figure_name)
            # ----------------------------------
            plt.rcParams["figure.figsize"] = [10, 8]
            plt.figure()
            plt.title(
                "BH value vs watch value for " + activity + "\n Imputating: " + imputate + "\n High reliabilities." + "\n ID: " + ID)

            xx = np.arange(60, 130)
            yy = np.arange(60, 130)
            plt.plot(xx, yy, "k")

            plt.plot(watch1_high, BH_high, "bo")

            plt.xlabel("Heart Rate from watch (bpm)")
            plt.ylabel("Heart rate from BH (bpm")
            plt.grid()

            figure_name = system_path + "graphs/" + "BH_vs_watch_" + activity + "_Imputating_" + imputate + "_high_rel_" + ID + ".png"
            plt.savefig(figure_name)




#def plot_BH_vs_watch_value_at_each_time_wild():



#def plot_BH_watch_diff_at_each_time_controlled():



#def plot_BH_watch_diff_at_each_time_wild():



#---------------------------- New graphs

def plot_diff_vs_avg_per_activity_across_participants(imputating):
    # The x axis is going to be the average of the hear rate values, the y axis will be the difference between them

    # deciding with general csv file to use.
    if imputating:
        general_csv_path = system_path + "main_csv_files/main_csv_file_with_data_imputating.csv"
        imputate = "True"
    else:
        general_csv_path = system_path + "main_csv_files/main_csv_file_without_data_imputating.csv"
        imputate = "False"

    general_csv_df = pd.read_csv(general_csv_path)
    general_csv_len = general_csv_df.shape[0]

    # the activity column of the general csv file
    activity_values = general_csv_df['activity']

    # creating a list of possible activities (10, including unknown for the 'in the wild' part)
    activities = []
    for item in activity_values:
        if item not in activities:
            activities.append(item)


    BH_values = general_csv_df['BH'] # the BH column of the general csv file
    watch1_values = general_csv_df['watch1'] # the watch1 column of the general csv file
    rel1_values = general_csv_df['rel1'] # the rel1 column of the general csv file

    # a dictionary for BH values. Keys are activities. The values will be a list of 'all' the datapoints for that activity for all participant
    # in other words, 'any' BH value that we have for each activity
    # The same for watch1 and rel1

    BH_dict = dict((el, []) for el in activities)
    watch1_dict = dict((el, []) for el in activities)
    rel1_dict = dict((el, []) for el in activities)

    # for every row in the general csv file, I read the activity, then I read the BH value, watch1 value and rel1 value and add it to the dictionaries
    # as the values for the key = activity
    # Because I am appending to a list, the order should be fine
    for i in range(general_csv_len):
        activity = activity_values[i]
        BH_dict[activity].append(BH_values[i])
        watch1_dict[activity].append(watch1_values[i])
        rel1_dict[activity].append(rel1_values[i])

    # Now I want to draw the graphs per activity. For each activity I go to the dictionaries, get the list of BH, watch1, and rel1 values for that activity
    for activity in activities:
        BH = BH_dict[activity]
        watch1 = watch1_dict[activity]
        rel1 = rel1_dict[activity]

        # BH_high and watch1_high are BH and watch1 values for datapoints that have a high rel1. ( by high I mean either 2 or 3)
        BH_high = []
        watch1_high = []

        for i in range(len(rel1)):
            if int(rel1[i]) > 1:
                BH_high.append(BH[i])
                watch1_high.append(watch1[i])

        plt.rcParams["figure.figsize"] = [10, 8]
        plt.figure()
        plt.title("Difference vs Average of BH heart rate values and watch heart rate values for " + activity + "\n Imputating: " + imputate + "\n All reliabilities.")


        diff  = [i - j for i, j in zip(BH, watch1)]
        avg = [(i + j)/2 for i, j in zip(BH, watch1)]

        plt.plot(avg, diff, "bo")
        plt.xlabel("Average heart rate value between BH and watch (bpm)")
        plt.ylabel("Difference of heart rate value between BH and watch")
        plt.grid()

        figure_name = system_path + "graphs/" + "diff_vs_avg_BH_watch_" + activity + "_Imputating_" + imputate + "_all_rel.png"
        plt.savefig(figure_name)


        # ----------------------------------  The following code are similar to above, but for data points with high reliability
        
        plt.rcParams["figure.figsize"] = [10, 8]
        plt.figure()
        plt.title("Difference vs Average of BH heart rate values and watch heart rate values for  " + activity + "\n Imputating: " + imputate + "\n High reliabilities.")

        diff_high = [i - j for i, j in zip(BH_high, watch1_high)]
        avg_high = [(i + j) / 2 for i, j in zip(BH_high, watch1_high)]


        plt.plot(avg_high, diff_high, "bo")
        plt.xlabel("Average heart rate value between BH and watch (bpm)")
        plt.ylabel("Difference of heart rate value between BH and watch")
        plt.grid()

        figure_name = system_path + "graphs/" + "diff_vs_avg_BH_watch_" + activity + "_Imputating_" + imputate + "_high_rel.png"
        plt.savefig(figure_name)
    



def plot_diff_vs_avg_per_activity_per_participant(imputating):
    # deciding with general csv file to use.
    if imputating:
        general_csv_path = system_path + "main_csv_files/main_csv_file_with_data_imputating.csv"
        imputate = "True"
    else:
        general_csv_path = system_path + "main_csv_files/main_csv_file_without_data_imputating.csv"
        imputate = "False"

    general_csv_df = pd.read_csv(general_csv_path)
    general_csv_len = general_csv_df.shape[0]

    # the ID column of the general csv file
    ID_values = general_csv_df['ID']

    IDs = []  # list of all the IDs
    for item in ID_values:
        if item not in IDs:
            IDs.append(item)

    # the activity column of the general csv file
    general_activity_values = general_csv_df['activity']
    activities = []  ## list of all the activities ( 10 including unknown for 'in the wild' part)
    for item in general_activity_values:
        if item not in activities:
            activities.append(item)

    # For each participant:
    for ID in IDs:
        print "ID is: ", ID
        participant_id = general_csv_df['ID'] == ID

        # This is how you select dataframe rows using a condition. Here BH_values will be a list of BH values
        # taken from the BH column for the rows that had this specific ID that we are having in this iteration of the loop
        # The same for watch1, rel1 and activity
        # So these lists are specific to each participant
        BH_values = list(general_csv_df[participant_id]['BH'])
        watch1_values = list(general_csv_df[participant_id]['watch1'])
        rel1_values = list(general_csv_df[participant_id]['rel1'])
        activity_values = list(general_csv_df[participant_id]['activity'])

        # Now I want to create a dictionary for each participant. The keys of the dictionary are activities.
        BH_dict = dict((el, []) for el in activities)
        watch1_dict = dict((el, []) for el in activities)
        rel1_dict = dict((el, []) for el in activities)

        for i in range(len(BH_values)):
            activity = activity_values[i]
            BH_dict[activity].append(BH_values[i])
            watch1_dict[activity].append(watch1_values[i])
            rel1_dict[activity].append(rel1_values[i])

        for activity in activities:
            BH = BH_dict[activity]
            watch1 = watch1_dict[activity]
            rel1 = rel1_dict[activity]


            # The BH and watch values for data points with high reliability ( by high I mean 2 and 3)
            BH_high = []
            watch1_high = []

            for i in range(len(rel1)):
                if int(rel1[i]) > 1:
                    BH_high.append(BH[i])
                    watch1_high.append(watch1[i])

            plt.rcParams["figure.figsize"] = [10, 8]
            plt.figure()
            plt.title(
                "Difference vs Average of BH heart rate values and watch heart rate values for " + activity + "\n Imputating: " + imputate + "\n All reliabilities." + "\n ID: " + ID)

            diff = [i - j for i, j in zip(BH, watch1)]
            avg = [(i + j) / 2 for i, j in zip(BH, watch1)]

            plt.plot(avg, diff, "bo")
            plt.xlabel("Average heart rate value between BH and watch (bpm)")
            plt.ylabel("Difference of heart rate value between BH and watch")
            plt.grid()

            figure_name = system_path + "graphs/" + "diff_vs_avg_BH_watch_" + activity + "_Imputating_" + imputate + "_all_rel_" + ID + ".png"
            plt.savefig(figure_name)
            # ----------------------------------
            plt.rcParams["figure.figsize"] = [10, 8]
            plt.figure()
            plt.title(
                "Difference vs Average of BH heart rate values and watch heart rate values for " + activity + "\n Imputating: " + imputate + "\n High reliabilities." + "\n ID: " + ID)

            diff_high = [i - j for i, j in zip(BH_high, watch1_high)]
            avg_high = [(i + j) / 2 for i, j in zip(BH_high, watch1_high)]

            plt.plot(avg_high, diff_high, "bo")
            plt.xlabel("Average heart rate value between BH and watch (bpm)")
            plt.ylabel("Difference of heart rate value between BH and watch")
            plt.grid()

            figure_name = system_path + "graphs/" + "diff_vs_avg_BH_watch_" + activity + "_Imputating_" + imputate + "_high_rel_" + ID + ".png"
            plt.savefig(figure_name)







if __name__ == '__main__':
    plot_diff_vs_avg_per_activity_per_participant(False)