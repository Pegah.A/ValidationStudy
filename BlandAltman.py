import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import pandas as pd
import numpy as np
import math
from scipy.stats.kde import gaussian_kde
import matplotlib.mlab as mlab
from numpy import linspace
from scipy import stats


import time
from datetime import datetime, date

import operator
import csv
# read the csv file
# per participant, per activity,
# get the BH column
# get the watch HR column
# calculate the difference
# show that difference has a normal distribution using gaussian_kde

color_map_dict_1 = {3.0: "darkblue" , 2.0: "darkturquoise" , 1.0: "cornflowerblue", 0.0: "skyblue"}
ID_list = ['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID10','ID12','ID13', 'ID15', 'ID16' ]
activity_list = ['lying'] #["sixmwt", "sitting" , "walking" , "lying" , "standing", "eating" , "brushing", "climbup", "climbdown"]


COPD_list = [True, False]

imputing_list = [False] #[True, False]
sign_list = ["neg" , "pos"]
gold_list = [False] #[True, False]

system_path = "/Users/pegah/Documents/VS/"

def abline(slope, intercept, color, style, x_lim_range, label = ""):
    """Plot a line from slope and intercept"""
    axes = plt.gca()
    x_vals = np.array(x_lim_range)
    y_vals = intercept + slope * x_vals
    plt.plot(x_vals, y_vals, linestyle = style, color = color, label = label)

def perform_shapiro_test(ID, activity, dataframe, imputing):

    no_val_from_watch = False
    # for each participant, if the p-value of the Shapiro test for an acitivty for that person is > 0.05 we will add
    # the actviity to the dictionary

    section = dataframe.query('activity==@activity and ID == @ID')
    new_section = dataframe.query('activity==@activity and ID == @ID')
    cols = ['BH', 'watch1']

    if imputing:
        new_section[cols] = section[cols].ffill()  # fill the missing values with the value from the previous data point
        watch1 = new_section['watch1']

        # even though we are filling the missing data points, if the very first data point is missing,
        # it won't be able to be filled because there is no previous value for it
        # These NaN values will be ignored.

        length = len(watch1)
        index = 0
        while math.isnan(watch1.iloc[index]):
            index += 1
            if index == length:
                no_val_from_watch = True
                break

        new_section = new_section[index:]

    else:
        # if there is no imputing, I just want to consider those BH values that have a corresponding watch value

        # new section has columns for BH, watch1, rel1
        new_section = section[cols].loc[section['watch1'].notnull()]
        if len(new_section) == 0:
            no_val_from_watch = True

    valid_length = len(new_section)
    if not no_val_from_watch:
        if valid_length >=3:
            dif = new_section['BH'] - new_section['watch1']

            w, p = stats.shapiro(dif)

            if p > 0.05:
                return (True, p)

    return (False, 0)

def dif_distribution_plot(dataframe):
    for ID in ID_list:
        for activity in activity_list:
            section = dataframe.query('activity==@activity and ID == @ID')
            new_section = dataframe.query('activity==@activity and ID == @ID')
            BH = section['BH']
            cols = ['watch1' , 'rel1']
            new_section[cols] = section[cols].ffill()
            watch1 = new_section['watch1']


            no_val_from_watch = False
            length = len(watch1)

            index = 0
            while math.isnan(watch1.iloc[index]):
                index += 1
                if index == length:
                    no_val_from_watch = True
                    break

            BH = BH [index:]
            watch1 = watch1[index:]
            valid_len = len(watch1)

            dif = np.subtract(BH, watch1)

            mean_dif = np.mean(dif)
            std_dif = np.std(dif)
            var_dif = np.var(dif)
            length = len(dif)

            plt.figure(figsize=(10, 8))


            if not no_val_from_watch and valid_len >3:
                sigma = math.sqrt(var_dif)
                x = np.linspace(mean_dif - 4 * sigma, mean_dif + 4 * sigma, length)
                plt.plot(x, mlab.normpdf(x, mean_dif, sigma), linestyle="dashed")

                dif_kde = gaussian_kde(dif)
                dif_dist_space = linspace(min(dif), max(dif), length)
                plt.plot(dif_dist_space, dif_kde(dif_dist_space), color = "mediumvioletred")

            plt.title("Distribution of the differene between the heart rate value reported by the Biohanress and the watch \n"
                      "(Bioharness - Watch) \n"
                      "ID: " + ID +"    Activity: "+ activity)

            fig_name = "/Users/pegah/Documents/VS/graphs/dif_dist/dif_dist_" + activity + "_" + ID + ".png"
            plt.savefig(fig_name)
            #plt.show()

def dif_mean_graph(dataframe):
    # this is per participant

    bland_altman_csv_path = system_path + "BA_res/Bland_Altman_Result.csv"
    bland_altman_df = pd.DataFrame(columns=['ID', 'Activity', 'Imputing', 'Gold', 'Difference_mean' ,
                                            'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high', 'Agreement_limit_range', 'Regression_line_slope'])

    for gold in gold_list:
        for imputing in imputing_list:
            for ID in ID_list:
                for activity in activity_list:

                    section = dataframe.query('activity==@activity and ID == @ID')
                    new_section = dataframe.query('activity==@activity and ID == @ID')
                    cols = ['BH','watch1', 'rel1']

                    no_val_from_watch = False

                    if imputing:
                        new_section[cols] = section[cols].ffill() # fill the missing values with the value from the previous data point
                        watch1 = new_section['watch1']

                        # even though we are filling the missing data points, if the very first data point is missing,
                        # it won't be able to be filled because there is no previous value for it
                        # These NaN values will be ignored.

                        length = len(new_section)
                        index = 0
                        while math.isnan(watch1.iloc[index]):
                            index += 1
                            if index == length:
                                no_val_from_watch = True
                                break

                        new_section = new_section[index:]

                    else:
                        # if there is no imputing, I just want to consider those BH values that have a corresponding watch value

                        # new section has columns for BH, watch1, rel1
                        new_section = section[cols].loc[section['watch1'].notnull()]
                        if len(new_section) == 0:
                            no_val_from_watch = True

                    if not no_val_from_watch:
                        #plt.figure(figsize=(10, 8))

                        if gold:
                            for rel,sub_section in new_section.groupby('rel1'):
                                dif = sub_section.BH - sub_section.watch1
                                #plt.plot(sub_section.BH, dif , "o" , color = color_map_dict_1[rel], label = 'watch rel: {}'.format(int(rel)))
                        else:

                            for rel,sub_section in new_section.groupby('rel1'):
                                dif = sub_section.BH - sub_section.watch1
                                avg = (sub_section.BH + sub_section.watch1) / 2
                                #plt.plot(avg, dif , "o" , color = color_map_dict_1[rel], label = 'watch rel: {}'.format(int(rel)))

                        dif = new_section['BH'] - new_section['watch1']
                        avg = (new_section['BH'] + new_section['watch1']) /2
                        mean_dif = np.mean(dif)
                        std_dif = np.std(dif)


                        # the approach to get the limits of agreement, based on Bland-Altman
                        # 95% of differences will be between these values, if the differences are normally distributed

                        agreement_limit_1 = mean_dif - (1.96 * std_dif)
                        agreement_limit_2 = mean_dif + (1.96 * std_dif)
                        agreement_limit_range = agreement_limit_2 - agreement_limit_1


                        axes = plt.gca()
                        x_lim_range = axes.get_xlim()

                        #abline(0, 0, "mediumspringgreen", "--", x_lim_range)  # ideally, we want dif to be equal or close to zero
                        #abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")
                
                        #abline(0, agreement_limit_1,"darkslateblue",  "-.", x_lim_range, "Limits of Agreement" )
                        #abline(0, agreement_limit_2, "darkslateblue","-.", x_lim_range)
                
                        if gold:
                            slope, intercept, r_value, p_value, std_err = stats.linregress(new_section['BH'], dif)
                            #abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")
                        else:
                            slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
                            #abline(slope, intercept,"orchid", "-", x_lim_range, "Regression Line" )

                        bland_altman_df = bland_altman_df.append(pd.DataFrame([[ID, activity, imputing, gold, mean_dif, std_dif, agreement_limit_1, agreement_limit_2,
                                                                                agreement_limit_range, slope]],
                                                                        columns=['ID', 'Activity', 'Imputing', 'Gold', 'Difference_mean' ,
                                            'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high', 'Agreement_limit_range', 'Regression_line_slope']), ignore_index=True)
                    """
                    if gold:
                        plt.xlabel("Bioharness HR values")
                        plt.title('Difference over Gold Standard \n Activity: ' + activity + '    Participant ID: ' + ID +
                                  "\n Imputing: " + str(imputing))
                    else:
                        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
                        plt.title('Difference over Average \n Activity: ' + activity + '    Participant ID: ' + ID +
                                  "\n Imputing: " + str(imputing))
                
                
                    plt.ylabel("Bioharness HR value - Watch HR value")
                    plt.legend()
                
                    if gold:
                        fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_gold/dif_over_gold_" + "imputing_" + str(imputing) \
                                   + "_"+ activity + "_" + ID + ".png"
                    else:
                        fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_avg/dif_over_avg_" + "imputing_" + str(imputing) \
                                   + "_" + activity + "_" + ID + ".png"
                
                    plt.savefig(fig_name)
                    # plt.show()
                    """


    bland_altman_df.to_csv(bland_altman_csv_path)

def dif_distribution_plot_across_participants(activity, dataframe):
    section = dataframe.query('activity==@activity')
    new_section = dataframe.query('activity==@activity')
    BH = section['BH']
    time = section['ts']
    cols = ['watch1', 'rel1']
    new_section[cols] = section[cols].ffill()
    watch1 = new_section['watch1']

    no_val_from_watch = False
    length = len(watch1)

    index = 0
    while math.isnan(watch1.iloc[index]):
        index += 1
        if index == length:
            no_val_from_watch = True
            break

    BH = BH[index:]
    watch1 = watch1[index:]
    valid_len = len(watch1)

    dif = np.subtract(BH, watch1)

    mean_dif = np.mean(dif)
    std_dif = np.std(dif)
    var_dif = np.var(dif)
    length = len(dif)

    plt.figure(figsize=(10, 8))

    if not no_val_from_watch and valid_len > 3:
        sigma = math.sqrt(var_dif)
        x = np.linspace(mean_dif - 4 * sigma, mean_dif + 4 * sigma, length)
        plt.plot(x, mlab.normpdf(x, mean_dif, sigma), linestyle="dashed")

        dif_kde = gaussian_kde(dif)
        dif_dist_space = linspace(min(dif), max(dif), length)
        plt.plot(dif_dist_space, dif_kde(dif_dist_space), color="mediumvioletred")

    plt.title(
        "Distribution of the differene between the heart rate value reported by the Biohanress and the watch \n"
        "(Bioharness - Watch) \n" +"Across participants\n"+ "Activity: " + activity)

    fig_name = "/Users/pegah/Documents/VS/graphs/across_participant/dif_dist_" + activity +".png"
    plt.savefig(fig_name)
    # plt.show()

def dif_mean_graph_across_participants( dataframe):
    bland_altman_csv_path = system_path + "BA_res/Bland_Altman_Result_Across_Participants.csv"
    bland_altman_df = pd.DataFrame(columns=['Activity', 'Imputing', 'Gold', 'Difference_mean',
                                            'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                                            'Agreement_limit_range', 'Regression_line_slope'])


    for gold in gold_list:  # i don't want gold
        for imputing in imputing_list:  # i don't want imputing
            for COPD in COPD_list:
                for activity in activity_list:

                    invalid_ID_1 = 'ID14' # bioharness is not valid
                    invalid_ID_2 = 'ID11' # watch is not valid
                    invalid_ID_3 = 'ID10' # bioharness is not valid for sixmwt

                    section = dataframe.query('activity==@activity and COPD ==@COPD and ID !=@invalid_ID_1 and ID !=@invalid_ID_2')
                    new_section = dataframe.query('activity==@activity and COPD ==@COPD and ID !=@invalid_ID_1 and ID !=@invalid_ID_2')

                    if activity == "sixmwt":
                        section = dataframe.query(
                            'activity==@activity and COPD ==@COPD and ID !=@invalid_ID_1 and ID !=@invalid_ID_2 and ID !=@invalid_ID_3')
                        new_section = dataframe.query(
                            'activity==@activity and COPD ==@COPD and ID !=@invalid_ID_1 and ID !=@invalid_ID_2 and ID !=@invalid_ID_3')
                    cols = ['BH', 'watch1', 'rel1']

                    no_val_from_watch = False

                    if imputing:
                        new_section[cols] = section[cols].ffill()  # fill the missing values with the value from the previous data point
                        watch1 = new_section['watch1']

                        # even though we are filling the missing data points, if the very first data point is missing,
                        # it won't be able to be filled because there is no previous value for it
                        # These NaN values will be ignored.

                        length = len(new_section)
                        index = 0
                        while math.isnan(watch1.iloc[index]):
                            index += 1
                            if index == length:
                                no_val_from_watch = True
                                break

                        new_section = new_section[index:]

                    else:
                        # if there is no imputing, I just want to consider those BH values that have a corresponding watch value

                        # new section has columns for BH, watch1, rel1
                        new_section = section[cols].loc[section['watch1'].notnull()]

                    if not no_val_from_watch:
                        plt.figure(figsize=(10, 8))
                        matplotlib.rcParams.update({'font.size': 14})
                        axes = plt.gca()
                        axes.set_ylim([-100, 100])

                        if gold:
                            for rel, sub_section in new_section.groupby('rel1'):
                                dif = sub_section.BH - sub_section.watch1
                                #plt.plot(sub_section.BH, dif, "o", color=color_map_dict_1[rel], label='watch rel: {}'.format(int(rel)))
                        else:

                            for rel, sub_section in new_section.groupby('rel1'):
                                dif = sub_section.BH - sub_section.watch1
                                avg = (sub_section.BH + sub_section.watch1) / 2
                                plt.plot(avg, dif, "o", color=color_map_dict_1[rel], label='watch rel: {}'.format(int(rel)))

                        dif = new_section['BH'] - new_section['watch1']
                        avg = (new_section['BH'] + new_section['watch1']) /2
                        mean_dif = np.mean(dif)
                        std_dif = np.std(dif)

                        # the approach to get the limits of agreement, based on Bland-Altman
                        # 95% of differences will be between these values, if the differences are normally distributed

                        agreement_limit_1 = mean_dif - (1.96 * std_dif)
                        agreement_limit_2 = mean_dif + (1.96 * std_dif)
                        agreement_limit_range = agreement_limit_2 - agreement_limit_1



                        x_lim_range = axes.get_xlim()

                        print "hereeee"



                        abline(0, 0, "mediumspringgreen", "--", x_lim_range)  # ideally, we want dif to be equal or close to zero
                        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

                        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
                        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

                        if gold:
                            slope, intercept, r_value, p_value, std_err = stats.linregress(new_section['BH'], dif)
                            # abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")
                        else:
                            slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
                            abline(slope, intercept,"orchid", "-", x_lim_range, "Regression Line" )

                        bland_altman_df = bland_altman_df.append(pd.DataFrame(
                            [[activity, imputing, gold, mean_dif, std_dif, agreement_limit_1, agreement_limit_2,
                              agreement_limit_range, slope]],
                            columns=['Activity', 'Imputing', 'Gold', 'Difference_ean',
                                     'Dofference_std', 'Agreement_limit_low', 'Agreement_limit_high', 'Agreement_limit_range',
                                     'Regression_line_slope']), ignore_index=True)

                    if gold:
                        plt.xlabel("Bioharness HR values")
                        plt.title('Difference over Gold Standard \n Activity: ' + activity + '    COPD: ' + str(COPD))
                    else:
                        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
                        plt.title('Difference over Average \n Activity: ' + activity + '    COPD: ' + str(COPD))

                    plt.ylabel("Bioharness HR value - Watch HR value")
                    plt.legend()

                    if gold:
                        fig_name = "/Users/pegah/Documents/VS/graphs/across_participant/dif_over_gold_" + "imputing_" + str(imputing) \
                                   + "_" + activity + ".png"
                    else:
                        fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_avg_all_rel/" + "BA_all_rel_"  + activity + \
                                   "_COPD_" + str(COPD)+ ".png"

                    plt.savefig(fig_name)
                    # plt.show()

    #bland_altman_df.to_csv(bland_altman_csv_path)

def dif_avg_reg_line_slope_analysis(sign, imputing):
    # I have the file for negative slopes  imputed /not imputed
    # and the file for positive slopes imputed/not imputed

    if imputing:
        unsorted_path = system_path + "BA_res/" + sign + "_dif_avg_reg_line_slopes_with_impute"
        sorted_path = system_path + "BA_res/" + sign + "_dif_avg_reg_line_slopes_with_impute_sorted"
    else:
        unsorted_path = system_path + "BA_res/" + sign + "_dif_avg_reg_line_slopes_without_impute"
        sorted_path = system_path + "BA_res/" + sign + "_dif_avg_reg_line_slopes_without_impute_sorted"

    unsorted_file = open(unsorted_path, "r")
    sorted_file = open(sorted_path, "w")

    unsorted_lines = unsorted_file.readlines()
    length = len(unsorted_lines)

    print "sign is: ", sign
    print "imputing is: ", imputing
    print "length is: ", length

    slope_dict = {}

    for i in range(length):
        slope = float(unsorted_lines[i].split("_")[13])
        slope_dict[unsorted_lines[i].strip()] = slope

    slope_sorted_list = sorted(slope_dict.items(), key=operator.itemgetter(1))

    for i in range(length):
        new_line = slope_sorted_list[i][0]+ "\n"
        sorted_file.write(new_line)

    sorted_file.close()

def sort_csv_by_column():

    csv_path = system_path + "BA_res_mid_rel/Bland_Altman_Result.csv"
    column_name = 'Agreement_limit_range'
    sorted_csv_path = system_path + "BA_res_mid_rel/Bland_Altman_Result_sorted_by_lim_range.csv"

    df = pd.read_csv(csv_path, index_col = 0)
    df = df.sort_values(by=[column_name])
    df.to_csv(sorted_csv_path, index = False)


# The updated version considers only data points with the specified reliabilities, without imputing, over average
def dif_mean_high_rel_per_activity_per_person(dataframe, reliability):
    bland_altman_csv_path = system_path + "BA_res_high_rel/Bland_Altman_Result.csv"
    bland_altman_df = pd.DataFrame(columns=['ID', 'Activity', 'Difference_mean',
                                            'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                                            'Agreement_limit_range', 'Regression_line_slope'])
    for ID in ID_list:
        for activity in activity_list:

            section = dataframe.query('activity==@activity and ID == @ID')
            cols = ['BH', 'watch1', 'rel1']

            no_val_from_watch = False

            # if there is no imputing, I just want to consider those BH values that have a corresponding watch value
            # new section has columns for BH, watch1, rel1

            non_null = section['watch1'].notnull()
            high_rel = section['rel1']== reliability
            new_section = section[non_null & high_rel]


            #new_section = section[cols].loc[section['watch1'].notnull()]
            #new_section = new_section[cols].loc[new_section['rel1']== reliability]

            if len(new_section) == 0:
                no_val_from_watch = True

            plt.figure(figsize=(10, 8))


            if not no_val_from_watch:
                dif = new_section['BH'] - new_section['watch1']
                avg = (new_section['BH'] + new_section['watch1']) / 2


                plt.plot(avg, dif, "o", color=color_map_dict_1[reliability])


                mean_dif = np.mean(dif)
                std_dif = np.std(dif)

                # the approach to get the limits of agreement, based on Bland-Altman
                # 95% of differences will be between these values, if the differences are normally distributed

                agreement_limit_1 = mean_dif - (1.96 * std_dif)
                agreement_limit_2 = mean_dif + (1.96 * std_dif)
                agreement_limit_range = agreement_limit_2 - agreement_limit_1

                axes = plt.gca()
                x_lim_range = axes.get_xlim()

                abline(0, 0, "mediumspringgreen", "--", x_lim_range)  # ideally, we want dif to be equal or close to zero
                abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

                abline(0, agreement_limit_1,"darkslateblue",  "-.", x_lim_range, "Limits of Agreement" )
                abline(0, agreement_limit_2, "darkslateblue","-.", x_lim_range)


                slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
                abline(slope, intercept,"orchid", "-", x_lim_range, "Regression Line" )

                bland_altman_df = bland_altman_df.append(pd.DataFrame(
                    [[ID, activity, mean_dif, std_dif, agreement_limit_1, agreement_limit_2,
                      agreement_limit_range, slope]],
                    columns=['ID', 'Activity','Difference_mean',
                             'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                             'Agreement_limit_range', 'Regression_line_slope']), ignore_index=True)

                bland_altman_df.to_csv(bland_altman_csv_path)


            plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
            plt.title('Difference over Average \n Activity: ' + activity + '   Participant ID: ' + ID +
                      "\n Reliability: " + str(int(reliability)))


            plt.ylabel("Bioharness HR value - Watch HR value")
            plt.legend()


            fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_avg_per_person/dif_over_avg_" + "rel_" + str(int(reliability))\
                           + "_" + activity + "_" + ID + ".png"

            plt.savefig(fig_name)
            # plt.show()


def dif_mean_high_rel_per_activity_per_healthiness(dataframe, reliability):



    bland_altman_csv_path = system_path + "BA_res_high_rel/Bland_Altman_Result.csv"
    bland_altman_df = pd.DataFrame(columns=['ID', 'Activity', 'Difference_mean',
                                            'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                                            'Agreement_limit_range', 'Regression_line_slope'])
    for COPD in COPD_list:
        for activity in activity_list:
            invalid_ID1 = 'ID14'
            invalid_ID2 = 'ID11'
            section = dataframe.query('activity==@activity and COPD == @COPD and ID != @invalid_ID1 and ID != @invalid_ID2')

            if activity == 'sixmwt':
                invalid_ID3 = 'ID10'
                section = dataframe.query(
                    'activity==@activity and COPD == @COPD and ID != @invalid_ID1 and ID != @invalid_ID2 and ID != @invalid_ID3')



            #print "section is: ", section
            # ------ adding imputing-------
            #cols = ['BH', 'watch1', 'rel1']

            #section[cols] = section[cols].ffill()  # fill the missing values with the value from the previous data point


            #print "Section after imputing is: ", section
            # -----------------------------

            no_val_from_watch = False

            # if there is no imputing, I just want to consider those BH values that have a corresponding watch value
            # new section has columns for BH, watch1, rel1

            non_null = section['watch1'].notnull()
            high_rel = section['rel1'] == reliability
            #med_rel = section['rel1'] == reliability[1]

            #new_section = section[non_null & (high_rel | med_rel)]
            new_section = section[non_null & high_rel]

            #print "new section is: ", new_section

            # new_section = section[cols].loc[section['watch1'].notnull()]
            # new_section = new_section[cols].loc[new_section['rel1']== reliability]

            if len(new_section) == 0:
                no_val_from_watch = True

            plt.figure(figsize=(10, 8))
            matplotlib.rcParams.update({'font.size': 14})

            if not no_val_from_watch:

                #for rel, sub_section in new_section.groupby('rel1'):
                 #   dif = sub_section.BH - sub_section.watch1
                 #   avg = (sub_section.BH + sub_section.watch1) / 2
                  #  plt.plot(avg, dif, "o", color=color_map_dict_1[rel], label = 'Reliability: ' + str(int(rel)))

                dif = new_section['BH'] - new_section['watch1']

                avg = (new_section['BH'] + new_section['watch1']) / 2


                plt.plot(avg, dif, "o", color=color_map_dict_1[reliability] ) #color_map_dict_1[reliability])

                mean_dif = np.mean(dif)
                std_dif = np.std(dif)

                # the approach to get the limits of agreement, based on Bland-Altman
                # 95% of differences will be between these values, if the differences are normally distributed

                agreement_limit_1 = mean_dif - (1.96 * std_dif)
                agreement_limit_2 = mean_dif + (1.96 * std_dif)
                agreement_limit_range = agreement_limit_2 - agreement_limit_1


                print "limits of agreement: ", agreement_limit_1
                print "limits of agreement: ", agreement_limit_2
                print "mean dif is: ", mean_dif
                axes = plt.gca()
                x_lim_range = axes.get_xlim()

                abline(0, 0, "mediumspringgreen", "--",
                       x_lim_range)  # ideally, we want dif to be equal or close to zero
                abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

                abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
                abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

                slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
                abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

                """
                bland_altman_df = bland_altman_df.append(pd.DataFrame(
                    [[ID, activity, mean_dif, std_dif, agreement_limit_1, agreement_limit_2,
                      agreement_limit_range, slope]],
                    columns=['ID', 'Activity', 'Difference_mean',
                             'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                             'Agreement_limit_range', 'Regression_line_slope']), ignore_index=True)

                bland_altman_df.to_csv(bland_altman_csv_path)
                """

            plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
            plt.title('Difference over Average \n Activity: ' + activity + '   COPD: ' + str(COPD) +
                      "\n Reliability: " + str(reliability))

            plt.ylabel("Bioharness HR value - Watch HR value")

            axes = plt.gca()
            axes.set_ylim([-30, 65])

            plt.legend()

            fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_avg_imputed/dif_over_avg_imputed_" + "rel_" + str(
                reliability)\
                       + "_" + activity + "_COPD_" + str(COPD) + ".png"

            #plt.savefig(fig_name)
            plt.show()


def dif_mean_high_rel_per_activity_across_participants(dataframe, reliability):

    bland_altman_csv_path = system_path + "BA_res_high_rel/Bland_Altman_Result.csv"
    bland_altman_df = pd.DataFrame(columns=['ID', 'Activity', 'Difference_mean',
                                            'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                                            'Agreement_limit_range', 'Regression_line_slope'])

    for activity in activity_list:

        invalid_ID1 = 'ID14'
        invalid_ID2 = 'ID11'
        section = dataframe.query('activity==@activity and ID != @invalid_ID1 and ID != @invalid_ID2')

        if activity == 'sixmwt':
            invalid_ID3 = 'ID10'
            section = dataframe.query(
                'activity==@activity and ID != @invalid_ID1 and ID != @invalid_ID2 and ID != @invalid_ID3')


        cols = ['BH', 'watch1', 'rel1']

        no_val_from_watch = False

        # if there is no imputing, I just want to consider those BH values that have a corresponding watch value
        # new section has columns for BH, watch1, rel1

        non_null = section['watch1'].notnull()
        high_rel = section['rel1'] == reliability
        new_section = section[non_null & high_rel]

        # new_section = section[cols].loc[section['watch1'].notnull()]
        # new_section = new_section[cols].loc[new_section['rel1']== reliability]

        if len(new_section) == 0:
            no_val_from_watch = True

        plt.figure(figsize=(10, 8))

        if not no_val_from_watch:

            dif = new_section['BH'] - new_section['watch1']

            avg = (new_section['BH'] + new_section['watch1']) / 2

            plt.plot(avg, dif, "o", color=color_map_dict_1[reliability])

            mean_dif = np.mean(dif)
            std_dif = np.std(dif)

            # the approach to get the limits of agreement, based on Bland-Altman
            # 95% of differences will be between these values, if the differences are normally distributed

            agreement_limit_1 = mean_dif - (1.96 * std_dif)
            agreement_limit_2 = mean_dif + (1.96 * std_dif)
            agreement_limit_range = agreement_limit_2 - agreement_limit_1

            axes = plt.gca()
            x_lim_range = axes.get_xlim()

            abline(0, 0, "mediumspringgreen", "--",
                   x_lim_range)  # ideally, we want dif to be equal or close to zero
            abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

            abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
            abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

            slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
            abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

            """
            bland_altman_df = bland_altman_df.append(pd.DataFrame(
                [[ID, activity, mean_dif, std_dif, agreement_limit_1, agreement_limit_2,
                  agreement_limit_range, slope]],
                columns=['ID', 'Activity', 'Difference_mean',
                         'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                         'Agreement_limit_range', 'Regression_line_slope']), ignore_index=True)

            bland_altman_df.to_csv(bland_altman_csv_path)
            """

        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
        plt.title('Difference over Average \n Activity: ' + activity +
                  "\n Reliability: " + str(int(reliability)))

        plt.ylabel("Bioharness HR value - Watch HR value")
        plt.legend()

        fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_avg_high_rel/healthiness/dif_over_avg_" + "rel_" + str(
            int(reliability)) \
                   + "_" + activity + ".png"

        plt.savefig(fig_name)
        # plt.show()



def dif_mean_high_rel_across_healthiness_wild(dataframe):

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'
    section = dataframe.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

    # ------ adding imputing-------
    cols = ['BH', 'watch1', 'rel1']

    section[cols] = section[cols].ffill()  # fill the missing values with the value from the previous data point

    print "Section after imputing is: ", section
    # -----------------------------

    # I want those data point for which the watch has reported a heart rate, accel, and gyro
    accel_not_null_condition = np.isnan(section['accel_mag_1']) == False
    gyro_not_null_condition = np.isnan(section['gyro_mag_1']) == False
    HR_not_null_condition = np.isnan(section['watch1']) == False
    high_rel_condition = section['rel1'] == 3.0

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD']== False


    new_section_COPD = section[accel_not_null_condition & gyro_not_null_condition &
                                HR_not_null_condition & COPD_condition & high_rel_condition]
    new_section_non_COPD = section[ accel_not_null_condition & gyro_not_null_condition &
                                    HR_not_null_condition & non_COPD_condition & high_rel_condition]


    COPD_options= [True, False]

    for option in COPD_options:
        if option == True:
            BH =  new_section_COPD['BH']
            watch = new_section_COPD['watch1']
        else:
            BH = new_section_non_COPD['BH']
            watch = new_section_non_COPD['watch1']



        plt.figure(figsize=(10, 8))
        matplotlib.rcParams.update({'font.size': 17})



        dif= BH - watch
        avg= (BH+ watch) / 2

        plt.plot(avg, dif, "o", color="darkblue")  # color_map_dict_1[reliability])

        mean_dif = np.mean(dif)
        std_dif = np.std(dif)

        # the approach to get the limits of agreement, based on Bland-Altman
        # 95% of differences will be between these values, if the differences are normally distributed

        agreement_limit_1 = mean_dif - (1.96 * std_dif)
        agreement_limit_2 = mean_dif + (1.96 * std_dif)
        agreement_limit_range = agreement_limit_2 - agreement_limit_1

        #------
        print "COPD is: ", str(option)
        print "mean dif is: ", mean_dif
        print "lim 1 is: ", agreement_limit_1
        print "lim2 is: ", agreement_limit_2
        #------


        axes = plt.gca()
        x_lim_range = axes.get_xlim()

        abline(0, 0, "mediumspringgreen", "--",
               x_lim_range)  # ideally, we want dif to be equal or close to zero
        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

        slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
        abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

        plt.title('Difference over Average \n' + 'Uncontrolled Setting\n' + 'COPD: ' + str(option))

        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
        plt.ylabel("Bioharness HR value - Watch HR value")

        axes = plt.gca()
        axes.set_ylim([-30, 65])

        plt.legend()

        fig_name = "/Users/pegah/Documents/VS/graphs/wild/dif_over_mean_COPD_" + str(option) + "_impute_high_wild.png"

        plt.savefig(fig_name)
        #plt.show()


def helper_get_percentiles(wild_dataframe, num_of_ranges, data_type, accel_DC, gyro_DC):

    invalid_ID = 'ID11'
    valid_section = wild_dataframe.query('ID != @invalid_ID ')
    not_null_condition = np.isnan(valid_section[data_type+'_mag_1']) == False
    valid_section = valid_section[not_null_condition]

    mag = valid_section[data_type + '_mag_1']

    if data_type == 'accel':
        mag = np.abs(mag- accel_DC)
    elif data_type == 'gyro':
        mag = np.abs(mag- gyro_DC)

    #print "mag is: ", mag

    percentile_list = []
    first_percentile_percentage = 100 / num_of_ranges

    prev_percentile_percentage = 0

    for i in range(len(valid_section)):
        if np.isnan(valid_section[data_type+'_mag_1'].iloc[i]):
            print "the null is at index: ", i

    for i in range(num_of_ranges):
        percentile_percentage = prev_percentile_percentage + first_percentile_percentage
        percentile_list.append(np.percentile(mag, percentile_percentage))
        prev_percentile_percentage += first_percentile_percentage

        """
                # np.nanpercentile() Compute the qth percentile of the data along the specified axis, while ignoring nan values.
                percentile_20 = np.percentile(mag, 20)
                percentile_40 = np.percentile(mag, 40)
                percentile_60 = np.percentile(mag, 60)
                percentile_80 = np.percentile(mag, 80)
                percentile_100 = np.percentile(mag, 100)

                """

    print "percentile list is: ", percentile_list
    return percentile_list

def Bland_Altman_based_on_accel_gyro(wild_dataframe, percentile_list, data_type, accel_DC, gyro_DC):

    # I want to read the main_csv_wild file.

    num_of_ranges = len(percentile_list)
    percentile = 0

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'
    section = wild_dataframe.query('ID != @invalid_ID_1  and ID != @invalid_ID_2')
    not_null_condition = np.isnan(section[data_type + '_mag_1']) == False
    section = section[not_null_condition]

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD'] == False


    #print "before removing DC: ", section[data_type + '_mag_1']

    if data_type == 'accel':
        section[data_type + '_mag_1'] = np.abs(section[data_type+'_mag_1']- accel_DC)
    elif data_type == 'gyro':
        section[data_type + '_mag_1'] = np.abs(section[data_type + '_mag_1'] - gyro_DC)

    #print "after removing DC: ", section[data_type + '_mag_1']



    for i in range(len(percentile_list)):
        this_section_rel_condition = section['rel1'] == 3.0
        this_section_range_condition = section[data_type+'_mag_1'] <= percentile_list[i]
        this_range_section = section[this_section_rel_condition & this_section_range_condition & COPD_condition]
        print "this range section length is: ", this_range_section.shape[0]


        percentile += 100/num_of_ranges
        plt.figure(figsize=(10, 8))
        matplotlib.rcParams.update({'font.size': 14})

        dif = this_range_section['BH'] - this_range_section['watch1']

        avg = (this_range_section['BH'] + this_range_section['watch1']) / 2

        plt.plot(avg, dif, "o", color="darkblue")  # color_map_dict_1[reliability])

        mean_dif = np.mean(dif)
        std_dif = np.std(dif)

        # the approach to get the limits of agreement, based on Bland-Altman
        # 95% of differences will be between these values, if the differences are normally distributed

        agreement_limit_1 = mean_dif - (1.96 * std_dif)
        agreement_limit_2 = mean_dif + (1.96 * std_dif)
        agreement_limit_range = agreement_limit_2 - agreement_limit_1

        axes = plt.gca()
        x_lim_range = axes.get_xlim()

        abline(0, 0, "mediumspringgreen", "--",
               x_lim_range)  # ideally, we want dif to be equal or close to zero
        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

        slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
        abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

        plt.title('Difference over Average \n' + data_type+ ' Vector Magnitude value less than or equal to: ' + str(
            percentile_list[i]) +"\n"+ str(percentile) + "th percentile\n Controlled Setting")

        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")
        plt.ylabel("Bioharness HR value - Watch HR value")

        axes = plt.gca()
        axes.set_ylim([-30, 65])

        plt.legend()

        fig_name = "/Users/pegah/Documents/VS/graphs/accel_gyro/in_the_wild/"+data_type+"/with_"+str(num_of_ranges)+"_ranges/" + str(
            percentile_list[i]) + "_" + data_type+"_range444.png"

        plt.savefig(fig_name)
        # plt.show()





def dif_over_mean_controlled_per_healthiness_per_reliability(dataframe, reliability, is_wild):

    for COPD in COPD_list:
            invalid_ID1 = 'ID14'
            invalid_ID2 = 'ID11'
            section = dataframe.query('COPD == @COPD and ID != @invalid_ID1 and ID != @invalid_ID2')

            if is_wild == False:
                ID10_condition = section['ID'] != 'ID10'
                sixmwt_condition = section['activity'] != "sixmwt"


            #print "section is: ", section
            # ------ adding imputing-------
            #cols = ['BH', 'watch1', 'rel1']

            #section[cols] = section[cols].ffill()  # fill the missing values with the value from the previous data point


            #print "Section after imputing is: ", section
            # -----------------------------

            no_val_from_watch = False

            # if there is no imputing, I just want to consider those BH values that have a corresponding watch value
            # new section has columns for BH, watch1, rel1

            non_null = section['watch1'].notnull()
            high_rel = section['rel1'] == reliability
            #med_rel = section['rel1'] == reliability[1]

            #new_section = section[non_null & (high_rel | med_rel)]
            new_section = section[non_null & high_rel]

            if is_wild == False:
                new_section = new_section[ ID10_condition |  sixmwt_condition]  # becuase sixmwt is invalid for ID10

            #print "new section is: ", new_section

            # new_section = section[cols].loc[section['watch1'].notnull()]
            # new_section = new_section[cols].loc[new_section['rel1']== reliability]

            if len(new_section) == 0:
                no_val_from_watch = True

            plt.figure(figsize=(10, 8))
            matplotlib.rcParams.update({'font.size': 14})

            if not no_val_from_watch:

                #for rel, sub_section in new_section.groupby('rel1'):
                 #   dif = sub_section.BH - sub_section.watch1
                 #   avg = (sub_section.BH + sub_section.watch1) / 2
                  #  plt.plot(avg, dif, "o", color=color_map_dict_1[rel], label = 'Reliability: ' + str(int(rel)))

                dif = new_section['BH'] - new_section['watch1']

                avg = (new_section['BH'] + new_section['watch1']) / 2


                plt.plot(avg, dif, "o", color=color_map_dict_1[reliability] ) #color_map_dict_1[reliability])

                mean_dif = np.mean(dif)
                std_dif = np.std(dif)

                # the approach to get the limits of agreement, based on Bland-Altman
                # 95% of differences will be between these values, if the differences are normally distributed

                agreement_limit_1 = mean_dif - (1.96 * std_dif)
                agreement_limit_2 = mean_dif + (1.96 * std_dif)
                agreement_limit_range = agreement_limit_2 - agreement_limit_1

                axes = plt.gca()
                x_lim_range = axes.get_xlim()

                abline(0, 0, "mediumspringgreen", "--",
                       x_lim_range)  # ideally, we want dif to be equal or close to zero
                abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

                abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
                abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

                slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
                abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

                """
                bland_altman_df = bland_altman_df.append(pd.DataFrame(
                    [[ID, activity, mean_dif, std_dif, agreement_limit_1, agreement_limit_2,
                      agreement_limit_range, slope]],
                    columns=['ID', 'Activity', 'Difference_mean',
                             'Difference_std', 'Agreement_limit_low', 'Agreement_limit_high',
                             'Agreement_limit_range', 'Regression_line_slope']), ignore_index=True)

                bland_altman_df.to_csv(bland_altman_csv_path)
                """
            plt.xlabel("(Bioharness HR value + Watch HR value) / 2")

            if is_wild == False:
                plt.title('Difference over Average\n Setting: Controlled (All Activities)\n' +'COPD: ' + str(COPD) +
                          "    Reliability: " + str(reliability))
                setting = 'Controlled'
            else:
                plt.title('Difference over Average\n  Setting: Uncontrolled\n' + 'COPD: ' + str(COPD) +
                          "    Reliability: " + str(reliability))
                setting = 'Uncontrolled'


            plt.ylabel("Bioharness HR value - Watch HR value")

            axes = plt.gca()
            axes.set_ylim([-30, 65])

            plt.legend()


            fig_name = "/Users/pegah/Documents/VS/graphs/dif_over_avg_controlled_whole/dif_over_avg_" + "rel_" + str(
                reliability)\
                       +"_COPD_" + str(COPD) + "_setting_" + setting + ".png"

            plt.savefig(fig_name)
            #plt.show()




def Bland_Altman_wild_controlled_together(controlled_df , wild_df, reliability):

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'

    for COPD in COPD_list:

        #------------- controlled csv -------------------

        # first we remove the info of ID11 and ID14
        controlled_section = controlled_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')


        ID10_condition = controlled_section['ID'] != 'ID10'
        sixmwt_condition = controlled_section['activity'] != "sixmwt"
        non_null_condition = controlled_section['watch1'].notnull()

        if reliability == 0.1: # to take the rel0 and rel1 data points together
            low_0_rel_condition = controlled_section['rel1'] == 0.0
            low_1_rel_condition = controlled_section['rel1'] == 1.0
            # then we remove every entry that has a reported value from the watch with rel == reliability
            controlled_section = controlled_section[non_null_condition & (low_0_rel_condition | low_1_rel_condition)]

        else:
            high_rel_condition = controlled_section['rel1'] == reliability
            # then we remove every entry that has a reported value from the watch with rel == reliability
            controlled_section = controlled_section[non_null_condition & high_rel_condition]

        # then we also remove sixmwt for ID10
        controlled_section = controlled_section[ID10_condition | sixmwt_condition]


        #------------ wild csv ----------------------

        wild_section = wild_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        non_null_condition = wild_section['watch1'].notnull()

        if reliability == 0.1: # to take the rel0 and rel1 data points together
            low_0_rel_condition = wild_section['rel1'] == 0.0
            low_1_rel_condition = wild_section['rel1'] == 1.0
            wild_section = wild_section[non_null_condition & (low_0_rel_condition | low_1_rel_condition)]

        else:
            high_rel_condition = wild_section['rel1'] == reliability
            wild_section = wild_section[non_null_condition & high_rel_condition]

        #-------------------------------------------


        print "reliability level is: ", reliability
        print "COPD is: ", COPD
        copd_condition = controlled_section['COPD'] == COPD
        controlled_section = controlled_section[copd_condition]

        copd_condition = wild_section['COPD'] == COPD
        wild_section = wild_section[copd_condition]


        controlled_dif = controlled_section['BH'] - controlled_section['watch1']
        controlled_avg = (controlled_section['BH'] + controlled_section['watch1']) / 2

        wild_dif = wild_section['BH'] - wild_section['watch1']
        wild_avg = (wild_section['BH'] + wild_section['watch1']) / 2


        plt.figure(figsize=(12, 12))
        matplotlib.rcParams.update({'font.size': 14})
        axes = plt.gca()

        if reliability == 0.1: # to take the rel0 and rel1 data points together, just use rel1 color
            plt.plot(controlled_avg, controlled_dif, "o", color=color_map_dict_1[1.0])
            plt.plot(wild_avg, wild_dif, "o", color=color_map_dict_1[1.0])
        else:
            plt.plot(controlled_avg, controlled_dif, "o", color=color_map_dict_1[reliability])
            plt.plot(wild_avg, wild_dif, "o", color=color_map_dict_1[reliability])






        axes.set_xlim([40, 140])
        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")

        axes.set_ylim([-30, 70])
        plt.ylabel("Bioharness HR value - Watch HR value")


        dif = pd.concat([controlled_dif, wild_dif], ignore_index=True)
        avg = pd.concat([controlled_avg, wild_avg], ignore_index=True)
        mean_dif = np.mean(dif)

        std_dif = np.std(dif)

        # the approach to get the limits of agreement, based on Bland-Altman
        # 95% of differences will be between these values, if the differences are normally distributed

        agreement_limit_1 = mean_dif - (1.96 * std_dif)
        agreement_limit_2 = mean_dif + (1.96 * std_dif)
        agreement_limit_range = agreement_limit_2 - agreement_limit_1

        x_lim_range = axes.get_xlim()

        abline(0, 0, "mediumspringgreen", "--", x_lim_range, "Zero")  # ideally, we want dif to be equal or close to zero
        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

        slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
        abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

        plt.title('Difference over Average\n  All the data (controlled + uncontrolled) \n'
                  + 'COPD: ' + str(COPD) +
                  "    Reliability: " + str(reliability) + "\n Limits of agreement: " + str(round(agreement_limit_1,3))
                  + " to " + str(round(agreement_limit_2,3)))

        print "limits of agreement are: "
        print agreement_limit_1, agreement_limit_2
        plt.legend()


        fig_name = "/Users/pegah/Documents/VS/graphs/all_the_data/dif_over_avg_" + "rel_" + str(
            reliability) \
                   + "_COPD_" + str(COPD) + ".png"

        #plt.savefig(fig_name)


        #


def Bland_Altman_wild_controlled_together_with_imputation(controlled_df, wild_df, reliability):
    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'

    for COPD in COPD_list:
        # ------------- controlled csv -------------------

        # first we remove the info of ID11 and ID14
        controlled_section = controlled_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')
        controlled_section_ = controlled_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        cols = ['BH', 'watch1', 'rel1']
        controlled_section[cols] = controlled_section_[cols].ffill()  # fill the missing values with the value from the previous data point

        ID10_condition = controlled_section['ID'] != 'ID10'
        sixmwt_condition = controlled_section['activity'] != "sixmwt"
        non_null_condition = controlled_section['watch1'].notnull()
        high_rel_condition = controlled_section['rel1'] == reliability

        # then we remove every entry that has a reported value from the watch with rel == reliability
        controlled_section = controlled_section[non_null_condition & high_rel_condition]

        # then we also remove sixmwt for ID10
        controlled_section = controlled_section[ID10_condition | sixmwt_condition]

        # ------------ wild csv ----------------------

        wild_section = wild_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')
        wild_section_ = wild_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        cols = ['BH', 'watch1', 'rel1']
        wild_section[cols] = wild_section_[cols].ffill()  # fill the missing values with the value from the previous data point

        non_null_condition = wild_section['watch1'].notnull()
        high_rel_condition = wild_section['rel1'] == reliability

        wild_section = wild_section[non_null_condition & high_rel_condition]

        # -------------------------------------------

        print "COPD is: ", COPD
        copd_condition = controlled_section['COPD'] == COPD
        controlled_section = controlled_section[copd_condition]

        copd_condition = wild_section['COPD'] == COPD
        wild_section = wild_section[copd_condition]

        controlled_dif = controlled_section['BH'] - controlled_section['watch1']
        controlled_avg = (controlled_section['BH'] + controlled_section['watch1']) / 2

        wild_dif = wild_section['BH'] - wild_section['watch1']
        wild_avg = (wild_section['BH'] + wild_section['watch1']) / 2

        plt.figure(figsize=(12, 12))
        matplotlib.rcParams.update({'font.size': 14})
        axes = plt.gca()

        plt.plot(controlled_avg, controlled_dif, "o", color=color_map_dict_1[reliability])
        plt.plot(wild_avg, wild_dif, "o", color=color_map_dict_1[reliability])

        axes.set_xlim([40, 140])
        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")

        axes.set_ylim([-30, 70])
        plt.ylabel("Bioharness HR value - Watch HR value")

        dif = pd.concat([controlled_dif, wild_dif], ignore_index=True)
        avg = pd.concat([controlled_avg, wild_avg], ignore_index=True)
        mean_dif = np.mean(dif)

        std_dif = np.std(dif)

        # the approach to get the limits of agreement, based on Bland-Altman
        # 95% of differences will be between these values, if the differences are normally distributed

        agreement_limit_1 = mean_dif - (1.96 * std_dif)
        agreement_limit_2 = mean_dif + (1.96 * std_dif)
        agreement_limit_range = agreement_limit_2 - agreement_limit_1

        x_lim_range = axes.get_xlim()

        abline(0, 0, "mediumspringgreen", "--", x_lim_range,
               "Zero")  # ideally, we want dif to be equal or close to zero
        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

        slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
        abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

        plt.title('Difference over Average\n  All the data (controlled + uncontrolled) \n'
                  + 'COPD: ' + str(COPD) +
                  "    Reliability: " + str(reliability) + "\n Limits of agreement: " + str(round(agreement_limit_1, 3))
                  + " to " + str(round(agreement_limit_2, 3)) +"\nWith Imputation")

        print "limits of agreement are: "
        print agreement_limit_1, agreement_limit_2
        plt.legend()

        fig_name = "/Users/pegah/Documents/VS/graphs/all_the_data/with_imputation/dif_over_avg_" + "rel_" + str(
            reliability) \
                   + "_COPD_" + str(COPD) + "with_imputation.png"

        plt.savefig(fig_name)

def Bland_Altman_wild_controlled_together_with_interpolation(controlled_df, wild_df, reliability):

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'

    for COPD in COPD_list:



        #------------- controlled csv -------------------

        # first we remove the info of ID11 and ID14
        controlled_section = controlled_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        # ---- interpolation -----
        rel = controlled_section['rel1']
        watch = controlled_section['watch1']

        nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
        rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])

        nans, x = np.isnan(watch), lambda z: z.nonzero()[0]
        watch[nans] = np.interp(x(nans), x(~nans), watch[~nans])

        watch = (map(int, watch))

        controlled_section['rel1'] = rel
        controlled_section['watch1'] = watch
        # -----------------------


        ID10_condition = controlled_section['ID'] != 'ID10'
        sixmwt_condition = controlled_section['activity'] != "sixmwt"
        high_rel_condition = controlled_section['rel1'] == reliability

        # then we keep every entry that has a reported value from the watch with rel == reliability
        controlled_section = controlled_section[high_rel_condition]

        # then we also remove sixmwt for ID10
        controlled_section = controlled_section[ID10_condition | sixmwt_condition]



        #------------ wild csv ----------------------

        wild_section = wild_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        # ---- interpolation ----
        rel = wild_section['rel1']
        watch = wild_section['watch1']

        nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
        rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])

        nans, x = np.isnan(watch), lambda z: z.nonzero()[0]
        watch[nans] = np.interp(x(nans), x(~nans), watch[~nans])

        watch = (map(int, watch))

        wild_section['rel1'] = rel
        wild_section['watch1'] = watch
        # -----------------------

        high_rel_condition = wild_section['rel1'] == reliability

        wild_section = wild_section[high_rel_condition]

        #-------------------------------------------



        print "COPD is: ", COPD
        copd_condition = controlled_section['COPD'] == COPD
        controlled_section = controlled_section[copd_condition]

        copd_condition = wild_section['COPD'] == COPD
        wild_section = wild_section[copd_condition]


        controlled_dif = controlled_section['BH'] - controlled_section['watch1']
        controlled_avg = (controlled_section['BH'] + controlled_section['watch1']) / 2

        wild_dif = wild_section['BH'] - wild_section['watch1']
        wild_avg = (wild_section['BH'] + wild_section['watch1']) / 2

        plt.figure(figsize=(12, 12))
        matplotlib.rcParams.update({'font.size': 14})
        axes = plt.gca()

        plt.plot(controlled_avg, controlled_dif, "o", color=color_map_dict_1[reliability])
        plt.plot(wild_avg, wild_dif, "o", color=color_map_dict_1[reliability])



        axes.set_xlim([40, 140])
        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")

        axes.set_ylim([-30, 70])
        plt.ylabel("Bioharness HR value - Watch HR value")


        dif = pd.concat([controlled_dif, wild_dif], ignore_index=True)
        avg = pd.concat([controlled_avg, wild_avg], ignore_index=True)
        mean_dif = np.mean(dif)

        std_dif = np.std(dif)

        # the approach to get the limits of agreement, based on Bland-Altman
        # 95% of differences will be between these values, if the differences are normally distributed

        agreement_limit_1 = mean_dif - (1.96 * std_dif)
        agreement_limit_2 = mean_dif + (1.96 * std_dif)
        agreement_limit_range = agreement_limit_2 - agreement_limit_1

        x_lim_range = axes.get_xlim()

        abline(0, 0, "mediumspringgreen", "--", x_lim_range, "Zero")  # ideally, we want dif to be equal or close to zero
        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

        slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
        abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

        plt.title('Difference over Average\n  All the data (controlled + uncontrolled) \n'
                  + 'COPD: ' + str(COPD) +
                  "    Reliability: " + str(reliability) + "\n Limits of agreement: " + str(round(agreement_limit_1, 3))
                  + " to " + str(round(agreement_limit_2, 3)) + "\nWith Interpolation")

        print "limits of agreement are: "
        print agreement_limit_1, agreement_limit_2
        plt.legend()

        fig_name = "/Users/pegah/Documents/VS/graphs/all_the_data/with_interpolation/dif_over_avg_" + "rel_" + str(
            reliability) \
                   + "_COPD_" + str(COPD) + "with_interpolation.png"
        plt.savefig(fig_name)



def Bland_Altman_wild_controlled_together_with_acceptable_range(controlled_df , wild_df, reliability, range):

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'

    for COPD in COPD_list:
        # ------------- controlled csv -------------------

        # first we remove the info of ID11 and ID14
        controlled_section = controlled_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        ID10_condition = controlled_section['ID'] != 'ID10'
        sixmwt_condition = controlled_section['activity'] != "sixmwt"
        non_null_condition = controlled_section['watch1'].notnull()
        high_rel_condition = controlled_section['rel1'] == reliability

        if range == 5 :
            within_range_condition = controlled_section['within_5'] == True
        elif range == 10:
            within_range_condition = controlled_section['within_10'] == True

        # then we remove every entry that has a reported value from the watch with rel == reliability
        controlled_section = controlled_section[non_null_condition & high_rel_condition & within_range_condition]

        # then we also remove sixmwt for ID10
        controlled_section = controlled_section[ID10_condition | sixmwt_condition]

        # ------------ wild csv ----------------------

        wild_section = wild_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        non_null_condition = wild_section['watch1'].notnull()
        high_rel_condition = wild_section['rel1'] == reliability

        if range == 5 :
            within_range_condition = wild_section['within_5'] == True
        elif range == 10:
            within_range_condition = wild_section['within_10'] == True

        wild_section = wild_section[non_null_condition & high_rel_condition & within_range_condition]

        # -------------------------------------------

        print
        "COPD is: ", COPD
        copd_condition = controlled_section['COPD'] == COPD
        controlled_section = controlled_section[copd_condition]

        copd_condition = wild_section['COPD'] == COPD
        wild_section = wild_section[copd_condition]

        controlled_dif = controlled_section['BH'] - controlled_section['watch1']
        controlled_avg = (controlled_section['BH'] + controlled_section['watch1']) / 2

        wild_dif = wild_section['BH'] - wild_section['watch1']
        wild_avg = (wild_section['BH'] + wild_section['watch1']) / 2

        plt.figure(figsize=(12, 12))
        matplotlib.rcParams.update({'font.size': 14})
        axes = plt.gca()

        plt.plot(controlled_avg, controlled_dif, "o", color='darkslategrey')
        plt.plot(wild_avg, wild_dif, "o", color='darkslategrey')

        axes.set_xlim([40, 140])
        plt.xlabel("(Bioharness HR value + Watch HR value) / 2")

        axes.set_ylim([-30, 70])
        plt.ylabel("Bioharness HR value - Watch HR value")

        dif = pd.concat([controlled_dif, wild_dif], ignore_index=True)
        avg = pd.concat([controlled_avg, wild_avg], ignore_index=True)
        mean_dif = np.mean(dif)

        std_dif = np.std(dif)

        # the approach to get the limits of agreement, based on Bland-Altman
        # 95% of differences will be between these values, if the differences are normally distributed

        agreement_limit_1 = mean_dif - (1.96 * std_dif)
        agreement_limit_2 = mean_dif + (1.96 * std_dif)
        agreement_limit_range = agreement_limit_2 - agreement_limit_1

        x_lim_range = axes.get_xlim()

        abline(0, 0, "mediumspringgreen", "--", x_lim_range,
               "Zero")  # ideally, we want dif to be equal or close to zero
        abline(0, mean_dif, "crimson", "-", x_lim_range, "Mean Difference")

        abline(0, agreement_limit_1, "darkslateblue", "-.", x_lim_range, "Limits of Agreement")
        abline(0, agreement_limit_2, "darkslateblue", "-.", x_lim_range)

        slope, intercept, r_value, p_value, std_err = stats.linregress(avg, dif)
        abline(slope, intercept, "orchid", "-", x_lim_range, "Regression Line")

        plt.title('Difference over Average\n  All the data (controlled + uncontrolled) \n'
                  + 'COPD: ' + str(COPD) +
                  "    Reliability: " + str(reliability) + "\n watch and BH heart rate values within range: " + str(range)+ " bpm"
                  "\n Limits of agreement: " + str(round(agreement_limit_1, 3))
                  + " to " + str(round(agreement_limit_2, 3)))

        print "limits of agreement are: "
        print agreement_limit_1, agreement_limit_2
        plt.legend()

        fig_name = "/Users/pegah/Documents/VS/graphs/all_the_data/within_range/dif_over_avg_" + "rel_" + str(
            reliability) \
                   + "range_" + str(range) +"_COPD_" + str(COPD) + ".png"

        plt.savefig(fig_name)





if __name__ == '__main__':


    # -------- wild -------

    wild_csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild_with_acceptable_column.csv"
    wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])

    #dif_mean_high_rel_across_healthiness_wild(wild_df)


    #csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
    #df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])
    #Bland_Altman_based_on_accel(wild_df, 9.7)

    #percentile_list = helper_get_percentiles(wild_df, 5, "accel", 9.77, 0.01)
    #Bland_Altman_based_on_accel_gyro(wild_df, percentile_list, "accel", 9.77, 0.01)





    # ---------------------
    csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_controlled_with_acceptable_column.csv"
    controlled_df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])

    #dif_mean_graph_across_participants(df)

    rel_list = [0.0, 1.0, 2.0, 3.0]
    for rel in rel_list:
        Bland_Altman_wild_controlled_together(controlled_df, wild_df, rel)
        #Bland_Altman_wild_controlled_together_with_imputation(controlled_df, wild_df, rel)
        #Bland_Altman_wild_controlled_together_with_interpolation(controlled_df, wild_df, rel)


    # if we count both level 0 and level 1 reliability together as "low":
    #rel = 0.1
    #Bland_Altman_wild_controlled_together(controlled_df, wild_df, rel)




    #Bland_Altman_wild_controlled_together_with_acceptable_range(controlled_df, wild_df, 3.0, 10)

    #dif_mean_high_rel_per_activity_per_healthiness(df, 3.0)
    #dif_over_mean_controlled_per_healthiness_per_reliability(df, 0.0, False)


    
    normal_dif_dict = dict((el, []) for el in ID_list)

    #dif_mean_high_rel_per_activity_per_person(df, 3.0)

    #dif_mean_updated(df, 2.0)
    #dif_mean_high_rel_per_activity_per_healthiness(df, 3)
    #dif_mean_high_rel_per_activity_per_person(df, 3.0)
    #dif_mean_high_rel_per_activity_across_participants(df, 3.0)

    #sort_csv_by_column()


    #dif_mean_graph(df)
    #dif_mean_graph_across_participants(df)

    #result_path = system_path + "BA_res/Bland_Altman_Result"
    #result_file = open(result_path, "w")
    #sorted_res_path = system_path + "BA_res/Bland_Altman_Result_sorted"

    #result_across_participant_path = system_path +  "BA_res/Bland_Altman_Result_Across"
    #result_across_participant_file = open(result_across_participant_path, "w")
    #sorted_res_across_path = system_path +  "BA_res/Bland_Altman_Result_Across_sorted"

    #sort_BA_res(result_across_participant_path, sorted_res_across_path)



    #for sign in sign_list:
     #   for imputing in imputing_list:
      #      dif_avg_reg_line_slope_analysis(sign, imputing)
    """
    ID = 'ID02'
    activity = 'sixmwt'

    dif_mean_graph(ID, activity, df, True, False)
    
    """

    """
    for ID in ID_list:
        for activity in activity_list:

            #shapirot_res = perform_shapiro_test(ID, activity, df, True)
            #if shapirot_res[0]:
                #count += 1
                #normal_dif_dict[ID].append((activity, shapirot_res[1]))

            #dif_distribution_plot(ID, activity, df)
            dif_mean_graph(ID, activity, df, False, False) # first bool is imputing second bool is gold
            dif_mean_graph(ID, activity, df, False, True)
            dif_mean_graph(ID, activity, df, True, False)
            dif_mean_graph(ID, activity, df, True, True)
    """
    """
    for activity in activity_list:
        #dif_distribution_plot_across_participants(activity, df)

        dif_mean_graph_across_participants(activity, df, False, False)
        dif_mean_graph_across_participants(activity, df, False, True)
        dif_mean_graph_across_participants(activity, df, True, False)
        dif_mean_graph_across_participants(activity, df, True, True)

    """
