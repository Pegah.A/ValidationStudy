# I want to have some code for general statistics here
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

ID_list =['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID10','ID12','ID13', 'ID14', 'ID15', 'ID16' ]
activity_list = ["sixmwt", "sitting", "walking" , "lying" , "standing", "eating" , "brushing" ] #,"climbup",  "climbdown"]

activity_time_dict = {"sixmwt" : 360, "sitting" : 240, "walking": 240 , "lying": 240 , "standing": 240,
                      "eating" : 180, "brushing" : 120 } #, "climbup": "NA" ,  "climbdown": "NA"}

COPD_list = [True, False]

system_path = "/Users/pegah/Documents/VS/"
def count_reliability_per_person_per_activity(dataframe):

    # I want to know for each participant and each activity how many highly reliable data ppints were reported by the watch without imputing
    count_rel_path = system_path + "BA_res/reliability_count_info_july.csv"
    rel_count_df = pd.DataFrame(columns=['ID', 'Activity', 'High_Rel_Count', ' Med_Rel_Count' , 'Low_Rel_Count',
                                         'Total', 'High_Rel_%', 'Medium_Rel_%', 'Low_Rel_%'])

    for ID in ID_list:
        for activity in activity_list:

            section = dataframe.query('activity==@activity and ID == @ID')
            rel1 = section['rel1']

            count_high = 0
            count_med = 0
            count_low = 0

            for i in range(len(rel1)):
                if rel1.iloc[i] == 3:
                    count_high += 1
                elif rel1.iloc[i] == 2:
                    count_med += 1
                elif rel1.iloc[i] == 1:
                    count_low += 1

            duration = activity_time_dict[activity]

            if duration != "NA":
                percentage_high = float(count_high)/duration
                percentage_med = float(count_med) / duration
                percentage_low = float(count_low) / duration
            else:
                percentage_high = "NA"
                percentage_med = "NA"
                percentage_low = "NA"

            rel_count_df = rel_count_df.append(pd.DataFrame([[ID, activity, count_high, count_med, count_low,
                                                              duration, percentage_high, percentage_med, percentage_low]],
                    columns=['ID', 'Activity', 'High_Rel_Count', ' Med_Rel_Count' ,
                             'Low_Rel_Count','Total', 'High_Rel_%',
                             'Med_Rel_%', 'Low_Rel_%']),ignore_index=True)


    rel_count_df.to_csv(count_rel_path, columns=['ID', 'Activity', 'High_Rel_Count', ' Med_Rel_Count' ,
                             'Low_Rel_Count','Total', 'High_Rel_%',
                             'Med_Rel_%', 'Low_Rel_%'])


def count_reliability_per_activity_per_healthiness(dataframe):

    # I want to know for each activity and each group of participants (COPD and non-COPD) how many highly reliable
    # data ppints were reported by the watch without imputing
    count_rel_path = system_path + "BA_res/reliability_count_info_per_healthiness_july.csv"
    rel_count_df = pd.DataFrame(columns=['COPD', 'Activity', 'High_Rel_Count', ' Med_Rel_Count' , 'Low_Rel_Count',
                                         'Total', 'High_Rel_%', 'Medium_Rel_%', 'Low_Rel_%'])

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'
    invalid_ID_3 = 'ID10'
    for COPD in COPD_list:
        for activity in activity_list:

            section = dataframe.query('activity==@activity and COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2')

            if activity == "sixmwt":
                section = dataframe.query(
                    'activity==@activity and COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2 and ID != @invalid_ID_3')
            #print section
            rel1 = section['rel1']

            count_high = 0
            count_med = 0
            count_low = 0

            for i in range(len(rel1)):
                if rel1.iloc[i] == 3:
                    count_high += 1
                elif rel1.iloc[i] == 2:
                    count_med += 1
                elif rel1.iloc[i] == 1:
                    count_low += 1



            if activity_time_dict[activity] != "NA":
                if not COPD:
                    duration = activity_time_dict[activity] * 7
                else:
                    if activity == "sixmwt":
                        duration = activity_time_dict[activity] * 5
                    else:
                        duration = activity_time_dict[activity] * 6

                print "here in not NA duration is: ", duration

                percentage_high = float(count_high)/duration
                percentage_med = float(count_med) / duration
                percentage_low = float(count_low) / duration
            else:
                percentage_high = "NA"
                percentage_med = "NA"
                percentage_low = "NA"
                duration = "NA"

            rel_count_df = rel_count_df.append(pd.DataFrame([[COPD, activity, count_high, count_med, count_low,
                                                              duration, percentage_high, percentage_med, percentage_low]],
                    columns=['COPD', 'Activity', 'High_Rel_Count', ' Med_Rel_Count' ,
                             'Low_Rel_Count','Total', 'High_Rel_%',
                             'Med_Rel_%', 'Low_Rel_%']),ignore_index=True)


    rel_count_df.to_csv(count_rel_path, columns=['COPD', 'Activity', 'High_Rel_Count', ' Med_Rel_Count' ,
                             'Low_Rel_Count','Total', 'High_Rel_%',
                             'Med_Rel_%', 'Low_Rel_%'])


# what is the BH heart rate value for the cases in which the watch thinks its reported value is reliable
def BH_vs_watch_reliable_data(dataframe, reliability):

    BH_vs_watch_rel_path = system_path + "BA_res/BH_vs_watch_reliable_" + str(reliability)+ "_points.csv"
    BH_vs_watch_df = pd.DataFrame(columns=['ID', 'Activity', 'ts','BH_value', 'watch1_value', 'Difference'])

    for ID in ID_list:
        for activity in activity_list:
            section = dataframe.query('activity==@activity and ID == @ID')
            rel1 = section['rel1']
            BH = section['BH']
            watch1 = section['watch1']
            ts = section['ts']

            for i in range(len(rel1)):
                if rel1.iloc[i] == reliability:  # if this data is reliable according to the watch
                    dif = BH.iloc[i]- watch1.iloc[i]

                    BH_vs_watch_df = BH_vs_watch_df.append(pd.DataFrame([[ID, activity , ts.iloc[i], BH.iloc[i], watch1.iloc[i], dif]],
                                                                    columns=['ID', 'Activity','ts','BH_value', 'watch1_value', 'Difference']), ignore_index=True)

    BH_vs_watch_df.to_csv(BH_vs_watch_rel_path)


def count_reliability_per_person_per_activity_with_window(dataframe, window_size):
    # I want to know for each participant and each activity how many highly reliable data
    # pints were reported by the watch without imputing
    # I want to divide each activity into windows of size "window_size". And I want to calculate the percentage
    # of each reliability within that window.

    # I can generate a text file for each activity.

    for activity in activity_list:

        if activity not in['climbup','climbdown']:

            rel_info_df = pd.DataFrame(columns=['ID', 'COPD', 'Low_Rel_Count', ' Med_Rel_Count', 'High_Rel_Count',
                                                  'Low_Rel_%', 'Med_Rel_%', 'High_Rel_%'])

            rel_info_path = system_path + "BA_res/clustering_files/reliability_count_info_" + activity + \
                            "_window_" + str(window_size) +".csv"

            for ID in ID_list:

                section = dataframe.query('activity==@activity and ID == @ID')
                rel1 = section["rel1"]

                num_of_windows = activity_time_dict[activity] / window_size

                start_index = 0

                for i in range(num_of_windows):
                    count_high = 0
                    count_med = 0
                    count_low = 0

                    for j in range(start_index , start_index + window_size):
                        if rel1.iloc[j] == 3:
                            count_high += 1
                        elif rel1.iloc[j] == 2:
                            count_med += 1
                        elif rel1.iloc[j] == 1:
                            count_low += 1

                    print "activity is: ", activity
                    print "ID is: ", ID
                    print "start index is: ", start_index
                    start_index += window_size

                    total_reported = count_high + count_med + count_low
                    if total_reported == 0:
                        percentage_high = 0
                        percentage_med = 0
                        percentage_low = 0
                    else:
                        percentage_high = (float(count_high)/total_reported) * 100
                        percentage_med = (float(count_med) / total_reported) * 100
                        percentage_low = (float(count_low) / total_reported) * 100

                    if int(ID[2:4]) > 8:
                        COPD = True
                    else:
                        COPD = False


                    rel_info_df = rel_info_df.append(pd.DataFrame([[ID, COPD, count_low, count_med, count_high,
                                                                      percentage_low, percentage_med,
                                                                      percentage_high]],
                                                                    columns=['ID', 'COPD', 'Low_Rel_Count',
                                                                             'Med_Rel_Count',
                                                                             'High_Rel_Count', 'Low_Rel_%',
                                                                             'Med_Rel_%', 'High_Rel_%']),
                                                       ignore_index=True)


            rel_info_df.to_csv(rel_info_path, columns=['ID', 'COPD', 'Low_Rel_Count', 'Med_Rel_Count' ,
                             'High_Rel_Count','Low_Rel_%',
                             'Med_Rel_%', 'High_Rel_%'])


def count_watch_reports_per_activity_per_healthiness(dataframe):
    # I am checking the values from the watch. So I should ignore those participants for which the watch1 values are invalid.
    # which is: ID 11

    reported_count_df = pd.DataFrame(columns=['Activity', 'COPD', 'Num of Participants','Activity Length',
                                              'Avg Num of Reported','SD', 'Min', 'Max','Percentage'])
    reported_count_path = system_path + "watch_values_info/reported_count_info.csv"
    for activity in activity_list:

        for COPD in COPD_list:
            invalid_ID = 'ID11'
            if activity not in ['climbup', 'climbdown']:
                section = dataframe.query('activity==@activity and COPD == @COPD and ID != @invalid_ID')
                num_of_reported_values_list = []

                activity_len = activity_time_dict[activity]

                for ID, sub_section in section.groupby('ID'):


                    watch1 = sub_section['watch1'][:activity_len].loc[sub_section['watch1'].notnull()]
                    watch1_count = watch1.size
                    num_of_reported_values_list.append(watch1_count)

                    print "activity is :", activity
                    print "ID is: ", ID
                    print "watch1 count is: ", watch1_count

                print num_of_reported_values_list
                avg = np.average(num_of_reported_values_list)
                sd = np.std(num_of_reported_values_list)

                num_of_participants = len(num_of_reported_values_list)
                total = activity_len * num_of_participants

                min_count = min(num_of_reported_values_list)
                max_count = max(num_of_reported_values_list)

                percentage = (avg / activity_len) * 100
                reported_count_df = reported_count_df.append(pd.DataFrame([[activity, COPD, num_of_participants, activity_len, avg, sd, min_count,
                                                                            max_count, percentage]],
                                                                columns=['Activity', 'COPD', 'Num of Participants','Activity Length',
                                              'Avg Num of Reported','SD', 'Min', 'Max','Percentage']),
                                                        ignore_index=True)

    reported_count_df.to_csv(reported_count_path)


def gap_length_count(dataframe):

    activity_gap_info_dict = {}
    for activity in activity_list: # we don't consider climbing activities
        max_per_person_list = []
        avg_per_person_list = []

        for ID in ID_list:

            section = dataframe.query('activity==@activity and ID == @ID')
            watch1 = section['watch1']

            gap_len_list = []
            activity_len = activity_time_dict[activity]


            index  = 0
            while index < activity_len:
                null_count = 0

                while index < (activity_len) and np.isnan(watch1.iloc[index]) :
                    null_count += 1
                    index += 1

                gap_len_list.append(null_count)
                index += 1


            gaps_avg = np.average(gap_len_list)
            gaps_max = max(gap_len_list)

            max_per_person_list.append(gaps_max)
            avg_per_person_list.append(gaps_avg)


        activity_gap_info_dict[activity] = (max(max_per_person_list) , np.average(avg_per_person_list))

    print activity_gap_info_dict


def gap_length_count_version_2_per_healthiness(dataframe):

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'
    invalid_ID_3 = 'ID10'

    activity_gap_info_dict = {}
    for COPD in COPD_list:
        for activity in activity_list:

            section = dataframe.query('activity == @activity and COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2')
            if activity == 'sixmwt':
                section = dataframe.query(
                    'activity == @activity and COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2 and ID != @invalid_ID_3')

            index = 0
            gap_len_list = []


            while index < len(section):
                null_count = 0
                while index < len(section) and np.isnan(section['watch1'].iloc[index]):
                    null_count += 1
                    index += 1

                gap_len_list.append(null_count)
                index += 1

            #print gap_len_list

            activity_gap_info_dict[activity] = (max(gap_len_list), np.mean(gap_len_list))

        print "COPD is: ", COPD
        print activity_gap_info_dict



def plot_count_reliability_bar_graph_high_med_low_compare():
    csv_path = "/Users/pegah/Documents/VS/BA_res/reliability_count_info_per_healthiness.csv"
    df = pd.read_csv(csv_path)

    graphs_path = "/Users/pegah/Documents/VS/graphs/rel_info/"

    high_rel_COPD_list_over_all = []
    high_rel_non_COPD_list_over_all = []

    med_rel_COPD_list_over_all = []
    med_rel_non_COPD_list_over_all = []

    low_rel_COPD_list_over_all = []
    low_rel_non_COPD_list_over_all = []

    high_rel_COPD_list_over_reported = []
    high_rel_non_COPD_list_over_reported = []

    med_rel_COPD_list_over_reported = []
    med_rel_non_COPD_list_over_reported = []

    low_rel_COPD_list_over_reported = []
    low_rel_non_COPD_list_over_reported = []

    for activity in activity_list:
        for COPD in COPD_list:

            section = df.query('Activity==@activity and COPD == @COPD')
            high_rel_count = int(section['High_Rel_Count'])
            med_rel_count = int(section[' Med_Rel_Count'])
            low_rel_count = int(section['Low_Rel_Count'])

            total = int(section['Total'])
            reported_total = high_rel_count + med_rel_count + low_rel_count

            high_rel_percent_over_all = (float(high_rel_count) / total) * 100
            med_rel_percent_over_all = (float(med_rel_count) / total) * 100
            low_rel_percent_over_all = (float(low_rel_count) / total) * 100

            high_rel_percent_over_reported = (float(high_rel_count) / reported_total) * 100
            med_rel_percent_over_reported = (float(med_rel_count) / reported_total) * 100
            low_rel_percent_over_reported = (float(low_rel_count) / reported_total) * 100

            if COPD:
                high_rel_COPD_list_over_all.append(high_rel_percent_over_all)
                med_rel_COPD_list_over_all.append(med_rel_percent_over_all)
                low_rel_COPD_list_over_all.append(low_rel_percent_over_all)

                high_rel_COPD_list_over_reported.append(high_rel_percent_over_reported)
                med_rel_COPD_list_over_reported.append(med_rel_percent_over_reported)
                low_rel_COPD_list_over_reported.append(low_rel_percent_over_reported)


            else:
                high_rel_non_COPD_list_over_all.append(high_rel_percent_over_all)
                med_rel_non_COPD_list_over_all.append(med_rel_percent_over_all)
                low_rel_non_COPD_list_over_all.append(low_rel_percent_over_all)

                high_rel_non_COPD_list_over_reported.append(high_rel_percent_over_reported)
                med_rel_non_COPD_list_over_reported.append(med_rel_percent_over_reported)
                low_rel_non_COPD_list_over_reported.append(low_rel_percent_over_reported)

    matplotlib.rcParams.update({'font.size': 16})
    plt.figure(figsize=(10, 8))
    x1 = [1, 6, 11, 16, 21, 26, 31]
    x2 = [2, 7, 12, 17, 22, 27, 32]
    x3 = [3, 8, 13, 18, 23, 28, 33]
    x4 = [sum(x) for x in zip(x1, x2, x3)]
    x4 = [x / 3.0 for x in x4]


    plt.bar(x1, high_rel_COPD_list_over_all, color="navy", label='High Rel')
    plt.bar(x2, med_rel_COPD_list_over_all, color="darkturquoise", label='Med Rel')
    plt.bar(x3, low_rel_COPD_list_over_all, color="steelblue", label='Low Rel')

    plt.xticks(x4, activity_list)

    plt.title("Watch Coverage \nPer Activity, Per Reliability \nCOPD Participants")
    plt.ylabel("Watch coverage of different reliabilities (%)")

    plt.legend()
    plt.grid()

    fig_path = graphs_path + "Coverage_per_activity_per_healthiness_high_med_low_bars_COPD_over_all.png"
    plt.savefig(fig_path)
    #plt.show()
    #-----

    plt.figure(figsize=(10, 8))
    x1 = [1, 6, 11, 16, 21, 26, 31]
    x2 = [2, 7, 12, 17, 22, 27, 32]
    x3 = [3, 8, 13, 18, 23, 28, 33]
    x4 = [sum(x) for x in zip(x1, x2, x3)]
    x4 = [x / 3.0 for x in x4]

    plt.bar(x1, high_rel_non_COPD_list_over_all, color="navy", label='High Rel')
    plt.bar(x2, med_rel_non_COPD_list_over_all, color="darkturquoise", label='Med Rel')
    plt.bar(x3, low_rel_non_COPD_list_over_all, color="steelblue", label='Low Rel')

    plt.xticks(x4, activity_list)

    plt.title("Watch Coverage \nPer Activity, Per Reliability \nnon-COPD Participants")
    plt.ylabel("Watch coverage of different reliabilities (%)")

    plt.legend()
    plt.grid()

    fig_path = graphs_path + "Coverage_per_activity_per_healthiness_high_med_low_bars_nonCOPD_over_all.png"
    plt.savefig(fig_path)
    #plt.show()



def plot_count_reliability_bar_graph_COPD_nonCOP_compare():
    csv_path = "/Users/pegah/Documents/VS/BA_res/reliability_count_info_per_healthiness.csv"
    df = pd.read_csv(csv_path)

    graphs_path = "/Users/pegah/Documents/VS/graphs/rel_info/"

    # for every category of health and every activity, I want high rel count and med rel count and the total
    # I will calculate high rel percentage and high+med rel percentage and draw them as bars

    x_ticks_list = []
    high_rel_COPD_list = []
    high_rel_non_COPD_list = []

    high_med_rel_COPD_list = []
    high_med_rel_non_COPD_list = []

    for activity in activity_list:
        for COPD in COPD_list:

            section = df.query('Activity==@activity and COPD == @COPD')
            high_rel_count = int(section['High_Rel_Count'])
            med_rel_count = int(section[' Med_Rel_Count'])
            high_med_rel_count = high_rel_count + med_rel_count
            total =  int(section['Total'])

            high_rel_percent = (float(high_rel_count) / total ) * 100
            high_med_rel_percent = (float(high_med_rel_count) / total ) * 100



            if COPD:
                high_rel_COPD_list.append(high_rel_percent)
                high_med_rel_COPD_list.append(high_med_rel_percent)
            else:
                high_rel_non_COPD_list.append(high_rel_percent)
                high_med_rel_non_COPD_list.append(high_med_rel_percent)

    matplotlib.rcParams.update({'font.size': 16})
    plt.figure(figsize=(10, 8))
    x1 = [1,5,8,11,14,17,20]
    x2 = [2,6,9,12,15,18,21]
    x3 = [sum(x) for x in zip(x1, x2)]
    x3 = [x / 2.0 for x in x3]

    plt.bar(x1,high_rel_non_COPD_list , color = "orchid", label = 'non-COPD')
    plt.xticks(x3, activity_list)
    plt.bar(x2, high_rel_COPD_list, color = "darkslateblue",label = 'COPD')

    plt.title("Watch Coverage per Activity\nHigh Reliability")
    plt.ylabel("Watch coverage with high reliability (%)")

    plt.legend()
    plt.grid()

    fig_path = graphs_path + "Coverage_per_activity_per_healthiness_high.png"
    plt.savefig(fig_path)
    #plt.show()

    #edgecolor='black', hatch="///"

    plt.figure(figsize=(10, 8))
    x1 = [1,5,8,11,14,17,20]
    x2 = [2,6,9,12,15,18,21]
    x3 = [sum(x) for x in zip(x1, x2)]
    x3 = [x / 2.0 for x in x3]

    plt.bar(x1,high_med_rel_non_COPD_list , color = "orchid", label = 'non-COPD')
    plt.xticks(x3, activity_list)
    plt.bar(x2, high_med_rel_COPD_list, color = "darkslateblue",label = 'COPD')

    plt.title("Watch Coverage per Activity\nHigh and Medium Reliability")
    plt.ylabel("Watch coverage with high and medium reliability (%)")

    plt.legend()
    plt.grid()

    fig_path = graphs_path + "Coverage_per_activity_per_healthiness_high_med.png"
    plt.savefig(fig_path)

    #plt.show()


def plot_rel_count_bar_graph_per_healthiness_across_activities(dataframe, is_wild):


    for COPD in COPD_list:
        invalid_ID_1 = 'ID11'
        invalid_ID_2 = 'ID14'

        section = dataframe.query('COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2')

        rel_not_null_condition = np.isnan(section['rel1']) == False

        if is_wild == False:
            sixmwt_condition = section ['activity'] != 'sixmwt'
            invalid_ID_3_condition = section['ID'] != 'ID10'
            section = section[sixmwt_condition | invalid_ID_3_condition]

        section = section[rel_not_null_condition]

        rell_0_condition = section['rel1'] == 0.0
        rel_1_condition = section['rel1'] == 1.0
        rel_2_condition = section['rel1'] == 2.0
        rel_3_condition = section['rel1'] == 3.0

        rel_0_section = section[rell_0_condition]
        rel_1_section = section[rel_1_condition]
        rel_2_section = section[rel_2_condition]
        rel_3_section = section[rel_3_condition]


        rel_0_count = len(rel_0_section)
        rel_1_count = len(rel_1_section)
        rel_2_count = len(rel_2_section)
        rel_3_count = len(rel_3_section)

        rel_count_list = [rel_0_count, rel_1_count, rel_2_count, rel_3_count ]
        rel_list = ['rel 0', 'rel 1', 'rel 2', 'rel 3']

        matplotlib.rcParams.update({'font.size': 16})
        plt.figure(figsize=(10, 8))
        x = [1, 3, 5, 7]

        if is_wild:
            plt.ylim(0,14500)
            setting = 'Uncontrolled'

        else:
            plt.ylim(0,2000)
            setting = 'Controlled'


        if COPD:
            if is_wild:
                bar_color = 'indigo'
            else:
                bar_color = 'mediumpurple'

        else:
            if is_wild:
                bar_color = 'mediumvioletred'
            else:
                bar_color = 'hotpink'


        plt.bar(x, rel_count_list, color= bar_color, width = 0.5)

        plt.xticks(x, rel_list)

        plt.title("Number of heart rate readings from the smart watch\nfor each level of reliability\n" +
                  "Setting: " + setting+"    COPD: " + str(COPD))
        plt.ylabel("Number of readings")
        plt.xlabel("Levels of reliability")

        plt.legend()
        plt.grid()

        graphs_path = "/Users/pegah/Documents/VS/graphs/rel_info/"
        fig_path = graphs_path + "per_healthiness_per_rel_per_setting/rel_count_" + setting + "_" + str(COPD) + ".png"
        plt.savefig(fig_path)
        #plt.show()



def high_rel_count_wild(dataframe):


    # for each person i want to count the total number of rows and the number of reported values from the watch with high rel

    total_count = 0
    total_high_rel_count = 0


    #ID_list_ = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']  # COPD
    ID_list_ = ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08']  # non-COPD
    for ID in ID_list_:
        section = dataframe.query('ID == @ID')

        BH = section['BH']
        high_rel = section['rel1'] == 3.0
        med_rel = section['rel1'] == 2.0

        new_section = section[high_rel]
        #new_section = section[high_rel | med_rel]





        print "ID is: ", ID
        print "BH count is: ", BH.shape[0]
        print "high rel count is: ", new_section.shape[0]
        print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100


        total_count += BH.shape[0]
        total_high_rel_count += new_section.shape[0]

    print "total percentage is: ", (float(total_high_rel_count) / total_count) * 100



def high_rel_count_wild_with_imputation(dataframe):


    # for each person i want to count the total number of rows and the number of reported values from the watch with high rel

    total_count = 0
    total_high_rel_count = 0

    ID_list_ =['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in ID_list_:
        section = dataframe.query('ID == @ID')
        section_ = wild_df.query('ID == @ID')

        cols = ['BH', 'watch1', 'rel1']
        section[cols] = section_[cols].ffill()  # fill the missing values with the value from the previous data point

        BH = section['BH']
        high_rel = section['rel1'] == 3.0
        med_rel = section['rel1'] == 2.0

        new_section = section[high_rel]
        #new_section = section[high_rel | med_rel]





        print "ID is: ", ID
        print "BH count is: ", BH.shape[0]
        print "high rel count is: ", new_section.shape[0]
        print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100


        total_count += BH.shape[0]
        total_high_rel_count += new_section.shape[0]

    print "total percentage is: ", (float(total_high_rel_count) / total_count) * 100


def high_rel_count_wild_with_interpolation(dataframe):
    total_count = 0
    total_high_rel_count = 0

    ID_list_ = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16'] #['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07','ID08'] # , 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in ID_list_:
        section = dataframe.query('ID == @ID')

        rel = section['rel1']

        nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
        rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])

        high_rel_count = np.count_nonzero(rel == 3.0)


        BH = section['BH']


        print "ID is: ", ID
        print "BH count is: ", BH.shape[0]
        print "high rel count is: ", high_rel_count
        print "percentage is: ", (float(high_rel_count) / BH.shape[0]) * 100

        total_count += BH.shape[0]
        total_high_rel_count += high_rel_count

    print "total percentage is: ", (float(total_high_rel_count) / total_count) * 100


def  mean_abs_error_for_rel(dataframe, reliability):

    for COPD in COPD_list:
        for activity in activity_list:

            invalid_ID_1 = 'ID11'
            invalid_ID_2 = 'ID14'

            section = dataframe.query('activity == @activity and COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2')
            if activity == 'sixmwt':
                invalid_ID_3 = 'ID10'
                section = dataframe.query('activity == @activity and COPD == @COPD and ID != @invalid_ID_1 '
                                          'and ID != @invalid_ID_2 and ID != @invalid_ID_3')



            # ------
            high_rel = section['rel1'] == 3.0
            med_rel = section['rel1'] == 2.0
            low_rel = section['rel1'] == 1.0

            high_sec= section[high_rel]
            med_sec = section[med_rel]
            low_sec = section[low_rel]

            print len(high_sec)
            print len(med_sec)
            print len(low_sec)

            # -------
            rel_condition = section['rel1'] == reliability
            new_section = section[rel_condition]

            BH = new_section['BH']
            watch = new_section['watch1']

            sub = np.subtract(BH, watch)
            abs_sub = np.abs(sub)
            mean_abs_sub = np.mean(abs_sub)

            print "activity is: ", activity
            print "COPD is: ", COPD
            print "mean abs error is: ", mean_abs_sub

            print "size after is: ", len(BH)



def mean_abs_error_all_data_together_for_rel(controlled_df, wild_df, reliability):
    """
    Gets the valid data points from controlled and wild based on reliability.
    Computes the difference between BH value and smartwatch value for these datapoints.
    Computes the mean absolute difference for COPD and non-COPD.

    :param wild_dataframe:
    :param controlled_dataframe:
    :param relialibility:
    :return:
    """

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'

    for COPD in COPD_list:

        # ------------- controlled csv -------------------

        # first we remove the info of ID11 and ID14
        controlled_section = controlled_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        ID10_condition = controlled_section['ID'] != 'ID10'
        sixmwt_condition = controlled_section['activity'] != "sixmwt"
        non_null_condition = controlled_section['watch1'].notnull()

        if reliability == 0.1:  # to take the rel0 and rel1 data points together
            low_0_rel_condition = controlled_section['rel1'] == 0.0
            low_1_rel_condition = controlled_section['rel1'] == 1.0
            # then we remove every entry that has a reported value from the watch with rel == reliability
            controlled_section = controlled_section[non_null_condition & (low_0_rel_condition | low_1_rel_condition)]

        else:
            high_rel_condition = controlled_section['rel1'] == reliability
            # then we remove every entry that has a reported value from the watch with rel == reliability
            controlled_section = controlled_section[non_null_condition & high_rel_condition]

        # then we also remove sixmwt for ID10
        controlled_section = controlled_section[ID10_condition | sixmwt_condition]

        # ------------ wild csv ----------------------

        wild_section = wild_df.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

        non_null_condition = wild_section['watch1'].notnull()

        if reliability == 0.1:  # to take the rel0 and rel1 data points together
            low_0_rel_condition = wild_section['rel1'] == 0.0
            low_1_rel_condition = wild_section['rel1'] == 1.0
            wild_section = wild_section[non_null_condition & (low_0_rel_condition | low_1_rel_condition)]

        else:
            high_rel_condition = wild_section['rel1'] == reliability
            wild_section = wild_section[non_null_condition & high_rel_condition]

        # -------------------------------------------

        print "reliability level is: ", reliability
        print "COPD is: ", COPD
        copd_condition = controlled_section['COPD'] == COPD
        controlled_section = controlled_section[copd_condition]

        copd_condition = wild_section['COPD'] == COPD
        wild_section = wild_section[copd_condition]

        controlled_dif = controlled_section['BH'] - controlled_section['watch1']
        wild_dif = wild_section['BH'] - wild_section['watch1']

        controlled_dif_abs = np.abs(controlled_dif)
        wild_dif_abs = np.abs(wild_dif)

        dif_abs = pd.concat([controlled_dif_abs, wild_dif_abs], ignore_index=True)

        mean_abs_dif = np.mean(dif_abs)
        print mean_abs_dif











def check_before_after_high_rel(controlled_df, wild_df):




    for COPD in COPD_list:

        # ---------------------------------- Controlled -------------------------------
        """
        length = 0
        num_of_0 = 0
        num_of_1 = 0
        num_of_other  = 0

        num_of_med_high_after = 0

        for activity in activity_list:

            invalid_ID_1 = 'ID11'
            invalid_ID_2 = 'ID14'

            section = controlled_df.query('activity == @activity and COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2')
            if activity == 'sixmwt':
                invalid_ID_3 = 'ID10'
                section = controlled_df.query('activity == @activity and COPD == @COPD and ID != @invalid_ID_1 '
                                          'and ID != @invalid_ID_2 and ID != @invalid_ID_3')


            gap_list = []
            next_list = []
            for i in range(len(section)):
                if section['rel1'].iloc[i] == 3.0:
                    gap_after_count = 0
                    j = i+1
                    while j < len(section) and np.isnan(section['rel1'].iloc[j]):
                        gap_after_count += 1
                        j+= 1

                    #print "gap_after_count is: ", gap_after_count
                    #print "next one is: ", section['rel1'].iloc[j]

                    gap_list.append(gap_after_count)
                    if j < len(section):
                        next_list.append(section['rel1'].iloc[j])
            print "activity is: ", activity
            print "COPD is: ", COPD
            print "gap list is:", gap_list
            print "gap list len is: ", len(gap_list)
            print "next list is: :", next_list

            length += len(gap_list)
            count_0 = 0
            count_1 = 0
            count_other = 0
            for item in gap_list:
                if item == 0:
                    count_0 += 1
                elif item == 1:
                    count_1 += 1
                else:
                    count_other +=1

            for item in next_list:
                if item == 2.0 or item == 3.0:
                    num_of_med_high_after += 1

            num_of_0 += count_0
            num_of_1 += count_1
            num_of_other += count_other

        print "total length is :", length
        print "num of 0 is :", num_of_0
        print "num of 1 is: ", num_of_1
        print "num of other is: ", num_of_other
        print "num of med high after: ", num_of_med_high_after
        """

        # ------------------------------------- End of Controlled --------------------------
        # ---------------------------------- Uncontrolled ----------------------------------

        invalid_ID_1 = 'ID11'
        invalid_ID_2 = 'ID14'

        w_length = 0
        w_num_of_0 = 0
        w_num_of_1 = 0
        w_num_of_other = 0

        w_num_of_med_high_after = 0

        wild_section = controlled_df.query('COPD == @COPD and ID != @invalid_ID_1 and ID != @invalid_ID_2')

        w_gap_list = []
        w_next_list = []
        for i in range(len(wild_section)):
            if wild_section['rel1'].iloc[i] == 3.0:
                gap_after_count = 0
                j = i + 1
                while j < len(wild_section) and np.isnan(wild_section['rel1'].iloc[j]):
                    gap_after_count += 1
                    j += 1

                # print "gap_after_count is: ", gap_after_count
                # print "next one is: ", section['rel1'].iloc[j]

                w_gap_list.append(gap_after_count)
                if j < len(wild_section):
                    w_next_list.append(wild_section['rel1'].iloc[j])
        print "in wild"
        print "COPD is: ", COPD
        print "gap list is:", w_gap_list
        print "gap list len is: ", len(w_gap_list)
        print "next list is: :", w_next_list

        w_length += len(w_gap_list)
        count_0 = 0
        count_1 = 0
        count_other = 0
        for item in w_gap_list:
            if item == 0:
                count_0 += 1
            elif item == 1:
                count_1 += 1
            else:
                count_other += 1

        for item in w_next_list:
            if item == 2.0 or item == 3.0:
                w_num_of_med_high_after += 1

        w_num_of_0 += count_0
        w_num_of_1 += count_1
        w_num_of_other += count_other

        print "total length is :", w_length
        print "num of 0 is :", w_num_of_0
        print "num of 1 is: ", w_num_of_1
        print "num of other is: ", w_num_of_other
        print "num of med high after: ", w_num_of_med_high_after

        # -------------------------------------- End of Uncontrolled -----------------------


def  check_HR_stability(dataframe):
    # I want to see how stable the heart rates are in COPD and non-COPD groups
    # but stability makes sense per participant
    # what if i just draw the BH data for each person?

    valid_ID_list = ['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID12','ID13', 'ID15', 'ID16' ]
    for ID in valid_ID_list:
        section = dataframe.query("ID == @ID")
        BH = section['BH']

        for i in range(len(BH)):
            if BH.iloc[i] <10:
                print section.iloc[i]

        plt.title(ID)
        plt.plot(BH)
        #plt.show()



def check_high_rel_frequency_per_activity(dataframe, rel):
    # I want to check on average, how often I get a reading with reliability = rel

    valid_ID_list = ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08','ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']

    for activity in activity_list:

        total_duration= 0
        total_reading_count = 0

        if activity == 'sixmwt':
            valid_ID_list = ['ID02', 'ID03', 'ID04' ,'ID05', 'ID06', 'ID07', 'ID08', 'ID09','ID12', 'ID13', 'ID15', 'ID16']

        for ID  in valid_ID_list:

            print "activity is: ", activity
            print "ID is: ", ID

            section = dataframe.query('activity==@activity and ID == @ID')

            start_ts = section['ts'].iloc[0]  # first time stamp in this participant, this activity
            end_ts = section['ts'].iloc[len(section) - 1] # last time stamp in this participant, this activity

            duration = int((end_ts-start_ts).total_seconds())

            high_rel_section = section.query('rel1 ==@rel') # now only get the readings with reliability == rel
            high_rel_count = len(high_rel_section)

            if high_rel_count > 0:

                total_reading_count += high_rel_count # total reading counts with reliability == 3 for this person, this activity
                total_duration += duration

                print "time sum is: ", duration
                print "length is: ", high_rel_count
                print "average is :", duration / high_rel_count

            else:

                print "time sum is: ", duration
                total_reading_count += 0
                total_duration+= duration

        print "\n"
        print "total duration is: ", total_duration
        print "total reading count is: ", total_reading_count
        print "total average is: ", total_duration / total_reading_count
        print "\n\n"


def check_high_rel_frequency_per_activity_with_imputation(dataframe, rel):
    valid_ID_list =['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16'] # ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']

    for activity in activity_list:

        total_duration = 0
        total_reading_count = 0

        if activity == 'sixmwt':
            valid_ID_list = ['ID09', 'ID12', 'ID13', 'ID15','ID16']#['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID12', 'ID13', 'ID15','ID16']

        for ID in valid_ID_list:

            print "activity is: ", activity
            print "ID is: ", ID

            section = dataframe.query('activity==@activity and ID == @ID')
            section_ = dataframe.query('activity==@activity and ID == @ID')

            cols = ['BH', 'watch1', 'rel1']
            section[cols] = section_[cols].ffill()  # fill the missing values with the value from the previous data point

            start_ts = section['ts'].iloc[0]  # first time stamp in this participant, this activity
            end_ts = section['ts'].iloc[len(section) - 1]  # last time stamp in this participant, this activity

            duration = int((end_ts - start_ts).total_seconds())

            high_rel_section = section.query('rel1 ==@rel')  # now only get the readings with reliability == rel
            high_rel_count = len(high_rel_section)

            if high_rel_count > 0:

                total_reading_count += high_rel_count  # total reading counts with reliability == 3 for this person, this activity
                total_duration += duration

                print "time sum is: ", duration
                print "length is: ", high_rel_count
                print "average is :", duration / high_rel_count

            else:

                print "time sum is: ", duration
                total_reading_count += 0
                total_duration += duration

        print "\n"
        print "total duration is: ", total_duration
        print "total reading count is: ", total_reading_count
        print "total average is: ", total_duration / total_reading_count
        print "\n\n"

def check_high_rel_frequency_per_activity_with_interpolation(dataframe, rel):

    valid_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16'] #['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']

    for activity in activity_list:

        total_duration = 0
        total_reading_count = 0

        if activity == 'sixmwt':
            valid_ID_list = ['ID09', 'ID12', 'ID13', 'ID15','ID16']#['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID12', 'ID13', 'ID15','ID16']

        for ID in valid_ID_list:

            print "activity is: ", activity
            print "ID is: ", ID

            section = dataframe.query('activity==@activity and ID == @ID')

            start_ts = section['ts'].iloc[0]  # first time stamp in this participant, this activity
            end_ts = section['ts'].iloc[len(section) - 1]  # last time stamp in this participant, this activity

            duration = int((end_ts - start_ts).total_seconds())

            rel = section['rel1']

            nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
            rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])


            high_rel_count = np.count_nonzero(rel == 3.0)

            if high_rel_count > 0:

                total_reading_count += high_rel_count  # total reading counts with reliability == 3 for this person, this activity
                total_duration += duration

                print "time sum is: ", duration
                print "length is: ", high_rel_count
                print "average is :", duration / high_rel_count

            else:

                print "time sum is: ", duration
                total_reading_count += 0
                total_duration += duration

        print "\n"
        print "total duration is: ", total_duration
        print "total reading count is: ", total_reading_count
        print "total average is: ", total_duration / total_reading_count
        print "\n\n"


def check_high_rel_frequency_wild(dataframe, rel):
    # I want to check on average, how often I get a reading with reliability = rel

    # for ID14 bioharness data is invalid  for ID11 watch data is not valid
    valid_ID_list = ['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID10','ID12','ID13', 'ID15', 'ID16' ]

    total_duration= 0
    total_reading_count = 0
    for ID in valid_ID_list:

        print "The ID is: ", ID

        section = dataframe.query('ID == @ID')

        high_rel_section = section.query('rel1 == @rel')

        start_ts = section['ts'].iloc[0]
        end_ts = section['ts'].iloc[len(section)-1]

        duration = int((end_ts - start_ts).total_seconds())
        high_rel_count = len(high_rel_section)
        average = duration / high_rel_count

        #print "average is: ", average

        total_duration += duration
        total_reading_count += high_rel_count


    print "average is: ", total_duration / total_reading_count



def check_high_rel_frequency_wild_with_imputation(dataframe, rel):

    print ("wild with imputation")
    # for ID14 bioharness data is invalid  for ID11 watch data is not valid
    valid_ID_list = ['ID09','ID10','ID12','ID13', 'ID15', 'ID16' ] #['ID02','ID03','ID04','ID05','ID06','ID07','ID08'] #,'ID09','ID10','ID12','ID13', 'ID15', 'ID16' ]

    total_duration = 0
    total_reading_count = 0
    for ID in valid_ID_list:
        print "The ID is: ", ID

        section = dataframe.query('ID == @ID')
        section_ = dataframe.query('ID == @ID')

        cols = ['BH', 'watch1', 'rel1']
        section[cols] = section_[cols].ffill()  # fill the missing values with the value from the previous data point


        high_rel_section = section.query('rel1 == @rel')


        start_ts = section['ts'].iloc[0]
        end_ts = section['ts'].iloc[len(section) - 1]

        duration = int((end_ts - start_ts).total_seconds())
        high_rel_count = len(high_rel_section)
        average = duration / high_rel_count

        # print "average is: ", average

        total_duration += duration
        total_reading_count += high_rel_count

    print "average is: ", total_duration / total_reading_count



def check_within_5_and_10_frequency_wild(dataframe,rel):

    valid_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15', 'ID16'] #['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15', 'ID16']

    total_duration = 0
    total_count_within_5 = 0
    total_count_within_10 = 0


    for ID in valid_ID_list:
        section = dataframe.query('ID == @ID')


        within_5_section = section.query('rel1 == @rel and within_5 == True')
        within_10_section = section.query('rel1 == @rel and within_10 == True')

        start_ts = section['ts'].iloc[0]
        end_ts = section['ts'].iloc[len(section) - 1]


        duration = int((end_ts - start_ts).total_seconds())

        within_5_count = len(within_5_section)
        within_10_count = len(within_10_section)

        total_duration += duration

        total_count_within_5 += within_5_count
        total_count_within_10 += within_10_count


    print "average of within 5 is: ", total_duration / total_count_within_5
    print "average of within 10 is: ", total_duration / total_count_within_10



def check_within_range_frequency_controlled(dataframe, rel):
    valid_ID_list =['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08','ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']

    for activity in activity_list:

        total_duration_5 = 0
        total_duration_10 = 0
        total_within_5_count = 0
        total_within_10_count = 0

        if activity == 'sixmwt':
            valid_ID_list = ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08','ID09', 'ID12', 'ID13', 'ID15','ID16']

        for ID in valid_ID_list:

            print "activity is: ", activity
            print "ID is: ", ID

            section = dataframe.query('activity==@activity and ID == @ID')

            start_ts = section['ts'].iloc[0]  # first time stamp in this participant, this activity
            end_ts = section['ts'].iloc[len(section) - 1]  # last time stamp in this participant, this activity

            duration = int((end_ts - start_ts).total_seconds())

            within_5_section = section.query('rel1 == @rel and within_5 == True')
            within_5_count = len(within_5_section)

            within_10_section = section.query('rel1 == @rel and within_10== True')
            within_10_count = len(within_10_section)


            if within_5_count > 0:

                total_within_5_count += within_5_count  # total reading counts with reliability == 3 for this person, this activity
                total_duration_5 += duration

                print "time duration is: ", duration
                print "length 5 is: ", within_5_count
                print "average is :", duration / within_5_count

            else:

                print "time duration is: ", duration
                total_within_5_count += 0
                total_duration_5 += duration

            # ------

            if within_10_count > 0:

                total_within_10_count += within_10_count  # total reading counts with reliability == 3 for this person, this activity
                total_duration_10 += duration

                print "time duration is: ", duration
                print "length 10 is: ", within_10_count
                print "average is :", duration / within_10_count

            else:

                print "time sum is: ", duration
                total_within_10_count += 0
                total_duration_10 += duration



        if total_within_5_count > 0:
            print "\n"
            print "total duration_5 is: ", total_duration_5
            print "total reading count is: ", total_within_5_count
            print "total average is: ", total_duration_5 / total_within_5_count


        if total_within_10_count > 0:
            print "total duration_10 is: ", total_duration_10
            print "total reading count is: ", total_within_10_count
            print "total average is: ", total_duration_10 / total_within_10_count


        print "\n\n"


def high_rel_count_all_data_together(controlled_df, wild_df):


    # --------------------------------- wild ------------------------
    wild_total_count = 0
    wild_total_high_rel_count = 0

    wild_ID_list = ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08', 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in wild_ID_list:
        wild_section = wild_df.query('ID == @ID')

        BH = wild_section['BH']
        high_rel = wild_section['rel1'] == 3.0
        new_section = wild_section[high_rel]

        #print "ID is: ", ID
        #print "BH count is: ", BH.shape[0]
        #print "high rel count is: ", new_section.shape[0]
        #print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100

        wild_total_count += BH.shape[0]
        wild_total_high_rel_count += new_section.shape[0]

    # ---------------------------------------------------------------

    #--------------------------------controlled ---------------------

    controlled_total_count = 0
    controlled_total_high_rel_count = 0

    ID_10_sixmwt_count = 0

    controlled_ID_list = ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08', 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in controlled_ID_list:
        controlled_section = controlled_df.query('ID == @ID')

        if ID == 'ID10':

            ID_10 = controlled_section['ID'] == 'ID10'
            sixmwt = controlled_section['activity'] == 'sixmwt'

            ID_10_sixmwt_count = len(controlled_section[ID_10 & sixmwt])

        BH = controlled_section['BH']
        high_rel = controlled_section['rel1'] == 3.0
        new_section = controlled_section[high_rel]

        # print "ID is: ", ID
        # print "BH count is: ", BH.shape[0]
        # print "high rel count is: ", new_section.shape[0]
        # print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100

        controlled_total_count += BH.shape[0]
        controlled_total_high_rel_count += new_section.shape[0]
    # -----------------------------------------------------------


    controlled_total_count -= ID_10_sixmwt_count

    print "ID10 6mwt count is: ", ID_10_sixmwt_count

    all_total = wild_total_count + controlled_total_count
    all_high_rel = wild_total_high_rel_count + controlled_total_high_rel_count


    print "total percentage is: ", (float(all_high_rel) / all_total) * 100


def high_rel_count_all_together_with_imputation(controlled_df, wild_df):
    # --------------------------------- wild ------------------------
    wild_total_count = 0
    wild_total_high_rel_count = 0

    wild_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']#['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in wild_ID_list:
        wild_section = wild_df.query('ID == @ID')
        wild_section_ = wild_df.query('ID == @ID')

        cols = ['BH', 'watch1', 'rel1']
        wild_section[cols] = wild_section_[cols].ffill()  # fill the missing values with the value from the previous data point


        BH = wild_section['BH']
        high_rel = wild_section['rel1'] == 3.0
        new_section = wild_section[high_rel]

        # print "ID is: ", ID
        # print "BH count is: ", BH.shape[0]
        # print "high rel count is: ", new_section.shape[0]
        # print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100

        wild_total_count += BH.shape[0]
        wild_total_high_rel_count += new_section.shape[0]

    # ---------------------------------------------------------------

    # --------------------------------controlled ---------------------

    controlled_total_count = 0
    controlled_total_high_rel_count = 0

    ID_10_sixmwt_count = 0

    controlled_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']#['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in controlled_ID_list:
        controlled_section = controlled_df.query('ID == @ID')
        controlled_section_ = controlled_df.query('ID == @ID')

        if ID == 'ID10':
            ID_10 = controlled_section['ID'] == 'ID10'
            sixmwt = controlled_section['activity'] == 'sixmwt'

            ID_10_sixmwt_count = len(controlled_section[ID_10 & sixmwt])



        cols = ['BH', 'watch1', 'rel1']
        controlled_section[cols] = controlled_section_[cols].ffill()  # fill the missing values with the value from the previous data point


        BH = controlled_section['BH']
        high_rel = controlled_section['rel1'] == 3.0
        new_section = controlled_section[high_rel]

        # print "ID is: ", ID
        # print "BH count is: ", BH.shape[0]
        # print "high rel count is: ", new_section.shape[0]
        # print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100

        controlled_total_count += BH.shape[0]
        controlled_total_high_rel_count += new_section.shape[0]
    # -----------------------------------------------------------

    controlled_total_count -= ID_10_sixmwt_count

    print "ID10 6mwt count is: ", ID_10_sixmwt_count

    all_total = wild_total_count + controlled_total_count
    all_high_rel = wild_total_high_rel_count + controlled_total_high_rel_count

    print "total percentage is: ", (float(all_high_rel) / all_total) * 100

def high_rel_count_all_together_with_interpolation(controlled_df, wild_df):


    # --------------------------------- wild ------------------------
    wild_total_count = 0
    wild_total_high_rel_count = 0

    wild_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']#['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in wild_ID_list:
        wild_section = wild_df.query('ID == @ID')

        BH = wild_section['BH']
        rel = wild_section['rel1']

        nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
        rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])

        high_rel_count = np.count_nonzero(rel == 3.0)

        #print "ID is: ", ID
        #print "BH count is: ", BH.shape[0]
        #print "high rel count is: ", new_section.shape[0]
        #print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100

        wild_total_count += BH.shape[0]
        wild_total_high_rel_count += high_rel_count

    # ---------------------------------------------------------------

    #--------------------------------controlled ---------------------

    controlled_total_count = 0
    controlled_total_high_rel_count = 0

    ID_10_sixmwt_count = 0

    controlled_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']#['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in controlled_ID_list:
        controlled_section = controlled_df.query('ID == @ID')

        if ID == 'ID10':

            ID_10 = controlled_section['ID'] == 'ID10'
            sixmwt = controlled_section['activity'] == 'sixmwt'

            ID_10_sixmwt_count = len(controlled_section[ID_10 & sixmwt])

        BH = controlled_section['BH']
        rel = controlled_section['rel1']

        nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
        rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])

        high_rel_count = np.count_nonzero(rel == 3.0)

        # print "ID is: ", ID
        # print "BH count is: ", BH.shape[0]
        # print "high rel count is: ", new_section.shape[0]
        # print "percentage is: ", (float(new_section.shape[0]) / BH.shape[0]) * 100

        controlled_total_count += BH.shape[0]
        controlled_total_high_rel_count += high_rel_count
    # -----------------------------------------------------------


    controlled_total_count -= ID_10_sixmwt_count

    print "ID10 6mwt count is: ", ID_10_sixmwt_count

    all_total = wild_total_count + controlled_total_count
    all_high_rel = wild_total_high_rel_count + controlled_total_high_rel_count


    print "total percentage is: ", (float(all_high_rel) / all_total) * 100



def check_high_rel_frequency_wild_with_interpolation(dataframe):

    # I have the whole chunk of wild
    # I want to perform interpolation for each ID separately

    total_duration = 0
    total_reading_count = 0


    wild_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16'] #['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08'] #, 'ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']
    for ID in wild_ID_list:

        print "ID is: ", ID

        wild_section = dataframe.query('ID == @ID')
        rel = wild_section['rel1']

        print "rel before interpolation: "
        print rel

        nans, x = np.isnan(rel), lambda z: z.nonzero()[0]
        rel[nans] = np.interp(x(nans), x(~nans), rel[~nans])

        print "rel after interpolation"
        print rel.round(2)


        section = dataframe.query('ID == @ID')
        start_ts = section['ts'].iloc[0]
        end_ts = section['ts'].iloc[len(section) - 1]
        duration = int((end_ts - start_ts).total_seconds())

        print "duration is:" , duration

        high_rel_count = np.count_nonzero(rel == 3.0)
        print "high rel count is: ", high_rel_count

        average = duration / high_rel_count

        #print "average is: ", average

        total_duration += duration
        total_reading_count += high_rel_count

    print "average is: ", total_duration / total_reading_count


def sampling_rate(df, is_COPD):
    print "is_COPD is: ", is_COPD

    for activity in activity_list:

        count = 0 # to count the number of readings by watch

        print "activity is: ", activity
        if is_COPD:
            if activity == 'sixmwt':
                valid_ID_list = ['ID09', 'ID12', 'ID13', 'ID15','ID16']

            else:
                valid_ID_list = ['ID09', 'ID10', 'ID12', 'ID13', 'ID15','ID16']

        else:
            valid_ID_list =  ['ID02', 'ID03', 'ID04', 'ID05', 'ID06', 'ID07', 'ID08']


        for ID in valid_ID_list:
            print "ID is: ", ID

            section = df .query('activity==@activity and ID == @ID')
            watch_not_null_condition = np.isnan(section['watch1']) == False
            section = section[watch_not_null_condition]

            watch = section['watch1']
            readings_count = len(watch)

            print "number of readings: ", readings_count

            count += readings_count


        average = float(count) / len(valid_ID_list)

        print "average is: ", average
        print "------"


def COPD_ages():

    age_list = [62, 81, 67, 69, 73, 63, 77]
    valid_sum = sum(age_list)
    print valid_sum

    print np.mean(age_list)
    print np.std(age_list)

def non_COPD_ages():

    age_list = [36, 32, 28, 22, 28, 24, 29]
    valid_sum = sum(age_list)
    print valid_sum

    print np.mean(age_list)
    print np.std(age_list)





if __name__ == '__main__':
    csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
    controlled_df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])

    wild_csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild.csv"
    wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])

    #check_high_rel_frequency_per_activity(controlled_df, 3)
    #check_high_rel_frequency_per_activity_with_imputation(controlled_df, 3)
    #check_high_rel_frequency_per_activity_with_interpolation(controlled_df, 3)

    #check_high_rel_frequency_wild(wild_df, 3)
    #check_high_rel_frequency_wild_with_imputation(wild_df, 3)

    #check_high_rel_frequency_wild_with_interpolation(wild_df)




    csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_controlled_with_acceptable_column.csv"
    controlled_df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])

    #check_within_range_frequency_controlled(controlled_df, 3)

    wild_csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild_with_acceptable_column.csv"
    wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])

    sampling_rate(controlled_df, True)
    print "//////"
    sampling_rate(controlled_df,False)

    rel_levels = [0.0, 1.0, 2.0, 3.0, 0.1]


    #for rel in rel_levels:

        #mean_abs_error_all_data_together_for_rel(controlled_df, wild_df, rel)

    #high_rel_count_all_data_together(controlled_df, wild_df)
    #high_rel_count_all_together_with_imputation(controlled_df, wild_df)
    #high_rel_count_all_together_with_interpolation(controlled_df, wild_df)

    #high_rel_count_wild(wild_df)
    #high_rel_count_wild_with_imputation(wild_df)
    #high_rel_count_wild_with_interpolation(wild_df)

    #check_within_5_and_10_frequency_wild(wild_df, 3)


    #check_HR_stability(controlled_df)

    #plot_rel_count_bar_graph_per_healthiness_across_activities(wild_df, True)

    #count_reliability_per_activity_per_healthiness(controlled_df)
    #check_before_after_high_rel(controlled_df, wild_df)
    #mean_abs_error_for_rel(df, 3.0)

    #non_COPD_ages()


    #count_reliability_per_person_per_activity(controlled_df)
    #BH_vs_watch_reliable_data(df, 2)

    #count_reliability_per_person_per_activity_with_window(df, 60)

    #count_watch_reports_per_activity_per_healthiness(controlled_df)

    #gap_length_count_version_2_per_healthiness(controlled_df)



    #count_reliability_per_person_per_activity(df)

    #count_reliability_per_activity_per_healthiness(df)
    #plot_count_reliability_bar_graph_high_med_low_compare()
    #plot_count_reliability_bar_graph_COPD_nonCOP_compare()

    """
    wild_csv_path  = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild.csv"
    wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])
    high_rel_count_wild(wild_df)
    """




