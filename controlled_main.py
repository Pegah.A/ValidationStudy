import activity_recorder_logs
import Bioharness
import watch
import phone
import general_device
import helper_functions


system_path = "/home/pegah/ValidationStudy/participant_data/"
this_participant_path= "Mar22_ID08_controlled/"
app_logs_file= "trial-14.json"
BH_file_name = "log_2018_03_22-15_47_53_Summary.csv"
BH_summary_file = "shifted_times_log.csv"

NEW_TIME = True  # is this experiment after March 11, 2018?  This is due to time change and difference between GMT and EST

# activities associated with the first watch
activities = ["sixmwt", "sitting" , "walking" , "lying" , "standing", "eating" , "brushing", "climbup", "climbdown"]

# activities associated with the second watch
activities2 = ["eating" , "brushing", "climbup", "climbdown"]


data = ["hr", "accel", "gyro"]


if __name__ == '__main__':

    # reads the json file of the activity logging app and converts the content into a csv file
    activity_logs_csv_path = activity_recorder_logs.read_logs_from_app(system_path, this_participant_path , app_logs_file, NEW_TIME)

    # Bioharness tends to be 10 seconds faster than the watch. This function adjusts the timestamps
    Bioharness.bh_change_timestamps(system_path, this_participant_path, BH_file_name)

    BH_csv_path = system_path + this_participant_path + "Bioharness/" + BH_summary_file

    #Getting the row numbers for each of the activities within the main Bioharness data file.
    BH_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, BH_csv_path, "Bioharness")

    #Based on the row divisions, for each activity a separate csv file is created
    Bioharness.bh_activity_csv_creator(system_path , this_participant_path ,BH_row_division, BH_csv_path)

    watch_list =["watch1", "watch2"]

    for watch_ in watch_list:

        # For each type of data (hr, accel, gyro) it will read all the .json files and turn them into csv files.
        # all_hr_data is at index 0, all_accel_data is at index 1 and all_gyro_data is at index 2
        # all_watch_data[0] is a path to the file that has the all_hr_data which has gaps within itself. It is not imputated.
        all_watch_data = watch.watch_csv_creator(system_path, this_participant_path, watch_, NEW_TIME)


        # --------------without imputating (without filling the rows)
        all_hr_data_complete_path = all_watch_data[0]
        # Getting the row numbers for each of the activities within the heart rate data file
        watch_hr_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, all_hr_data_complete_path, watch_)
        #Creating separate csv files for heart rate for each activity
        general_device.device_activity_csv_creator(system_path, this_participant_path, watch_hr_row_division, all_hr_data_complete_path, watch_, "hr", False)
        # Bioharness data file has data point for every second. We want to pick only the ones that have the match from the watch as well.
        Bioharness.bh_activity_csv_creator_pick_values_based_on_watch_data(system_path, this_participant_path, watch_)
        # -----------------------------------------------------------

        # --------------with imputating (with filling the rows)
        all_hr_data_complete_path = helper_functions.fill_missing_rows_in_file(all_watch_data[0])
        watch_hr_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, all_hr_data_complete_path , watch_)
        general_device.device_activity_csv_creator(system_path, this_participant_path, watch_hr_row_division, all_hr_data_complete_path,watch_ , "hr", True)
        # -----------------------------------------------------


        # Getting the row numbers for each of the activities within the accel data file
        watch_accel_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, all_watch_data[1] , watch_)
        # Creating separate csv files for accel for each activity
        general_device.device_activity_csv_creator(system_path, this_participant_path,watch_accel_row_division, all_watch_data[1], watch_, "accel")

        #Getting the row numbers for each of the activities within the gyroscope data file
        watch_gyro_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, all_watch_data[2], watch_)
        # Creating separate csv files for gyro for each activity
        general_device.device_activity_csv_creator(system_path, this_participant_path,watch_gyro_row_division, all_watch_data[2], watch_, "gyro")


    # For each type of data (accel, gyro) it will read all the .json files and turn them into csv files.
    # all_accel_data is at index 0 and all_gyro_data is at index 1
    all_phone_data = phone.phone_csv_creator(system_path, this_participant_path, NEW_TIME)

    # Getting the row numbers for each of the activities within the accel data file
    phone_accel_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, all_phone_data[0] , "phone")
    # Creating separate csv files for accel for each activity
    general_device.device_activity_csv_creator(system_path, this_participant_path,phone_accel_row_division, all_phone_data[0], "phone", "accel")

    # Getting the row numbers for each of the activities within the gyro data file
    phone_gyro_row_division = general_device.find_log_chunks_from_device(activity_logs_csv_path, all_phone_data[1] , "phone")
    # Creating separate csv files for gyro for each activity
    general_device.device_activity_csv_creator(system_path, this_participant_path,phone_gyro_row_division, all_phone_data[1], "phone", "gyro")

