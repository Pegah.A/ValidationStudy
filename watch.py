import os
import pandas as pd
from CopdFeatureExtraction.helpers.SensorHelper import *
import general_device

def watch_csv_creator(system_path, this_participant_path, watch, new_time):
    # First, read all .json files.
    # Take the hr data and put it into a csv file. Take the accel data and put it into a csv file. Take the gyro data and put it into a csv file
    # Second, combine all of the hr data files into one csv and combine all of the accel data files into one csv and combine all of th e gyro data file into one csv

    list_of_files =[]
    for file in os.listdir(system_path+this_participant_path+watch):
        if file.endswith(".json"):
            list_of_files.append(file)

    for item in list_of_files:
        file_name= str(item).split(".")[0]
        file_path = system_path + this_participant_path + watch +"/" + file_name +".json"
        hr_file_path = system_path+this_participant_path + watch +"/" + watch +"_csv_files/" +file_name +"_hr.csv"
        accel_file_path= system_path+this_participant_path + watch + "/" + watch +"_csv_files/" + file_name +"_accel.csv"
        gyro_file_path= system_path+this_participant_path + watch +"/" + watch +"_csv_files/" + file_name +"_gyro.csv"

        data = SensorJson(file_path)
        hr = data.get("hr")
        accel = data.get("accel")
        gyro = data.get("gyro")

        if new_time:
            hr.index =hr.index -  pd.Timedelta('04:00:00')
            accel.index =accel.index - pd.Timedelta('04:00:00')
            gyro.index= gyro.index - pd.Timedelta('04:00:00')
        else:
            hr.index = hr.index - pd.Timedelta('05:00:00')
            accel.index = accel.index - pd.Timedelta('05:00:00')
            gyro.index = gyro.index - pd.Timedelta('05:00:00')

        hr.to_csv(hr_file_path)
        accel.to_csv(accel_file_path)
        gyro.to_csv(gyro_file_path)

    all_hr_path = system_path + this_participant_path + watch + "/" + watch +"_csv_files" +"/all_hr_data.csv"
    all_accel_path = system_path + this_participant_path+ watch + "/" + watch +"_csv_files" +"/all_accel_data.csv"
    all_gyro_path = system_path + this_participant_path + watch + "/" + watch +"_csv_files" +"/all_gyro_data.csv"


    hr_files_list =[]
    accel_files_list = []
    gyro_files_list =[]

    for file in os.listdir(system_path + this_participant_path + watch + "/" + watch +"_csv_files" ):

        if file.endswith("hr.csv"):
            hr_files_list.append(file)
        elif file.endswith("accel.csv"):
            accel_files_list.append(file)
        elif file.endswith("gyro.csv"):
            gyro_files_list.append(file)

    hr_files_list.sort()
    accel_files_list.sort()
    gyro_files_list.sort()

    for i in range (len(hr_files_list)):
        hr_files_list[i]=open(system_path + this_participant_path + watch + "/" + watch +"_csv_files/" + hr_files_list[i], "r")

    for i in range (len(accel_files_list)):
        accel_files_list[i]=open(system_path + this_participant_path + watch + "/" + watch +"_csv_files/" +accel_files_list[i], "r")

    for i in range (len(gyro_files_list)):
        gyro_files_list[i]=open(system_path + this_participant_path + watch + "/" + watch +"_csv_files/" +gyro_files_list[i], "r")

    combined_csv_hr = pd.concat([pd.read_csv(f) for f in hr_files_list])
    combined_csv_hr.to_csv(all_hr_path , index= False)

    combined_csv_accel = pd.concat([pd.read_csv(f) for f in accel_files_list])
    combined_csv_accel.to_csv(all_accel_path, header=['ts', 'x', 'y', 'z' ], index=False)

    combined_csv_gyro = pd.concat([pd.read_csv(f) for f in gyro_files_list])
    combined_csv_gyro.to_csv(all_gyro_path, header=['ts', 'x', 'y', 'z' ], index=False)

    csv_data = pd.read_csv(all_hr_path)
    titles_list = csv_data.columns.tolist()
    general_device.remove_duplicate_timestamps_and_time_ranges(all_hr_path, titles_list[0])

    return all_hr_path, all_accel_path, all_gyro_path



