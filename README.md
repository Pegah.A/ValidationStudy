



Each folder's name is the date of the study, the participant's ID, and the study setting (controlled or in the wild)
1.controlled: Each folder has 4 sub-folders, a json file which is the time logs of the activities and a csv file which is the time logs in a csv format.
    Sub-folder are: Bioharness, phone, watch1, watch2
    1.1.Bioharness: This folder is for all the data from the Bioharness for this participant 
    It has a sub=folder: Bioharness_csv_files, 9 csv files that have names starting with log_ which are all the files that we get from the Biohanress and shifted_times_hr_log.csv   
    The one that is important to us here is the Summary.csv.
    Bioharness time is about 10 seconds faster. shifted_times_hr_log has the adjusted timestamps and heart rate of the data collected by the Biohanress.
        1.1.1. Bioharness_csv_files folder is a file that contains the Bioharness data for each activity. (sixmwt, sitting, walking, lying, standing, eating, brushing, climbup, climbdown).
        The picked_values_based_on_watch1: For each activity, we have a csv of those timestamps that also appear in the watch1 data files. ( = the 'not imputated' version)
        The picked_values_based_on_watch2: For each activity, we have a csv of those timestamps that also appear in the watch2 data files. ( = the 'not imputated' version
        
        
        
        
        
        
    1.2: Phone:
    
    
    
    1.3: watch1:
    
    
    
    1.4; watch2:
    
    
    
----------------------------------------------------------------------
Issue: Watch doesn't have an exact timestamp for the starting time

Let's say we have a starting time and an end time. We want the data points for this chunk of time. Example: the "in the wild" part of the study.
It is not a probem in the case of the Bioharness since th Bioharness has data stamps of every second. We surely haeva a datapoint at the starting time and a datapoint at the end time.
But for the heart rate data from the watch, that is not the case. The watch does not report a heart rate value every second. Even if we are imputating the data, the watch may not have a value for the exact start time. However, if we want to have the same number of data points for this chunk of time from both devices, we have to fix this issue.
Example: start time is 13:00:00   end time is: 13:10:00    watch's first time stamp is 13:00:05  so its imputated datastamps will be 13:00:05, 13:00:06, .... 13:10:00
It is missing the first 5 timestamps (13:00:00, 13:00:01 , ... , 13:00:04)

Solution:
I create the chunk for Bioharness. Then I create the chunk for the watch and perform imputation on it. Now I compare the two files. As long as I'm encountering a timestamp in the BH file that does not appeat in the watch file, I remove its row from the BH file.

Keep in mind:
This is not an issue for the "controlled" part of the study. In the "controlled" part, the first event that we use as a starting time to perform the imputation is "Experiment-start". Therefore, we definitely have the timestamp corresponding to the first activity which is the "6 Minute Walk Test". (Assuming of course that "Experiment start" has been logged long enough before the "6MWT". Which it should be by default. "Experiment start" is logged when the particiapnt is first met and is putting on the devices.)

----------------------------------------------------------------------
Issue: Day-Month switched in pandas to_datetime function

For some reason, when I'm creating the "shifted_logs" version of the BH file to adjust the timestamps, it messes up the data. In some cases (when both day and month are less than 12) it switches the plaement of the day and the month. And becauase every other Bioharness file is created using this "shifted_logs" file, they all get messed up.
I am using pandas.to_datetime()
pandas default format for date is : YY-MM-DD  This is not what the Biohanrness has. I can't just swap the day and month values because datetime.datetime and datetime.date objects are immutable.

Whaty if I change the str?
This was a HORRIBLE idea. Sooo soooo slow. It had to read each row and the timestamp which was a string. Then check to see if the day and month need to be changed. Then create a new string with the correct format and replace it with the old one. For all the rows in the Bioharness csv file. very VERY slow.

Solution:
pd.to_datetime() has a default value dayfirst which is False by default. 

dayfirst : boolean, default False
    Specify a date parse order if arg is str or its list-likes. If True, parses dates with the day first, eg 10/11/12 is parsed as 2012-11-10
yearfirst : boolean, default False
    Specify a date parse order if arg is str or its list-likes.
    If True parses dates with the year first, eg 10/11/12 is parsed as 2010-11-12.
    If both dayfirst and yearfirst are True, yearfirst is preceded (same as dateutil)

The string I was getting from the BH was for example: 10/04/2018 for April 10th. That means it was in a day firt format. Therefore, all I had to do to fix this bug was to set dayfirst to True.

-----------------------------------------------------------------------

------- By May 7th -----------------
I have not completed the code for phone data and accel-gyro data of the watch for the "in the wild" part.
----------------------------------------=



