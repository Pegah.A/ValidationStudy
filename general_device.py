import pandas as pd
import csv

from datetime import datetime, date
activities = ["sixmwt", "sitting" , "walking" , "lying" , "standing", "eating" , "brushing", "climbup", "climbdown"]


def find_log_chunks_from_device(activity_logs_csv_path, device_csv_path, device):
    # We have a lot of data files from the device. We want to know which row numbers are the range for each activity.
    # We know the timestamp of each activity from the csv of the activity logging app.

    print " in find chunks"
    print "path is: ", device_csv_path
    logs_from_app = pd.read_csv(activity_logs_csv_path)
    app_timestamps = logs_from_app['timestamp']
    app_activity = logs_from_app['activity']

    if device == "Bioharness":
        app_timestamps = app_timestamps.str.replace("-", "/")

    device_content = pd.read_csv(device_csv_path)
    device_timestamps = device_content.iloc[:,0]
    device_timestamps = pd.to_datetime(device_timestamps)
    device_times = device_timestamps.dt.time

    app_timestamps = pd.to_datetime(app_timestamps)
    app_times= app_timestamps.dt.time

    app_times_no_ms = app_times.apply(lambda x: x.replace(microsecond=0))
    device_times_no_ms= device_times.apply(lambda x: x.replace(microsecond=0))

    device_row_division = []
    activity_num_counter = 0

    for app_time in app_times_no_ms:
        print "app time is: " , app_time
        found_exact = False
        for row_num in range(device_times_no_ms.size):
            if app_time == device_times_no_ms[row_num]:
                print "in found exact. the device time is: ", device_times_no_ms[row_num]
                device_row_division.append (row_num + 1)
                found_exact = True
                break

        # for this time in app log, we have checked all the file and there is not exact timestamp entry
        if found_exact == False:
            print "in not exact"
            min = 60.0
            min_row_num = 0
            for row_num in range(device_times_no_ms.size):
                if device_times_no_ms[row_num] > app_time:
                    min_row_num = row_num
                    #subtraction = datetime.combine(date.today(), device_times_no_ms[row_num]) - datetime.combine(date.today(), app_time)
                    #subtraction = abs(subtraction.total_seconds())
                    #if subtraction < min:
                        #min = subtraction
                        #min_row_num = row_num
                    break

            print "the not exact time is: ", device_times_no_ms[min_row_num]
            print "min row num is: ", min_row_num
            device_row_division.append(min_row_num + 1)

    print device_row_division
    return device_row_division


def device_activity_csv_creator(system_path, this_participant_path, device_row_division, device_csv_path, device, data, fill=None):

    all_files  =[]
    for activity in activities:
        if fill == True:
            path_=system_path + this_participant_path + device + "/" +device+ "_"+data + "_activity_sep_files/with_filling/" + activity + "_" + data +".csv"
        elif fill == False:
            path_ = system_path + this_participant_path + device + "/" + device + "_" + data + "_activity_sep_files/without_filling/" + activity + "_" + data + ".csv"
        elif fill == None:
            path_ = system_path + this_participant_path + device + "/" + device + "_" + data + "_activity_sep_files/" + activity + ".csv"

        #all_files.append(open(path_),"w")
        file_ = open(path_, "w")
        all_files.append(file_)

    for item in all_files:
        item.truncate()

    start_index = 1  # starting from 6MWT and not "experiment start"
    end_index = 2
    file_counter = 0
    titles = []
    device_csv_file = open(device_csv_path, "r")
    if data == "hr":
        titles = ["ts" , "hr" , "r"]
    else:
        titles = ["ts" , "x" , "y" , "z"]

    while (file_counter < 9):
        # for i in range(row_chunk_nums[start_index],row_chunk_nums[end_index]):
        reader = csv.reader(device_csv_file)
        device_csv_file.seek(
            0)  # to reset the file iterator, otherwise it will iterate through the BH file only once and will fill only the first file.
        writer = csv.writer(all_files[file_counter])
        writer.writerow(titles)
        for counter, row in enumerate(reader):
            if counter < device_row_division[start_index]: continue
            if counter >= device_row_division[end_index]: break
            writer.writerow(row)

        start_index += 2  # because we have to skip the end log of the current activity to get to the start log of the next one
        end_index += 2
        file_counter += 1


def device_activity_csv_creator_not_exact(system_path, this_participant_path, device_row_division, device_csv_path, device, data):
    all_files = []
    for activity in activities:
        path_ = system_path + this_participant_path + device + "/" + device + "_" + data + "_activity_sep_files/" + activity + "_" + data + ".csv"
        # all_files.append(open(path_),"w")
        file_ = open(path_, "w")
        all_files.append(file_)

    for item in all_files:
        item.truncate()

    start_index = 1  # starting from 6MWT and not "experiment start"
    end_index = 2
    file_counter = 0
    titles = []
    device_csv_file = open(device_csv_path, "r")
    if data == "hr":
        titles = ["ts", "hr", "r"]
    else:
        titles = ["ts", "x", "y", "z"]

    while (file_counter < 9):
        # for i in range(row_chunk_nums[start_index],row_chunk_nums[end_index]):
        reader = csv.reader(device_csv_file)
        device_csv_file.seek(
            0)  # to reset the file iterator, otherwise it will iterate through the BH file only once and will fill only the first file.
        writer = csv.writer(all_files[file_counter])
        writer.writerow(titles)
        for counter, row in enumerate(reader):
            if counter < device_row_division[start_index]: continue
            if counter >= device_row_division[end_index]: break
            writer.writerow(row)

        start_index += 2  # because we have to skip the end log of the current activity to get to the start log of the next one
        end_index += 2
        file_counter += 1


def combine_csv_files(csv_1_path, csv_2_path,combined_path):
    csv_files=[0,0]
    csv_files[0] = open(csv_1_path, "r")
    csv_files[1] = open(csv_2_path, "r")

    combined_csv = pd.concat([pd.read_csv(f) for f in csv_files])
    combined_csv.to_csv(combined_path, index=False)



def remove_duplicate_timestamps_and_time_ranges(csv_file_path, timestamp_column_name): #, timestamp_column_name):
    # Replace the timestamps with new timestamps that do not have the millisecond section.
    # Then use pd.drop_duplicates() to get rid of the duplicates

    df = pd.read_csv(csv_file_path)

    df[timestamp_column_name]= pd.to_datetime(df[timestamp_column_name]).apply(lambda x: x.replace(microsecond=0))
    df = df.drop_duplicates(timestamp_column_name, keep= 'first')
    df.to_csv(csv_file_path, index=False)
    df = pd.read_csv(csv_file_path)


    # So far, the exact duplicates have been removed.
    # Now I want to check the increasing order of the timestamps.

    current_index = 0
    next_index = 1
    current_ts = df[timestamp_column_name][current_index]
    next_ts = df[timestamp_column_name][next_index]

    rows_to_drop_list = []
    # BH_wild_df = BH_wild_df.drop(BH_time.index[index_list])    it gets the list of the row indices that it needs to drop

    for i in range(df.shape[0]):
        while next_ts > current_ts:
            current_index += 1
            next_index += 1
            if current_index == df.shape[0] or next_index == df.shape[0]:
                break

            current_ts = df[timestamp_column_name][current_index]
            next_ts = df[timestamp_column_name][next_index]


        # here means next is no longer greater than current
        while next_ts < current_ts:
            rows_to_drop_list.append(next_index)
            next_index += 1
            next_ts = df[timestamp_column_name][next_index]

        current_index = next_index
        next_index += 1

        if current_index == df.shape[0] or next_index == df.shape[0]:
            break

    df = df.drop(df[timestamp_column_name].index[rows_to_drop_list])
    df.to_csv(csv_file_path,index = False )



def check_file_length_match(BH_wild_path, BH_wild_picked_path, watch_with_filling_path, watch_without_filling_path):

    # After wild_main is executed the length of Bioharness/in_the_wild.csv should be the same as watch1/watch1_in_the_wild_files/in_the_wild_hr_with_filling.csv
    # and the length of Bioharness/in_the_wild_picked_values.csv should be the same as watch1/watch1_in_the_wild_files/in_the_wild_hr_without_filling.csv

    BH_wild_df = pd.read_csv(BH_wild_path)
    BH_wild_picked_df = pd.read_csv(BH_wild_picked_path)
    watch1_with_filling_df = pd.read_csv(watch_with_filling_path)
    watch1_without_filling_df = pd.read_csv(watch_without_filling_path)

    if BH_wild_df.shape[0] == watch1_with_filling_df.shape[0] and BH_wild_picked_df.shape[0] == watch1_without_filling_df.shape[0]:
        print "lengths match :)"
    else:
        print "Something's wrong :("




