import pandas as pd
import csv

activities= ["sixmwt", "sitting" , "walking" ,"lying", "standing", "eating", "brushing", "climbup", "climbdown"]
activities_watch2 = ["brushing", "eating", "climbup", "climbdown"]
participants_list = ["Feb20_ID02", "Feb22_ID03", "Mar1_ID04" , "Mar14_ID05", "Mar16_ID06" , "Mar21_ID07",
                    "Mar22_ID08", "Apr10_ID09","Apr13_ID10","Apr19_ID11","Apr24_ID12", "Apr27_ID13", "May22_ID14",
                    "May22_ID15", "Jun11_ID16" ]



activity_time_dict = {"sixmwt": 360, "sitting": 240, "walking": 240, "lying": 240, "standing": 240,
                      "eating": 180, "brushing": 120, "climbup": "NA", "climbdown": "NA"}



system_path = "/Users/pegah/Documents/VS/participant_data_updated/"

# "/home/pegah/ValidationStudy/participant_data/"
#this_participant_path= "Feb20_ID02_controlled/"

def create_general_csv(imputing):
    # i want to creat a big csv file from a pandas dataframe.
    # The columns are index, time, participant ID , COPD, activity, BH, watch1, watch2, rel, imputated data

    main_df = pd.DataFrame(
        columns=['ts', 'ID', 'COPD', 'activity', 'BH', 'watch1', 'rel1', 'watch2', 'rel2', 'imputed'])

    if imputing:
        main_df_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_data_imputing.csv"
    else:
        main_df_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_without_data_imputing.csv"

    # now i want to fill this data frame. let's first consider for one participant. ID2


    # FIRST VERSION: Without imputing data. Work with only what I directly have from the watch
    # So for participant 2, I go to watch1_hr_activity_sep_files and open each file in a pandas dataframe
    # then I'll try to fill the general dataframe

    general_dataframes_list = []
    for participant in participants_list:
        for activity in activities:
            general_df = pd.DataFrame(
                columns=['ts', 'ID', 'COPD', 'activity', 'BH', 'watch1', 'watch2', 'rel1', 'rel2', 'imputed'])

            if imputing == True:
                watch1_path_controlled = system_path + participant + "_controlled/"+ "watch1/watch1_hr_activity_sep_files/with_filling/" + activity + "_hr.csv"
                BH_path_controlled = system_path + participant + "_controlled/"+ "Bioharness/Bioharness_csv_files/" + activity + ".csv"

            elif imputing == False:
                watch1_path_controlled = system_path + participant + "_controlled/" + "watch1/watch1_hr_activity_sep_files/without_filling/" + activity + "_hr.csv"
                BH_path_controlled = system_path + participant + "_controlled/" + "Bioharness/Bioharness_csv_files/picked_values_based_on_watch1/" + activity + "_picked_values.csv"

            watch1_df_controlled = pd.read_csv(watch1_path_controlled)
            BH_df_controlled = pd.read_csv(BH_path_controlled)

            num_of_watch1_rows= watch1_df_controlled.shape[0]
            num_of_BH_rows = BH_df_controlled.shape[0]

            general_df['ts'] = watch1_df_controlled['ts']
            general_df['ID'].loc[:num_of_watch1_rows] = participant.split("_")[1]

            ID = int(participant.split("_")[1][2:4])
            if ID < 9 :
                general_df['COPD'].loc[:num_of_watch1_rows] = False
            else:
                general_df['COPD'].loc[:num_of_watch1_rows] = True
            general_df['activity'].loc[:num_of_watch1_rows] = activity
            general_df['watch1'] = watch1_df_controlled['hr']
            general_df['watch2'].loc[:num_of_watch1_rows]= "NA"
            general_df['rel1']= watch1_df_controlled['r']
            general_df['rel2'].loc[:num_of_watch1_rows] = "NA"
            general_df['imputed'].loc[:num_of_watch1_rows]= imputing

            general_df['BH'] = BH_df_controlled['HR']

            if imputing == True and activity in activities_watch2 and participant != "Apr13_ID10":
                watch2_path = system_path + participant + "_controlled/"+ "watch2/watch2_hr_activity_sep_files/with_filling/" + activity + "_hr.csv"
                watch2_df = pd.read_csv(watch2_path)
                general_df['watch2'] = watch2_df['hr']
                general_df['rel2'] = watch2_df['r']

            general_dataframes_list.append(general_df)

        # Here is outside the loop iterating through the activities
        # Should add the wild part here

        general_df = pd.DataFrame(
            columns=['ts', 'ID', 'COPD', 'activity', 'BH', 'watch1', 'watch2', 'rel1', 'rel2', 'imputed'])
        if imputing == True:
            watch1_path_wild = system_path + participant + "_wild/"+ "watch1/watch1_in_the_wild_files/" + "in_the_wild_hr_with_filling.csv"
            BH_path_wild = system_path + participant + "_wild/"+ "Bioharness/"+ "in_the_wild.csv"
        elif imputing == False:
            watch1_path_wild = system_path + participant + "_wild/" + "watch1/watch1_in_the_wild_files/" + "in_the_wild_hr_without_filling.csv"
            BH_path_wild = system_path + participant + "_wild/" + "Bioharness/"+ "in_the_wild_picked_values.csv"

        watch1_df_wild = pd.read_csv(watch1_path_wild)
        BH_df_wild = pd.read_csv(BH_path_wild)

        num_of_watch1_rows = watch1_df_wild.shape[0]

        general_df['ts'] = watch1_df_wild['ts']
        general_df['ID'].loc[:num_of_watch1_rows] = participant.split("_")[1]

        ID = int(participant.split("_")[1][2:4])
        if ID < 9:
            general_df['COPD'].loc[:num_of_watch1_rows] = False
        else:
            general_df['COPD'].loc[:num_of_watch1_rows] = True

        general_df['activity'].loc[:num_of_watch1_rows] = "Unknown"
        general_df['watch1'] = watch1_df_wild['hr']
        general_df['watch2'].loc[:num_of_watch1_rows] = "NA"
        general_df['rel1'] = watch1_df_wild['r']
        general_df['rel2'].loc[:num_of_watch1_rows] = "NA"
        general_df['imputed'].loc[:num_of_watch1_rows] = imputing

        general_df['BH'] = BH_df_wild['HR']

        general_dataframes_list.append(general_df)


    main_df = pd.concat(general_dataframes_list)
    main_df = main_df.reset_index(drop=True) # so indices don't start from zero after every concat
    main_df.to_csv(main_df_path)

def add_to_general_csv(imputating, participant_file_name, study_environment_type):
    # I dont want tot have to create it from scrath everytime I have the data for a new participant

    participant_file_name = participant_file_name + "_" + study_environment_type

    if imputating:
        general_csv_so_far_path = system_path + "/main_csv_files/main_csv_file_with_data_imputating.csv"
    else:
        general_csv_so_far_path = system_path + "/main_csv_files/main_csv_file_without_data_imputating.csv"

    if study_environment_type == "c":  # c mean controlled
        add_controlled_data_to_general_csv(participant_file_name, general_csv_so_far_path)

    elif study_environment_type == "w":  # w means in the wild
        add_wild_data_to_general_csv(participant_file_name, general_csv_so_far_path)


def add_controlled_data_to_general_csv(participant_path, general_csv_path):

    general_csv_df = pd.read_csv(general_csv_path)
    general_df = pd.DataFrame(
        columns=['ts', 'ID', 'COPD', 'activity', 'BH', 'watch1', 'watch2', 'rel1', 'rel2', 'imputated'])


def add_wild_data_to_general_csv(participant_path, general_csv_path):
    general_csv_df = pd.read_csv(general_csv_path)
    general_df = pd.DataFrame(
        columns=['ts', 'ID', 'COPD', 'activity', 'BH', 'watch1', 'watch2', 'rel1', 'rel2', 'imputated'])


def create_general_csv_all_data():
    # I want to read from the BH main files which are at:  participant/Bioharness/Bioharness_csv_files/acitivity.csv
    # Based on these, I will complete the ts column and the BH column
    # then I will go through the without imputing files for watch1 and watch2 and fill the columns for watch1, rel1, watch2, rel2
    # I have to mathe the timestamps. THINK ABOUT THIS


    main_df_path =  "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
    general_dataframes_list = []

    for participant in participants_list:
        for activity in activities:

            print "participant is: ", participant
            print "activity is:", activity
            general_df = pd.DataFrame(
                columns=['ts', 'ID', 'COPD', 'activity', 'BH', 'watch1', 'rel1', 'watch2', 'rel2', 'imputed', 'accel_mag_1', 'gyro_mag_1'])

            if participant != "Apr19_ID11":
                watch1_path_controlled = system_path + participant + "_controlled/" + "watch1/watch1_hr_activity_sep_files/without_filling/" + activity + "_hr.csv"
            BH_path_controlled = system_path + participant + "_controlled/" + "Bioharness/Bioharness_csv_files/" + activity + ".csv"

            if participant != "Apr19_ID11":
                watch1_accel_controlled_path = system_path + participant + "_controlled/" + "watch1/watch1_accel_activity_sep_files/compressed/" + activity + ".csv"
                watch1_gyro_controlled_path = system_path + participant + "_controlled/" + "watch1/watch1_gyro_activity_sep_files/compressed/" + activity + ".csv"

            if participant != "Apr19_ID11":
                watch1_df_controlled = pd.read_csv(watch1_path_controlled)
            BH_df_controlled = pd.read_csv(BH_path_controlled)

            if participant != "Apr19_ID11":
                watch1_accel_df_controlled = pd.read_csv(watch1_accel_controlled_path)
                watch1_gyro_df_controlled = pd.read_csv(watch1_gyro_controlled_path)

            num_of_watch1_rows = watch1_df_controlled.shape[0]
            num_of_BH_rows = BH_df_controlled.shape[0]

            #------------------------------------------
            BH_timestamps = BH_df_controlled['Time']
            BH_timestamps = pd.to_datetime(BH_timestamps, dayfirst = True)
            BH_timestamps = BH_timestamps.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
            general_df['ts'] = BH_timestamps
            #-----------------------------------------


            general_df['ID'].loc[:num_of_BH_rows] = participant.split("_")[1]

            ID = int(participant.split("_")[1][2:4])
            if ID < 9:
                general_df['COPD'].loc[:num_of_BH_rows] = False
            else:
                general_df['COPD'].loc[:num_of_BH_rows] = True

            general_df['activity'].loc[:num_of_BH_rows] = activity

            #--------------------------------------------------------

            general_times = general_df['ts'].dt.time
            if participant != "Apr19_ID11":
                index_in_general_df = 0

                ts_watch1 = watch1_df_controlled['ts']
                ts_watch1 = pd.to_datetime(ts_watch1, dayfirst = True)
                ts_watch1 = ts_watch1.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
                ts_watch1 = ts_watch1.dt.time

                #general_times = general_df['ts'].dt.time
                for i in range(num_of_watch1_rows):
                    ts = ts_watch1[i]
                    while (general_times.iloc[index_in_general_df] != ts):
                        index_in_general_df += 1


                    general_df['watch1'].iloc[index_in_general_df] = watch1_df_controlled['hr'].iloc[i]
                    general_df['rel1'].iloc[index_in_general_df] = watch1_df_controlled['r'].iloc[i]
                    index_in_general_df += 1
            #--------------------------------------------------------

            general_df['imputed'].loc[:num_of_BH_rows] = ""

            general_df['BH'] = BH_df_controlled['HR']

            general_df['accel_mag_1'] = watch1_accel_df_controlled['r']
            general_df['gyro_mag_1'] = watch1_gyro_df_controlled['r']

            if activity in activities_watch2 and participant != "Apr13_ID10":
                watch2_path = system_path + participant + "_controlled/" + "watch2/watch2_hr_activity_sep_files/without_filling/" + activity + "_hr.csv"
                watch2_df = pd.read_csv(watch2_path)
                num_of_watch2_rows = watch2_df.shape[0]
                index_in_general_df = 0

                ts_watch2 = watch2_df['ts']
                ts_watch2 = pd.to_datetime(ts_watch2, dayfirst = True)
                ts_watch2 = ts_watch2.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
                ts_watch2 = ts_watch2.dt.time
                for i in range(num_of_watch2_rows):
                    ts = ts_watch2[i]
                    while (general_times.iloc[index_in_general_df] != ts):
                        index_in_general_df += 1


                    general_df['watch2'].iloc[index_in_general_df] = watch2_df['hr'].iloc[i]
                    general_df['rel2'].iloc[index_in_general_df] = watch2_df['r'].iloc[i]
                    index_in_general_df += 1


            general_dataframes_list.append(general_df)

        # Here is outside the loop iterating through the activities
        # Should add the wild part here

    print len(general_dataframes_list)
    main_df = pd.concat(general_dataframes_list)
    main_df = main_df.reset_index(drop=True)  # so indices don't start from zero after every concat
    main_df.to_csv(main_df_path)


def create_general_csv_wild():
    main_df_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild.csv"
    general_dataframes_list = []

    for participant in participants_list:
            print "participant is: ", participant
            general_df = pd.DataFrame(
                columns=['ts', 'ID', 'COPD', 'BH', 'watch1', 'rel1', 'accel_mag_1', 'gyro_mag_1'])

            if participant != "Apr19_ID11":
                watch1_path_controlled = system_path + participant + "_wild/" + "watch1/watch1_csv_files/all_hr_data.csv"
            BH_path_controlled = system_path + participant + "_wild/" + "Bioharness/shifted_times_log_wild.csv"

            if participant != "Apr19_ID11":
                watch1_accel_controlled_path = system_path + participant + "_wild/" + "watch1/watch1_csv_files/all_accel_data_compressed.csv"
                watch1_gyro_controlled_path = system_path + participant + "_wild/" + "watch1/watch1_csv_files/all_gyro_data_compressed.csv"

            if participant != "Apr19_ID11":
                watch1_df_controlled = pd.read_csv(watch1_path_controlled)
            BH_df_controlled = pd.read_csv(BH_path_controlled)

            if participant != "Apr19_ID11":
                watch1_accel_df_controlled = pd.read_csv(watch1_accel_controlled_path)
                watch1_gyro_df_controlled = pd.read_csv(watch1_gyro_controlled_path)

            num_of_watch1_rows = watch1_df_controlled.shape[0]
            num_of_BH_rows = BH_df_controlled.shape[0]

            # ------------------------------------------
            BH_timestamps = BH_df_controlled['Time']
            BH_timestamps = pd.to_datetime(BH_timestamps, dayfirst=True)
            BH_timestamps = BH_timestamps.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
            general_df['ts'] = BH_timestamps
            # -----------------------------------------

            general_df['ID'].loc[:num_of_BH_rows] = participant.split("_")[1]

            ID = int(participant.split("_")[1][2:4])
            if ID < 9:
                general_df['COPD'].loc[:num_of_BH_rows] = False
            else:
                general_df['COPD'].loc[:num_of_BH_rows] = True


            # --------------------------------------------------------

            general_times = general_df['ts'].dt.time
            if participant != "Apr19_ID11":
                index_in_general_df = 0
                ts_watch1 = watch1_df_controlled['ts']
                ts_watch1 = pd.to_datetime(ts_watch1, dayfirst=True)
                ts_watch1 = ts_watch1.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
                ts_watch1 = ts_watch1.dt.time

                # general_times = general_df['ts'].dt.time

                for i in range(num_of_watch1_rows):
                    ts = ts_watch1[i]

                    if ts < general_times.iloc[index_in_general_df]:
                        continue

                    while (general_times.iloc[index_in_general_df] != ts):
                        index_in_general_df += 1

                        if index_in_general_df == general_times.shape[0]:
                            break

                    if index_in_general_df == general_times.shape[0]:
                        break
                    general_df['watch1'].iloc[index_in_general_df] = watch1_df_controlled['bpm'].iloc[i]
                    general_df['rel1'].iloc[index_in_general_df] = watch1_df_controlled['acc'].iloc[i]

                    index_in_general_df += 1
                    if index_in_general_df == general_times.shape[0]:
                        break
            # --------------------------------------------------------


            general_df['BH'] = BH_df_controlled['HR']

            # ----- added this -----
            #general_times = general_df['ts'].dt.time


            start_time_in_general = general_df['ts'].dt.time.iloc[0]
            end_time_in_general = general_df['ts'].dt.time.iloc[-1]

            print "start time in general is: ", start_time_in_general
            print "end time in general is: ", end_time_in_general

            # the watch accel and gyro files have the same timestamps because they are generated using the same json files
            ts_watch1_accel_gyro = watch1_accel_df_controlled['ts']
            ts_watch1_accel_gyro = pd.to_datetime(ts_watch1_accel_gyro, dayfirst=True)
            ts_watch1_accel_gyro = ts_watch1_accel_gyro.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
            ts_watch1_accel_gyro = ts_watch1_accel_gyro.dt.time


            index_in_general_df = 0
            for i in range (watch1_accel_df_controlled.shape[0]):
                ts_accel_gyro = ts_watch1_accel_gyro[i]


                while ts_accel_gyro > general_times.iloc[index_in_general_df] and index_in_general_df < general_df.shape[0]-1:
                    index_in_general_df += 1

                if index_in_general_df == general_df.shape[0]:
                    break

                if general_times.iloc[index_in_general_df] == ts_accel_gyro:
                    general_df['accel_mag_1'].iloc[index_in_general_df] = watch1_accel_df_controlled['r'].iloc[i]
                    general_df['gyro_mag_1'].iloc[index_in_general_df] = watch1_gyro_df_controlled['r'].iloc[i]
                    index_in_general_df += 1

                if index_in_general_df == general_df.shape[0]:
                    break



            general_dataframes_list.append(general_df)

        # Here is outside the loop iterating through the activities
        # Should add the wild part here

    print len(general_dataframes_list)
    main_df = pd.concat(general_dataframes_list)
    main_df = main_df.reset_index(drop=True)  # so indices don't start from zero after every concat
    main_df.to_csv(main_df_path)




def add_withing_acceptable_range_column(dataframe, wild):
    # I get a dataframe as input. I generate another dataframe with the same columns + 2 extra columns:
    # within_10 and within_5
    # I look at the BH column and watch1 column
    # If the absolute difference between these two values is less than or equal to 5 : within_5 == True
    # If the absolute difference between these two values is less than or equal to 10 : within_10 == True

    if wild:
        new_dataframe_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild_with_acceptable_column.csv"
    else:
        new_dataframe_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_controlled_with_acceptable_column.csv"

    new_dataframe = dataframe.copy()

    new_dataframe['within_5'] = pd.Series(abs(dataframe['BH'] - dataframe['watch1']) <= 5, index=dataframe.index)
    new_dataframe['within_10'] = pd.Series(abs(dataframe['BH'] - dataframe['watch1']) <= 10, index=dataframe.index)

    new_dataframe.to_csv(new_dataframe_path)




if __name__ == '__main__':

    #create_general_csv_all_data()
    #add_accel_to_general_csv_all_data()
    #create_general_csv_wild()

    wild_csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild.csv"
    wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])
    add_withing_acceptable_range_column(wild_df, True)

    csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
    controlled_df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])

    add_withing_acceptable_range_column(controlled_df, False)


