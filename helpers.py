import pandas as pd
from datetime import datetime, date
import numpy as np
import datetime as dt
system_path = "/Users/pegah/Documents/VS/"

def fill_missing_rows_in_file(file_path):

    print "file path is: ", file_path
    reader = pd.read_csv(file_path)  #reader is a pandas dataframe
    new_path = file_path.split(".csv")[0] + "_complete.csv"
    writer_file = open(new_path, "w")
    writer_file.truncate()
    start_index = 0

    heart_rates = reader.iloc[:,1]
    reliabilities = reader.iloc[:,2]

    timestamps= reader.iloc[:,0] #[(reader.size)-1]
    timestamps = pd.to_datetime(timestamps)

    timestamps = timestamps.apply(lambda t: t.replace(microsecond=0))

    times = timestamps.dt.time

    file_length = times.size
    last_line_time = times[file_length-1]

    writer_df = pd.DataFrame(columns=['ts', 'hr', 'r'])  # writer is also a pandas dataframe
    while True:

        start_time = times[start_index]
        if start_time == last_line_time:
            writer_df = writer_df.append({'ts': timestamps[start_index] , 'hr': start_hr , 'r': start_r} , ignore_index= True )
            print "here in break"
            break
        start_hr = heart_rates[start_index]
        start_r = reliabilities[start_index]
        next_ts_time = times[start_index+1]

        subtraction = datetime.combine(date.today(), next_ts_time) - datetime.combine(date.today(),start_time)
        subtraction = abs(subtraction.total_seconds())

        writer_df = writer_df.append({'ts': timestamps[start_index] , 'hr': start_hr , 'r': start_r} , ignore_index= True )

        new_timestamp = timestamps[start_index]
        for j in range(1, int(subtraction)):
            new_timestamp =  new_timestamp + pd.Timedelta('00:00:01')

            new_row = [new_timestamp, start_hr, start_r ]
            writer_df = writer_df.append({'ts': new_timestamp , 'hr': start_hr , 'r': start_r} , ignore_index= True )

        start_index = start_index + 1

    writer_df.to_csv(writer_file, index = False)
    return new_path

def check_consecutive_watch_HR_values():
    # Based on what we know the watch reports a new value for the heart rate only if there has been a change.
    # Therefore, there should not exist two consecutive data points (consecutive means time stamps with 1 second difference)
    # with the same heart rate value.


    # Load the main csv file that has all the info of everyone.
    # Get the BH and watch1 column.

    # Check the values of watch1. If there are two consecutive points with the same value, check the time stamps.
    # Time stamps should not be exactly consecutive


    main_df_path = system_path + "main_csv_files/main_csv_file_with_all_data.csv"
    main_df = pd.read_csv(main_df_path)

    ts = pd.to_datetime(main_df['ts'])
    #ts = main_df['ts']
    watch1 = main_df['watch1']
    rel1 = main_df['rel1']

    size = ts.size

    equal_HR_rel_ts = 0

    for i in range(size-1):
        if (not np.isnan(watch1.iloc[i]) and (watch1.iloc[i] == watch1.iloc[i+1])):
            #print "same heart rate values"
            if rel1.iloc[i] == rel1.iloc[i+1]:
                dif = (ts[i+1].hour *3600 + ts[i+1].minute * 60 + ts[i+1].second) - \
                      (ts[i].hour *3600 + ts[i].minute * 60 + ts[i].second)


                if dif == 1:
                    equal_HR_rel_ts  += 1

    print equal_HR_rel_ts 








if __name__ == "__main__":
    check_consecutive_watch_HR_values()

