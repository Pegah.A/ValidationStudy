import pandas as pd
import csv

system_path = "/Users/pegah/Documents/ValidationStudyFiles/"
this_participant_path= "Feb1st_ID01_Healthy/"

# Checking to see if changes are saved.


def read_logs_from_app():

    activity_recorder_path = system_path + this_participant_path + "trial-10.json"
    activity_logs_csv_path = system_path + this_participant_path + "logs_Feb1_ID01.csv"  #"logs.csv"
    content = pd.read_json(activity_recorder_path)
    content['timestamp'] = content['timestamp'] - pd.Timedelta('05:00:00') # the app gives GMT time which is +5:00 from Toronto time
    #content.to_csv(activity_logs_csv_path)
    # if the participant performs all the 9 activities, the logs.csv should have 20 lines.

    row_chunk_nums, BH_logs_path = find_log_chunks_from_bh(activity_logs_csv_path)
    bh_activity_csv_creator(row_chunk_nums, BH_logs_path)



def find_log_chunks_from_bh(activity_logs_csv_path):
    logs_from_app = pd.read_csv(activity_logs_csv_path)
    app_timestamps= logs_from_app['timestamp']
    app_activity= logs_from_app['activity']

    BH_logs_path = system_path + this_participant_path + "Bioharness/"+  "log_2018_02_01-14_07_00_Summary.csv"
    BH_content= pd.read_csv(BH_logs_path)
    BH_timestamps = BH_content['Time']

    app_timestamps=app_timestamps.str.replace('-', '/')

    activity_num_counter = 0

    #BH_timestamps[i]  this piece of code, gives you what is in row i+2 of the csv file. index starts from 0 and does not count the title row.

    BH_row_division =[]
    for i in app_timestamps:
        time_= i.split(" ")[1].split(".")[0]  #getting the hour-minute-second of app log timestamps  format: 2018-02-01 14:25:11.246
        for row_num in range (BH_timestamps.size):
            if time_ in BH_timestamps[row_num]:
                print "Found the timestamp of " , app_activity[activity_num_counter] ,": ", time_
                print "It is at row: ", row_num + 2 , " of the BH logs"
                BH_row_division.append(row_num+2)

        activity_num_counter += 1

    print BH_row_division
    return BH_row_division, BH_logs_path

#def find_log_chunks_from_watch(activity_logs_csv_path):

def bh_activity_csv_creator(row_chunk_nums, BH_logs_path):  # creates a csv file for each activity that has all the summary information

    # csv files for each of the 9 activities
    sixmwt_path= system_path + this_participant_path + "Bioharness/bh_csv_files/sixmwt.csv"
    sixmwt_file = open(sixmwt_path, "w")
    sitting_path=system_path + this_participant_path + "Bioharness/bh_csv_files/sitting.csv"
    sitting_file = open(sitting_path, "w")
    walking_path=system_path + this_participant_path + "Bioharness/bh_csv_files/walking.csv"
    walking_file = open(walking_path, "w")
    lying_path=system_path + this_participant_path + "Bioharness/bh_csv_files/lyingdown.csv"
    lying_file = open(lying_path, "w")
    standing_path=system_path + this_participant_path + "Bioharness/bh_csv_files/standing.csv"
    standing_file = open(standing_path, "w")
    eating_path=system_path + this_participant_path + "Bioharness/bh_csv_files/eating.csv"
    eating_file = open(eating_path, "w")
    brushing_path=system_path + this_participant_path + "Bioharness/bh_csv_files/brushing.csv"
    brushing_file = open(brushing_path, "w")
    climbing_up_path=system_path + this_participant_path + "Bioharness/bh_csv_files/climbup.csv"
    climbing_up_file = open(climbing_up_path, "w")
    climbing_down_path=system_path + this_participant_path + "Bioharness/bh_csv_files/climbdown.csv"
    climbing_down_file = open(climbing_down_path, "w")

    BH_file = open(BH_logs_path, "r")
    reader = csv.reader(BH_file)
    row1 = next(reader) # reads the first row of the BH file which is the column titles

    all_files=[sixmwt_file, sitting_file, walking_file, lying_file, standing_file,eating_file, brushing_file, climbing_up_file, climbing_down_file]
    for item in all_files:
        item.truncate()
        writer = csv.writer(item)
        writer.writerow(row1)  # writes the column title row to the top of each file


    start_index = 1  #starting from 6MWT and not "experiment start"
    end_index = 2
    file_counter= 0


    BH_logs_file = open(BH_logs_path, "r")

    while(file_counter < 9):
        #for i in range(row_chunk_nums[start_index],row_chunk_nums[end_index]):
        reader= csv.reader(BH_logs_file)
        BH_logs_file.seek(0)  # to reset the file iterator, otherwise it will iterate through the BH file only once and will fill only the first file.
        writer= csv.writer(all_files[file_counter])
        for counter, row in enumerate(reader):
            if counter<row_chunk_nums[start_index]: continue
            if counter >= row_chunk_nums[end_index]: break
            writer.writerow(row)

        start_index += 2  # because we have to skip the end log of the current activity to get to the start log of the next one
        end_index += 2
        file_counter += 1


#def watch_activity_csv_creator():


#read_logs(activity_recorder_path)

def choose_file_for_HR_avg():
    sixmwt_path = system_path + this_participant_path + "Bioharness/bh_csv_files/sixmwt.csv"
    sitting_path = system_path + this_participant_path + "Bioharness/bh_csv_files/sitting.csv"
    walking_path = system_path + this_participant_path + "Bioharness/bh_csv_files/walking.csv"
    lying_path = system_path + this_participant_path + "Bioharness/bh_csv_files/lyingdown.csv"
    standing_path = system_path + this_participant_path + "Bioharness/bh_csv_files/standing.csv"
    eating_path = system_path + this_participant_path + "Bioharness/bh_csv_files/eating.csv"
    brushing_path = system_path + this_participant_path + "Bioharness/bh_csv_files/brushing.csv"
    climbing_up_path = system_path + this_participant_path + "Bioharness/bh_csv_files/climbup.csv"
    climbing_down_path = system_path + this_participant_path + "Bioharness/bh_csv_files/climbdown.csv"

    all_paths = [sixmwt_path, sitting_path, walking_path, lying_path, standing_path, eating_path, brushing_path,
                 climbing_up_path, climbing_down_path]
    for item in all_paths:
        HR_avg_segments_with_overlap(item)


#file_path = system_path + this_participant_path + "Bioharness/bh_csv_files/brushing.csv"

def HR_avg_segments_with_overlap(file_path):

    segments_avgs_path = file_path.split(".")[0] + "segments_avgs.txt"
    segments_avgs_file = open(segments_avgs_path, "w")
    segments_avgs_file.truncate()


    content = pd.read_csv(file_path)
    segment_length = 20
    overlap_percentage = 0.75
    overlap_value = int(segment_length * overlap_percentage)  # 15

    my_file = open(file_path, "r")
    segment_shift = segment_length - overlap_value
    segment_start = 0
    segment_end = segment_start + segment_length

    segments_avg_list=[]

    file_lines= my_file.readlines()
    file_len = len(file_lines)


    if file_len <= segment_length:   # all the file data can fit into one segment
        segment_sum=0
        segment_end = segment_start + file_len-1

        for i in range(segment_start, segment_end):
            segment_sum += content['HR'][i]

        segments_avg_list.append((segment_sum / 20))

    else:
        while (segment_end < file_len):
            segment_sum = 0
            for i in range(segment_start, segment_end):
                segment_sum += content['HR'][i]


            segments_avg_list.append((segment_sum/20))
            segment_start += segment_shift
            segment_end += segment_shift

    #print segments_avg_list

    segments_avgs_file.write(str(segments_avg_list))






















def check_row():
    BH_logs_path = system_path + this_participant_path + "Bioharness" + "/log_2018_02_01-14_07_00_Summary.csv"
    BH_content = pd.read_csv(BH_logs_path)

    sum_HR= 0
    max_hr = 0
    min_hr = 1000
    for i in range((2541-2),(2726-2)):
        #print BH_content['Time'][i]
        sum_HR = sum_HR + BH_content['HR'][i]
        if BH_content['HR'][i] > max_hr:
            max_hr = BH_content['HR'][i]
        if BH_content['HR'][i] < min_hr:
            min_hr= BH_content['HR'][i]

    avg = sum_HR / (2726-2541)
    print "eating avg is" , avg
    print "max is ",max_hr
    print "min is" ,min_hr

    sum_HR = 0
    for i in range((2829 - 2), (2959 - 2)):
        #print BH_content['Time'][i]
        sum_HR = sum_HR + BH_content['HR'][i]
        if BH_content['HR'][i] > max_hr:
            max_hr = BH_content['HR'][i]
        if BH_content['HR'][i] < min_hr:
            min_hr = BH_content['HR'][i]

    avg = sum_HR / (2959 - 2829)
    print "brushing avg is",avg
    print "max is",max_hr
    print "min is", min_hr



#read_logs_from_app()
#check_row()

choose_file_for_HR_avg()