import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from statsmodels.graphics.gofplots import qqplot

import pandas as pd
import numpy as np
from scipy import stats
import os
from CopdFeatureExtraction.helpers.SensorHelper import *


import time
from datetime import datetime, date

activity_list = ['sixmwt'] #, 'walking', 'lying', 'eating', 'brushing']
ID_list =['ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09','ID10','ID12', 'ID13', 'ID15', 'ID16' ]

csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])

system_path = "/Users/pegah/Documents/VS/"

def accel_gyro_statistics(dataframe):

    for activity in activity_list:
        accel_mean_list = []
        accel_std_list = []

        gyro_mean_list = []
        gyro_std_list = []


        for ID in ID_list:
            section = dataframe.query('activity==@activity and ID == @ID')
            accel_mag_1 = section['accel_mag_1']
            gyro_mag_1 = section ['gyro_mag_1']

            """
            print "ID is: ", ID
            print "activity is: ", activity
            print "mean accel is: ", np.mean(accel_mag_1)
            print "std accel is: ", np.std(accel_mag_1)
            print "var accel is: ", np.var(accel_mag_1)

            print "ID is: ", ID
            print "activity is: ", activity
            print "mean gyro is: ", np.mean(gyro_mag_1)
            print "std gyro is: ", np.std(gyro_mag_1)
            print "var gyro is: ", np.var(gyro_mag_1)
            
            """


            accel_mean_list.append(np.mean(accel_mag_1))
            accel_std_list.append(np.std(accel_mag_1))

            gyro_mean_list.append(np.mean(gyro_mag_1))
            gyro_std_list.append(np.std(gyro_mag_1))

        print accel_mean_list

        for i in range (len(accel_mean_list)):
            accel_mean_list[i] = accel_mean_list[i]

        print accel_mean_list


        matplotlib.rcParams.update({'font.size': 14})


        """
        plt.figure(figsize=(10, 8))
        plt.errorbar(ID_list[:7],accel_mean_list[:7], accel_std_list[:7], linestyle='None', marker='o', capsize=3,
                     color = "deeppink", label = "Non-COPD")
        plt.errorbar(ID_list[7:], accel_mean_list[7:], accel_std_list[7:], linestyle='None', marker='o', capsize=3,
                     color = "darkslateblue", label = "COPD")

        plt.title("Accelerometer Vector Magnitude Mean and Standard Deviation \n Activity: " + activity + "\n")
        plt.xlabel("Participant ID")
        plt.ylabel("Accelerometer mean and standard deviation")
        axes = plt.gca()
        axes.set_ylim([9, 15])
        plt.grid()
        plt.legend()

        fig_name = system_path + "graphs/accel_gyro/mean_std_scaled_with_dc/" + "accel_mean_std_per_participant_" + activity + ".png"
        #plt.savefig(fig_name)
        plt.show()

        """

        plt.figure(figsize=(10, 8))
        plt.errorbar(ID_list[:7], gyro_mean_list[:7], gyro_std_list[:7], linestyle='None', marker='o', capsize=3,
                     color="deeppink", label="Non-COPD")
        plt.errorbar(ID_list[7:], gyro_mean_list[7:], gyro_std_list[7:], linestyle='None', marker='o', capsize=3,
                     color="darkslateblue", label="COPD")

        plt.title("Gyroscope Vector Magnitude Mean and Standard Deviation \n Activity: " + activity + "\n")
        plt.xlabel("Participant ID")
        plt.ylabel("Gyroscope mean and standard deviation")
        axes = plt.gca()
        axes.set_ylim([-0.5, 5])
        plt.grid()
        plt.legend()

        fig_name = system_path + "graphs/accel_gyro/mean_std/scaled/" + "gyro_mean_std_per_participant_" + activity + ".png"
        #plt.savefig(fig_name)
        plt.show()


def accel_gyro_statistics_remove_DC(dataframe, accel_DC, gyro_DC):
    for activity in activity_list:
        accel_mean_list = []
        accel_std_list = []

        gyro_mean_list = []
        gyro_std_list = []


        invalid_ID_1 = 'ID11'
        invalid_ID_2 = 'ID14'
        invalid_ID_3 = 'ID10'


        COPD_true = True
        COPD_false = False


        this_activity_COPD_section = dataframe.query('activity == @activity and COPD == @COPD_true and ID != @invalid_ID_1 and ID != @invalid_ID_2')
        this_activity_non_COPD_section = dataframe.query('activity==@activity and COPD == @COPD_false and ID != @invalid_ID_1'
                                                         ' and ID != @invalid_ID_2')


        not_null_condition_COPD = np.isnan(this_activity_COPD_section['accel_mag_1']) == False
        not_null_condition_non_COPD = np.isnan(this_activity_non_COPD_section['accel_mag_1']) == False


        if activity == 'sixmwt':
            this_activity_COPD_section = dataframe.query(
                'activity==@activity and COPD == @COPD_true and ID != @invalid_ID_1 '
                ' and ID != @invalid_ID_2 and ID != @invalid_ID_3')
            this_activity_non_COPD_section = dataframe.query(
                'activity==@activity and COPD == @COPD_false and ID != @invalid_ID_1'
                ' and ID != @invalid_ID_2 and ID != @invalid_ID_3')


        this_activity_COPD_section = this_activity_COPD_section[not_null_condition_COPD]
        this_activity_non_COPD_section = this_activity_non_COPD_section[not_null_condition_non_COPD]

        this_activity_COPD_accel_vector_mag = np.abs(this_activity_COPD_section['accel_mag_1'] - accel_DC)
        this_activity_non_COPD_accel_vector_mag = np.abs(this_activity_non_COPD_section['accel_mag_1'] - accel_DC)

        this_activity_COPD_gyro_vector_mag = np.abs(this_activity_COPD_section['gyro_mag_1'] - gyro_DC)
        this_activity_non_COPD_gyro_vector_mag = np.abs(this_activity_non_COPD_section['gyro_mag_1'] - gyro_DC)


        for i in range(len(this_activity_COPD_section)):
            if np.isnan(this_activity_COPD_section['accel_mag_1'].iloc[i]):
                print this_activity_COPD_section.iloc[i]

        # ------- printing statistics -------


        #print accel_mean_list
        #print "accel mean non-COPD is: ", np.mean(accel_mean_list[:7])
        #print "accel mean COPD is: ", np.mean(accel_mean_list[7:])

        #print "gyro mean non-COPD is: ", np.mean(gyro_mean_list[:7])
        #print "gyro mean COPD is: ", np.mean(gyro_mean_list[7:])


        k, p = stats.kruskal(this_activity_COPD_accel_vector_mag, this_activity_non_COPD_accel_vector_mag)
        print "p is: ", p

        k, p = stats.kruskal(this_activity_COPD_gyro_vector_mag, this_activity_non_COPD_gyro_vector_mag)
        print "p is: ", p





        for ID in ID_list:
            section = dataframe.query('activity==@activity and ID == @ID')



            accel_mag_1 = np.abs(section['accel_mag_1'] - accel_DC)
            gyro_mag_1 = np.abs(section['gyro_mag_1'] - gyro_DC)



            
            print "ID is: ", ID
            print "activity is: ", activity
            print "mean accel is: ", np.mean(accel_mag_1)
            print "std accel is: ", np.std(accel_mag_1)
            print "var accel is: ", np.var(accel_mag_1)

            print "ID is: ", ID
            print "activity is: ", activity
            print "mean gyro is: ", np.mean(gyro_mag_1)
            print "std gyro is: ", np.std(gyro_mag_1)
            print "var gyro is: ", np.var(gyro_mag_1)

        


            accel_mean_list.append(np.mean(accel_mag_1))
            accel_std_list.append(np.std(accel_mag_1))

            gyro_mean_list.append(np.mean(gyro_mag_1))
            gyro_std_list.append(np.std(gyro_mag_1))
        


        # ------- printing statistics -------


        #print accel_mean_list
        print "accel mean non-COPD is: ", np.mean(accel_mean_list[:7])
        print "accel mean COPD is: ", np.mean(accel_mean_list[7:])

        print "gyro mean non-COPD is: ", np.mean(gyro_mean_list[:7])
        print "gyro mean COPD is: ", np.mean(gyro_mean_list[7:])

        k, p = stats.kruskal(this_activity_COPD_accel_vector_mag, this_activity_non_COPD_accel_vector_mag)
        print "p is: ", p

        k, p = stats.kruskal(this_activity_COPD_gyro_vector_mag, this_activity_non_COPD_gyro_vector_mag)
        print "p is: ", p
        

        # ------- drawing the plots -------

        matplotlib.rcParams.update({'font.size': 14})

        plt.figure(figsize=(10, 8))
        plt.errorbar(ID_list[:7],accel_mean_list[:7], accel_std_list[:7], linestyle='None', marker='o', capsize=3,
                     color = "deeppink", label = "Non-COPD")
        plt.errorbar(ID_list[7:], accel_mean_list[7:], accel_std_list[7:], linestyle='None', marker='o', capsize=3,
                     color = "darkslateblue", label = "COPD")

        plt.title("Accelerometer Vector Magnitude Mean and Standard Deviation \n Activity: " + activity + "\n")
        plt.xlabel("Participant ID")
        plt.ylabel("Accelerometer mean and standard deviation")
        axes = plt.gca()
        #axes.set_ylim([9, 15])
        plt.grid()
        plt.legend()

        fig_name = system_path + "graphs/accel_gyro/mean_std_scaled_with_dc/" + "accel_mean_std_per_participant_" + activity + ".png"
        #plt.savefig(fig_name)
        #plt.show()


        plt.figure(figsize=(10, 8))
        plt.errorbar(ID_list[:7], gyro_mean_list[:7], gyro_std_list[:7], linestyle='None', marker='o', capsize=3,
                     color="deeppink", label="Non-COPD")
        plt.errorbar(ID_list[7:], gyro_mean_list[7:], gyro_std_list[7:], linestyle='None', marker='o', capsize=3,
                     color="darkslateblue", label="COPD")

        plt.title("Gyroscope Vector Magnitude Mean and Standard Deviation \n Activity: " + activity + "\n")
        plt.xlabel("Participant ID")
        plt.ylabel("Gyroscope mean and standard deviation")
        axes = plt.gca()
        #axes.set_ylim([-0.5, 5])
        plt.grid()
        plt.legend()

        fig_name = system_path + "graphs/accel_gyro/mean_std/scaled/" + "gyro_mean_std_per_participant_" + activity + ".png"
        # plt.savefig(fig_name)
        plt.show()




def accel_gyro_statistics_wild(dataframe, accel_DC, gyro_DC):
    # I have the main_csv_file_wild file.
    # I want to put the accel value of all the COPD participants in one list and all the non-COPD participants in another
    # list. And do the same for gyro.
    # Then get statistics. Draw box plot. Calculate significance checks.

    ### I don't care about the reliability value. This is just to see how much movement they have.

    # The only invalid user here is ID11 because watch didn't work for them.

    invalid_ID_1 = 'ID11'
    invalid_ID_2 = 'ID14'

    section = dataframe.query('ID != @invalid_ID_1 and ID != @invalid_ID_2')

    #rel_not_null_condition = np.isnan(section['rel1']) == False
    accel_not_null_condition = np.isnan(section['accel_mag_1']) == False
    gyro_not_null_condition = np.isnan(section['gyro_mag_1']) == False

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD'] == False

    COPD_section = section[accel_not_null_condition & gyro_not_null_condition & COPD_condition]
    non_COPD_section = section[accel_not_null_condition & gyro_not_null_condition & non_COPD_condition]


    #rel_COPD_section = COPD_section['rel1']
    accel_abs_mag_COPD_section = np.abs(COPD_section['accel_mag_1'] - accel_DC)
    gyro_abs_mag_COPD_section = np.abs(COPD_section['gyro_mag_1'] - gyro_DC)


    #rel_non_COPD_section = non_COPD_section['rel1']
    accel_abs_mag_non_COPD_section = np.abs(non_COPD_section['accel_mag_1'] - accel_DC)
    gyro_abs_mag_non_COPD_section = np.abs(non_COPD_section['gyro_mag_1'] - gyro_DC)


    # Up to here, all the accel absolute vector magnitude values of COPD participants are in accel_abs_mag_COPD_section
    # All the accel absolute vector magnitude values of non-COPD participants are in accel_abs_mag_non_COPD_section
    # The same for gyro


    x = [1, 3, 5, 7, 9]
    k , p = stats.kruskal(list(accel_abs_mag_COPD_section), list(accel_abs_mag_non_COPD_section))
    print "p is: ", p


    y = [2, 4, 6, 8, 10]
    k1, p1 = stats.kruskal(list(accel_abs_mag_COPD_section), y)
    print "p1 isss:", p1

    print "accel mean COPD: ", np.mean(accel_abs_mag_COPD_section)
    print "accel mean non-COPD: ", np.mean(accel_abs_mag_non_COPD_section)

    print "gyro mean COPD: ", np.mean(gyro_abs_mag_COPD_section)
    print "gyro mean non_COPD: ", np.mean(gyro_abs_mag_non_COPD_section)



    #k2, p = stats.shapiro(accel_abs_mag_COPD_section)
    # Shapiro is not the best option because we have more than 5000 samples. Shapiro is good if the sample size is not that big


    #statistic, COPD_p_val = stats.f_oneway(gyro_abs_mag_COPD_section, gyro_abs_mag_non_COPD_section)
    # #print COPD_p_val


def check_difference_significance(activity, data_type, dataframe):


    for first_ID in ID_list:
        for second_ID in ID_list:
            if first_ID != second_ID:
                section_1 = dataframe.query('activity==@activity and ID == @first_ID')
                section_2 = dataframe.query('activity==@activity and ID == @second_ID')

                data_1 = section_1[data_type + '_mag_1']   # accel or gyro of the first person
                data_2 = section_2[data_type + '_mag_1']   # accel or gyro of the second person

                statistic, p_val = stats.ttest_ind(data_1, data_2)

                print "first ID is: ", first_ID
                print "second ID is: ", second_ID

                print "first mean is: ",np.mean(data_1)
                print "second mean is: ", np.mean(data_2)

                print "p value is: ", p_val


def anova_test(activity, data_type,dataframe):

    COPD_ID_list = ['ID09','ID10','ID12', 'ID13', 'ID14', 'ID15', 'ID16' ]
    non_COPD_ID_list = ['ID02','ID03','ID04','ID05','ID06','ID07','ID08',]

    ID = 'ID09'
    section09 = dataframe.query('activity==@activity and ID == @ID')
    data09 = section09[data_type + '_mag_1']



    ID = 'ID10'
    section10 = dataframe.query('activity==@activity and ID == @ID')
    data10 = section10[data_type + '_mag_1']

    ID = 'ID12'
    section12 = dataframe.query('activity==@activity and ID == @ID')
    data12 = section12[data_type + '_mag_1']

    ID = 'ID13'
    section13 = dataframe.query('activity==@activity and ID == @ID')
    data13 = section13[data_type + '_mag_1']

    data13 = data13[:-1]

    ID = 'ID14'
    section14 = dataframe.query('activity==@activity and ID == @ID')
    data14 = section14[data_type + '_mag_1']

    ID = 'ID15'
    section15 = dataframe.query('activity==@activity and ID == @ID')
    data15= section15[data_type + '_mag_1']

    ID = 'ID16'
    section16 = dataframe.query('activity==@activity and ID == @ID')
    data16= section16[data_type + '_mag_1']


    for item in data13:
        if np.isnan(item):
            print item



    statistic, COPD_p_val = stats.f_oneway(data09, data10, data12, data13, data14, data15, data16)

    print "copd p value: ",COPD_p_val


    # -----

    ID = 'ID02'
    section02 = dataframe.query('activity==@activity and ID == @ID')
    data02 = section02[data_type + '_mag_1']

    ID = 'ID03'
    section03 = dataframe.query('activity==@activity and ID == @ID')
    data03 = section03[data_type + '_mag_1']

    ID = 'ID04'
    section04 = dataframe.query('activity==@activity and ID == @ID')
    data04 = section04[data_type + '_mag_1']

    ID = 'ID05'
    section05 = dataframe.query('activity==@activity and ID == @ID')
    data05 = section05[data_type + '_mag_1']

    ID = 'ID06'
    section06 = dataframe.query('activity==@activity and ID == @ID')
    data06 = section06[data_type + '_mag_1']

    ID = 'ID07'
    section07 = dataframe.query('activity==@activity and ID == @ID')
    data07 = section07[data_type + '_mag_1']

    ID = 'ID08'
    section08 = dataframe.query('activity==@activity and ID == @ID')
    data08 = section08[data_type + '_mag_1']

    statistic, non_COPD_p_val = stats.f_oneway(data02, data03, data04, data05, data06, data07, data08)

    print "non-copd p value: ", non_COPD_p_val



def check_axis_from_watch():
    system_path = "/Users/pegah/Documents/VS/participant_data_updated/"
    this_participant_path = "axis_check/"

    path = system_path + this_participant_path
    list_of_files = []
    for file in os.listdir(path):
        if file.endswith(".json"):
            list_of_files.append(file)

    for item in list_of_files:
        file_name = str(item).split(".")[0]
        file_path = system_path + this_participant_path + file_name + ".json"

        accel_file_path = system_path + this_participant_path + "csv_files/" + file_name + "_accel.csv"
        gyro_file_path = system_path + this_participant_path  + "csv_files/" + file_name + "_gyro.csv"

        data = SensorJson(file_path)
        accel = data.get("accel")
        gyro = data.get("gyro")


        accel.index = accel.index - pd.Timedelta('04:00:00')
        gyro.index = gyro.index - pd.Timedelta('04:00:00')


        accel.to_csv(accel_file_path)
        gyro.to_csv(gyro_file_path)

    all_accel_path = system_path + this_participant_path + "csv_files" + "/all_accel_data.csv"
    all_gyro_path = system_path + this_participant_path + "csv_files" + "/all_gyro_data.csv"

    accel_files_list = []
    gyro_files_list = []

    for file in os.listdir(system_path + this_participant_path +  "csv_files"):


        if file.endswith("accel.csv"):
            accel_files_list.append(file)
        elif file.endswith("gyro.csv"):
            gyro_files_list.append(file)

    accel_files_list.sort()
    gyro_files_list.sort()


    for i in range(len(accel_files_list)):
        accel_files_list[i] = open(
            system_path + this_participant_path + "csv_files/" + accel_files_list[i], "r")

    for i in range(len(gyro_files_list)):
        gyro_files_list[i] = open(
            system_path + this_participant_path +  "csv_files/" + gyro_files_list[i], "r")

    combined_csv_accel = pd.concat([pd.read_csv(f) for f in accel_files_list])
    combined_csv_accel.to_csv(all_accel_path, index=False)

    combined_csv_gyro = pd.concat([pd.read_csv(f) for f in gyro_files_list])
    combined_csv_gyro.to_csv(all_gyro_path, index=False)




def convert_str_to_unix(datetime_in_str):
    # We have the timestamp in a string format and we want to convert it to unix time
    # Format of the str timestamp:  May 4th, 2018 at 15:34:12 would be 20180504153412

    print "date and time in str is: ", datetime_in_str
    return int(time.mktime(datetime.strptime(datetime_in_str , "%Y%m%d%H%M%S").timetuple()))





def calc_DC_watch_accel_gyro(newtime):
    # In the participants, there is a axis_check folder
    # It has a all_accel_data.csv and all_gyro_data.csv file.
    # There is also a time_range tyext file that has the start time and end time of the chunk that we are looking for

    time_range_path = system_path + "participant_data_updated/axis_check/time_range"
    time_range_file = open(time_range_path, "r")

    start_time = time_range_file.readline().strip()
    end_time = time_range_file.readline().strip()

    # Converting the str start time and end time into unix time
    start_time_unix = convert_str_to_unix(start_time)
    end_time_unix = convert_str_to_unix(end_time)

    if newtime:
        start_time =  pd.to_datetime(start_time_unix, unit = 's')-pd.Timedelta('04:00:00')
        end_time = pd.to_datetime(end_time_unix, unit='s') - pd.Timedelta('04:00:00')
    else:
        start_time = pd.to_datetime(start_time_unix, unit='s') - pd.Timedelta('05:00:00')
        end_time = pd.to_datetime(end_time_unix, unit='s') - pd.Timedelta('05:00:00')


    print "start time is: ", start_time.timetz()
    print "end time is: ", end_time.timetz()

    start_time = start_time.timetz()
    end_time = end_time.timetz()

    accel_path = system_path + "participant_data_updated/axis_check/watch1/watch1_csv_files/all_accel_data.csv"
    gyro_path = system_path + "participant_data_updated/axis_check/watch1/watch1_csv_files/all_gyro_data.csv"


    accel_df = pd.read_csv(accel_path)
    gyro_df = pd.read_csv(gyro_path)

    ts_watch1_accel_gyro = accel_df['ts']
    ts_watch1_accel_gyro = pd.to_datetime(ts_watch1_accel_gyro, dayfirst=True)
    ts_watch1_accel_gyro = ts_watch1_accel_gyro.apply(lambda t: t.replace(microsecond=0))  # to remove the milliseconds
    ts_watch1_accel_gyro = ts_watch1_accel_gyro.dt.time


    start_index = 0
    end_index = 0

    found_start = False

    for i in range(accel_df.shape[0]):
        ts_accel_gyro = ts_watch1_accel_gyro[i]

        if  start_time == ts_accel_gyro and not found_start:
            start_index = i
            found_start = True

        if  end_time == ts_accel_gyro:
            end_index = i


    accel_chunk = ((accel_df[start_index: end_index+1]['x'] **2) +  (accel_df[start_index: end_index+1]['y'] **2)
    +(accel_df[start_index: end_index + 1]['z'] ** 2)) ** 0.5

    gyro_chunk = ((gyro_df[start_index: end_index+1]['x'] **2) +  (gyro_df[start_index: end_index+1]['y'] **2)
    +(gyro_df[start_index: end_index + 1]['z'] ** 2)) ** 0.5

    print np.mean(accel_chunk)
    print np.mean(gyro_chunk)


def box_plot_rel_accel_gyro_ranges(dataframe, accel_DC, gyro_DC):
    # I want a graph with rel as y-axis and accel as x-axis
    # I have the main_csv_wild file.
    # I want to read that, then for every data point that has a reported watch value, I want the rel and the absolute value of accel-DC

    invalid_ID = 'ID11'
    section = dataframe.query('ID != @invalid_ID')

    rel_not_null_condition = np.isnan(section['rel1']) == False
    accel_not_null_condition = np.isnan(section['accel_mag_1']) == False
    gyro_not_null_condition = np.isnan(section['gyro_mag_1']) == False

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD'] == False

    COPD_collection_list_accel = []
    non_COPD_collection_list_accel = []

    COPD_collection_list_gyro = []
    non_COPD_collection_list_gyro = []


    COPD_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition & COPD_condition]
    non_COPD_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition & non_COPD_condition]
    all_participants_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition]

    print COPD_section



    for i in range(0,4):
        rel_val_condition= section['rel1']== 1.0 * i

        new_COPD_section = COPD_section[rel_val_condition]
        new_non_COPD_section = non_COPD_section[rel_val_condition]

        COPD_collection_list_accel.append(np.abs(new_COPD_section['accel_mag_1'] - accel_DC))
        COPD_collection_list_gyro.append(np.abs(COPD_section['gyro_mag_1'] - gyro_DC))

        non_COPD_collection_list_accel.append(np.abs(new_non_COPD_section['accel_mag_1'] - accel_DC))
        non_COPD_collection_list_gyro.append(np.abs(non_COPD_section['gyro_mag_1'] - gyro_DC))



    # ------ plots ---------
    matplotlib.rcParams.update({'font.size': 16})

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    axes.axes.get_yaxis().set_ticks([])


    plt.boxplot(COPD_collection_list_accel, whis = 'range')

    plt.title("Reliability over Accelerometer Absolute Vector Magnitude\nControlled Setting")
    plt.ylabel("Reliability")
    plt.xlabel("Accelerometer Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_accel/together.png"
    #plt.savefig(fig_name)
    plt.show()


    """
    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    axes.axes.get_yaxis().set_ticks([])

    plt.plot(gyro_abs_mag_non_COPD_section, rel_non_COPD_section, 'o', color="deeppink", label="non-COPD")
    plt.plot(gyro_abs_mag_COPD_section, rel_COPD_section, 'o', color="darkslateblue", label="COPD")

    plt.title("Reliability over Gyroscope Absolute Vector Magnitude\nControlled Setting")
    plt.ylabel("Reliability")
    plt.xlabel("Gyroscope Absolute Vector Magnitude")
    plt.grid()
    plt.legend()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_gyro/together.png"
    #plt.savefig(fig_name)
    """



def plot_rel_accel_gyro_ranges(dataframe, accel_DC, gyro_DC):

    # I want a graph with rel as y-axis and accel as x-axis
    # I have the main_csv_wild file.
    # I want to read that, then for every data point that has a reported watch value, I want the rel and the absolute value of accel-DC

    # I'm just using the data from the watch. HR and accel and gyro so the only invalid is: ID11
    invalid_ID = 'ID11'
    section = dataframe.query('ID != @invalid_ID')


    rel_not_null_condition = np.isnan(section['rel1']) == False
    accel_not_null_condition = np.isnan(section['accel_mag_1']) == False
    gyro_not_null_condition = np.isnan(section['gyro_mag_1']) == False

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD'] == False

    COPD_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition & COPD_condition]
    non_COPD_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition & non_COPD_condition]
    all_participants_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition]


    rel_COPD_section = COPD_section['rel1']
    accel_abs_mag_COPD_section = np.abs(COPD_section['accel_mag_1'] - accel_DC)
    gyro_abs_mag_COPD_section= np.abs(COPD_section['gyro_mag_1'] - gyro_DC)

    rel_non_COPD_section = non_COPD_section['rel1']
    accel_abs_mag_non_COPD_section = np.abs(non_COPD_section['accel_mag_1'] - accel_DC)
    gyro_abs_mag_non_COPD_section = np.abs(non_COPD_section['gyro_mag_1'] - gyro_DC)

    rel_all_section = all_participants_section['rel1']
    accel_abs_mag_all_section = np.abs(all_participants_section['accel_mag_1'] - accel_DC)
    gyro_abs_mag_all_section = np.abs(all_participants_section['gyro_mag_1'] - gyro_DC)


    # ------ plots ---------
    matplotlib.rcParams.update({'font.size': 16})

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    axes.axes.get_yaxis().set_ticks([])

    rel_COPD_section -= 0.2

    y3 = [-0.1, 0.9, 1.9, 2.9]
    yticks_val = [0,1,2,3]

    plt.plot(accel_abs_mag_non_COPD_section, rel_non_COPD_section, 'o', color="deeppink" , label = " non-COPD")
    plt.plot(accel_abs_mag_COPD_section, rel_COPD_section, 'o', color="darkslateblue", label = "COPD")

    plt.title("Reliability over Accelerometer Vector Magnitude\nUncontrolled Setting")
    plt.ylabel("Reliability")
    plt.xlabel("Accelerometer Vector Magnitude")

    plt.yticks(y3, yticks_val)

    plt.grid()
    plt.legend()

    fig_name = system_path + "graphs/accel_gyro/in_the_wild/rel_over_accel/rel_over_accel_wild_all.png"
    plt.savefig(fig_name)
    #plt.show()

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    axes.axes.get_yaxis().set_ticks([])


    plt.plot(gyro_abs_mag_non_COPD_section, rel_non_COPD_section, 'o', color="deeppink" , label = "non-COPD")
    plt.plot(gyro_abs_mag_COPD_section, rel_COPD_section, 'o', color="darkslateblue", label = "COPD")

    plt.title("Reliability over Gyroscope Vector Magnitude\nUncontrolled Setting")
    plt.ylabel("Reliability")
    plt.xlabel("Gyroscope Vector Magnitude")

    plt.yticks(y3, yticks_val)

    plt.grid()
    plt.legend()

    fig_name = system_path + "graphs/accel_gyro/in_the_wild/rel_over_gyro/rel_over_gyro_wild_all.png"
    plt.savefig(fig_name)
    #plt.show()



    """
    # ------ plots ---------
    matplotlib.rcParams.update({'font.size': 16})

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    plt.plot(accel_abs_mag_all_section, rel_all_section,'o', color = "slateblue")
    plt.title("Reliability over Accelerometer Absolute Vector Magnitude\n All participants")
    plt.ylabel("Reliability")
    plt.xlabel("Accelerometer Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_accel/all_participants.png"
    plt.savefig(fig_name)

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    plt.plot(gyro_abs_mag_all_section, rel_all_section, 'o', color="slateblue")
    plt.title("Reliability over Gyroscope Absolute Vector Magnitude\n All participants")
    plt.ylabel("Reliability")
    plt.xlabel("Gyroscope Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_gyro/all_participants.png"
    plt.savefig(fig_name)

    # ------ non-COPD ---------

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    plt.plot(accel_abs_mag_non_COPD_section, rel_non_COPD_section, 'o', color="slateblue")
    plt.title("Reliability over Accelerometer Absolute Vector Magnitude\n Non-COPD participants")
    plt.ylabel("Reliability")
    plt.xlabel("Accelerometer Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_accel/non_COPD_participants.png"
    plt.savefig(fig_name)

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    plt.plot(gyro_abs_mag_non_COPD_section, rel_non_COPD_section, 'o', color="slateblue")
    plt.title("Reliability over Gyroscope Absolute Vector Magnitude\n Non-COPD participants")
    plt.ylabel("Reliability")
    plt.xlabel("Gyroscope Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_gyro/non_copd_participants.png"
    plt.savefig(fig_name)

    #------ COPD -------------
    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    plt.plot(accel_abs_mag_COPD_section, rel_COPD_section,'o', color="slateblue")
    plt.title("Reliability over Accelerometer Absolute Vector Magnitude\n COPD participants")
    plt.ylabel("Reliability")
    plt.xlabel("Accelerometer Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_accel/copd_participants.png"
    plt.savefig(fig_name)

    plt.figure(figsize=(10, 8))
    axes = plt.gca()
    axes.set_xlim([0, 12])
    plt.plot(gyro_abs_mag_COPD_section, rel_COPD_section, 'o',color="slateblue")
    plt.title("Reliability over Gyroscope Absolute Vector Magnitude\n COPD participants")
    plt.ylabel("Reliability")
    plt.xlabel("Gyroscope Absolute Vector Magnitude")
    plt.grid()

    fig_name = system_path + "graphs/accel_gyro/controlled/rel_over_gyro/copd_participants.png"
    plt.savefig(fig_name)
    """


    # ------- calculating percentiles --------
    #rel_condition = COPD_section['rel1'] == 3.0
    #rel_section = np.abs(COPD_section[rel_condition]['gyro_mag_1'] - gyro_DC)

    #print np.percentile(rel_section, 95)

    """
    rel_3 = not_null_section['rel1'] == 1.0
    rel_3_section = np.abs(not_null_section[rel_3]['accel_mag_1'] - accel_DC)
    print np.percentile(rel_3_section, 5)


    count_rel_3_accel_more_3 = 0
    for i in range (rel_not_null_section.shape[0]):
        if rel_not_null_section.iloc[i] == 1.0  and accel_abs_mag_not_null_section.iloc[i] > 0.9:
            count_rel_3_accel_more_3 += 1

    print count_rel_3_accel_more_3

    #plt.plot(accel_abs_mag_not_null_section,rel_not_null_section, "bo")
    #plt.plot(gyro_abs_mag_not_null_section, rel_not_null_section, "bo")

    #plt.grid()
    #plt.show()
    
    """


def plot_rel_accel_gyro_ranges_percentile_bar_graph(dataframe, accel_DC, gyro_DC):
    # I have the graphs from the previous function
    # for each reliability, instead of having the data points drawn, I want the percentile value

    invalid_ID = 'ID11'
    section = dataframe.query('ID != @invalid_ID')

    rel_not_null_condition = np.isnan(section['rel1']) == False
    accel_not_null_condition = np.isnan(section['accel_mag_1']) == False
    gyro_not_null_condition = np.isnan(section['gyro_mag_1']) == False

    COPD_condition = section['COPD'] == True
    non_COPD_condition = section['COPD'] == False

    COPD_section = section[rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition & COPD_condition]
    non_COPD_section = section[
        rel_not_null_condition & accel_not_null_condition & gyro_not_null_condition & non_COPD_condition]


    # ------- calculating percentiles --------
    # rel_condition = COPD_section['rel1'] == 3.0
    # rel_section = np.abs(COPD_section[rel_condition]['gyro_mag_1'] - gyro_DC)

    # print np.percentile(rel_section, 95)

    COPD_accel_percentile_list = []
    COPD_gyro_percentile_list = []

    non_COPD_accel_percentile_list = []
    non_COPD_gyro_percentile_list = []

    for i in range (4):
        rel_condition_COPD = COPD_section['rel1'] == 1.0 * i
        rel_condition_non_COPD = non_COPD_section['rel1'] == 1.0 * i

        #print COPD_section[rel_condition]

        rel_section_COPD_accel = np.abs(COPD_section[rel_condition_COPD]['accel_mag_1'] - accel_DC)
        rel_section_COPD_gyro = np.abs(COPD_section[rel_condition_COPD]['gyro_mag_1'] - gyro_DC)


        COPD_accel_percentile_list.append(np.percentile(rel_section_COPD_accel, 95))
        COPD_gyro_percentile_list.append(np.percentile(rel_section_COPD_gyro, 95))


        rel_section_non_COPD_accel = np.abs(non_COPD_section[rel_condition_non_COPD]['accel_mag_1'] - accel_DC)
        rel_section_non_COPD_gyro = np.abs(non_COPD_section[rel_condition_non_COPD]['gyro_mag_1'] - gyro_DC)

        non_COPD_accel_percentile_list.append(np.percentile(rel_section_non_COPD_accel, 95))
        non_COPD_gyro_percentile_list.append(np.percentile(rel_section_non_COPD_gyro, 95))


    print COPD_accel_percentile_list
    print non_COPD_accel_percentile_list

    print COPD_gyro_percentile_list
    print non_COPD_gyro_percentile_list

    # So far I have the lists
    # I want to draw bar graph

    matplotlib.rcParams.update({'font.size': 16})

    y3 = [-0.1, 0.9, 1.9, 2.9]
    yticks_val = [0, 1, 2, 3]
    rel_non_COPD_list = [0, 1, 2, 3]
    rel_COPD_list = [x - 0.2 for x in rel_non_COPD_list]

    plt.figure(figsize=(10, 8))
    plt.barh(rel_non_COPD_list, non_COPD_accel_percentile_list, 0.1, color="deeppink", label='non-COPD')
    plt.barh(rel_COPD_list, COPD_accel_percentile_list, 0.1 , color= "darkslateblue", label = 'COPD')
    plt.yticks(y3, yticks_val)

    plt.ylabel("Reliability")
    plt.xlabel("95 Percentile")
    plt.title("95 Percentile Values Of Accelerometer Data For Different Reliabilities\n")

    plt.legend()
    plt.grid()
    fig_name = system_path + "graphs/accel_gyro/in_the_wild/rel_over_accel/rel_over_accel_wild_all_percentile.png"
    plt.savefig(fig_name)
    #plt.show()

    plt.figure(figsize=(10, 8))
    plt.barh(rel_non_COPD_list, non_COPD_gyro_percentile_list, 0.1, color="deeppink", label='non-COPD')
    plt.barh(rel_COPD_list, COPD_gyro_percentile_list, 0.1, color="darkslateblue", label='COPD')
    plt.yticks(y3, yticks_val)

    plt.ylabel("Reliability")
    plt.xlabel("95 Percentile")
    plt.title("95 Percentile Values Of Gyroscope Data For Different Reliabilities\n")

    plt.legend()
    plt.grid()
    fig_name = system_path + "graphs/accel_gyro/in_the_wild/rel_over_gyro/rel_over_gyro_wild_all_percentile.png"
    plt.savefig(fig_name)
    #plt.show()













#accel_gyro_statistics(df)

#check_difference_significance('lying','gyro', df)
#anova_test('walking', 'gyro', df)
#check_axis_from_watch()

wild_csv_path  = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_wild.csv"
wild_df = pd.read_csv(wild_csv_path, index_col=0, parse_dates=['ts'])

#csv_path = "/Users/pegah/Documents/VS/main_csv_files/main_csv_file_with_all_data.csv"
df = pd.read_csv(csv_path, index_col=0, parse_dates=['ts'])
#plot_rel_accel_gyro_ranges(wild_df, 9.77, 0.01)
#plot_rel_accel_gyro_ranges_percentile_bar_graph(wild_df, 9.77, 0.01)

#accel_gyro_statistics(df)
accel_gyro_statistics_remove_DC(df,9.76709709718, 0.0126726204948 )
#accel_gyro_statistics_wild(wild_df, 9.76709709718, 0.0126726204948)

#calc_DC_watch_accel_gyro(True)


